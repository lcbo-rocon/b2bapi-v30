﻿using System;
using Microsoft.Extensions.Configuration;
using System.IO;
using System.Threading.Tasks;
using RoconLibrary.Utility;
using Microsoft.Extensions.DependencyInjection;
using LoadLCOCatalog.Services;
using Serilog;
using Serilog.Events;

namespace LoadLCOCatalog
{
    class Program
    {
        static void Main(string[] args)
        {
            IServiceCollection services = new ServiceCollection();
            
            Startup startup = new Startup();
            startup.ConfigureServices(services);
            IServiceProvider serviceProvider = services.BuildServiceProvider();

            var config = serviceProvider.GetService<IConfigurationRoot>();

            var loggingFilePath = config["Serilog:filepath"];
            var outputTemplate = config["Serilog:outputTemplate"];
            var rollingInterval = config["Serilog:rollingInterval"];
            var fileSizeLimitBytes = Int32.Parse(config["Serilog:fileSizeLimitBytes"]);
            var minimumLevel = config["Serilog:minimumLevel"];

            var level = (LogEventLevel) Enum.Parse(typeof(LogEventLevel), minimumLevel);

            Log.Logger = new LoggerConfiguration()
                            .MinimumLevel.Is(level)
                            .WriteTo.Console()
                            .WriteTo.File(loggingFilePath,
                                          outputTemplate: outputTemplate,
                                          fileSizeLimitBytes: fileSizeLimitBytes,
                                          rollingInterval: (RollingInterval) Enum.Parse(typeof(RollingInterval), rollingInterval),
                                          restrictedToMinimumLevel: level)
                            .CreateLogger();

            Log.Information($"Current dir: {Directory.GetCurrentDirectory()}");

            var connection = ConnectionUtil.getConnection(config);
            var catalogService = serviceProvider.GetService<ICatalogService>();
            
            try
            {
                catalogService.processStart(connection);
            } catch(Exception e)
            {
                Log.Error("Error reading in catalogService processStart. Message = {0}. Stacktrace is: {1}", e.Message, e.StackTrace);
            }
        }
    }
}
