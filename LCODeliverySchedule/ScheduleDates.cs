using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Text;
using LCODeliverySchedule.Services;
using Oracle.ManagedDataAccess.Client;

namespace LCODeliverySchedule
{
    public class ScheduleDates
    {
        public ArrayList DeliveryDates { get; set; }
        public Hashtable ExcludedDates { get; set; }

        public Hashtable PrevCutoffDayOfWeeks { get; set; }

        public ScheduleDates(DateTime dtStartDate, OracleConnection connection)
        {
            DeliveryDates = new ArrayList();
            ExcludedDates = new Hashtable();
            PrevCutoffDayOfWeeks = new Hashtable();

            DataTable dtHolidays = DeliveryScheduleHelper.GetHolidays(connection);

            foreach (DataRow dr in dtHolidays.Rows)
            {
                if (!ExcludedDates.ContainsKey(dr["HOLIDAY_DT"]))
                { 
                    DateTime.TryParse(dr["HOLIDAY_DT"].ToString(), out dtStartDate);
                    ExcludedDates.Add(dr["HOLIDAY_DT"].ToString(), dtStartDate);
                }
            }

            //Valid Cutoff Order dates is MON - Friday
            PrevCutoffDayOfWeeks.Add(DayOfWeek.Monday, DayOfWeek.Friday);
            PrevCutoffDayOfWeeks.Add(DayOfWeek.Tuesday, DayOfWeek.Monday);
            PrevCutoffDayOfWeeks.Add(DayOfWeek.Wednesday, DayOfWeek.Tuesday);
            PrevCutoffDayOfWeeks.Add(DayOfWeek.Thursday, DayOfWeek.Wednesday);
            PrevCutoffDayOfWeeks.Add(DayOfWeek.Friday, DayOfWeek.Thursday);
        }
    }
}
