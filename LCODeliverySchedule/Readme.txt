1-) From the donwloaded directory run:
    
    donet run

    Because we are not passing parameters above the program will use default parameters value setup in 
    appsettings.Development.json or appsettings.Production.json

2-) To override default parameters, see examples below (notice that omitted parameters will come from default parameters):

    dotnet run initialDate="2020-08-01" year=2020 deliveryCode="BR06"

    or

    dotnet run initialDate="2020-08-01" year=2020

    or

    dotnet run initialDate="2020-08-01" year=2020

    or

    any combination

    dotnet deliveryCode="BR06"