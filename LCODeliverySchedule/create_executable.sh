rm -R bin/Debug/netcoreapp2.1/win10-x64
dotnet publish -c Debug -r win10-x64
cp appsettings.Development.json bin/Debug/netcoreapp2.1/win10-x64/publish/

sed -i 's/lib\/netstandard2.0\///g' bin/Debug/netcoreapp2.1/win10-x64/publish/LCODeliverySchedule.deps.json
sed -i 's/lib\/netcoreapp2.0\///g' bin/Debug/netcoreapp2.1/win10-x64/publish/LCODeliverySchedule.deps.json

sleep 5

pwd
zip -r LCODeliverySchedule.zip bin/Debug/netcoreapp2.1/win10-x64/publish
