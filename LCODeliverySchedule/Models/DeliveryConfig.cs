﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LCODeliverySchedule.Models
{
    class DeliveryConfig
    {
        public string DeliveryCode { get; set; }
        public DayOfWeek CutoffDOW { get; set; }
        public string CutoffTime { get; set; }
        public DayOfWeek DeliveryDOW { get; set; }
        public DayOfWeek HolCutoffDOW { get; set; }   
        public DayOfWeek HolDeliveryDOW { get; set; }

    }
}
