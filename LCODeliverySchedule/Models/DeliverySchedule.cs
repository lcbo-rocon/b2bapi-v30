﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LCODeliverySchedule.Models
{
    public class DeliverySchedule
    {
        public string YearWeek { get; set; }
        public string DeliveryCode { get; set; }

        public DayOfWeek CutOffDOW { get; set; }

        public DayOfWeek DeliveryDOW { get; set; }

        public DateTime CutOffDateTime { get; set; }

        public DateTime DeliveryDate { get; set; }

        public bool isHolidaySchedule { get; set; }

        public bool Override { get; set; }

        public string CreatedBy { get; set; }


        public override string ToString()
        {
            return "YearWeek: " + YearWeek + " Delivery code: " + DeliveryCode + " " + 
                    CutOffDOW + "/" + DeliveryDOW +
                " Cutoff date: " + CutOffDateTime.ToString("ddd yyyy-MM-dd hh:mm") +
                " Delivery Date: " + DeliveryDate.ToString("ddd yyyy-MM-dd");
        }
    }

}
