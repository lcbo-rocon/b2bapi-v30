﻿using Microsoft.Extensions.Configuration;
using Oracle.ManagedDataAccess.Client;
using RoconLibrary.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace RoconListener.Services
{
    public interface IListenerOrderService
    {
        void processStart(OracleConnection connection);
    }
}
