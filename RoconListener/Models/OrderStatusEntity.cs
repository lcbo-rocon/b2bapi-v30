﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RoconListener.Models
{
    public class OrderStatusEntity
    {
        public string OrderNumber { get; set; }
        public string Status { get; set; }
        public string StatusSequence { get; set; }
        public string StatusCyclePath { get; set; }
        public DateTime StatusDtTm { get; set; }
        public DateTime ExtractDtTm { get; set; }
    }
}
