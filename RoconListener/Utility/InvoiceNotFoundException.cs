using System;

[Serializable]
public class InvoiceNotFoundException : Exception { }