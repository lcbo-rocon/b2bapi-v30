using Serilog;
using System;
using System.Globalization;

namespace RoconListener.Utility
{
    class DateHelper
    {
        public static void DateTimeTimeZoneInfo()
        {
            const string dataFmt = "{0,-20}{1}";
            const string timeFmt = "{0,-20}{1:yyyy-MM-dd HH:mm}";

            // Get the local time zone and the current local time and year.
            TimeZone localZone = TimeZone.CurrentTimeZone;
            DateTime currentDate = DateTime.Now;
            int currentYear = currentDate.Year;

            TimeSpan currentOffset = localZone.GetUtcOffset(currentDate);
            Log.Information(dataFmt, "Server Time Zone:", localZone.StandardName + ", " + currentDate + " " + currentOffset);

            DateTime currentUTC = localZone.ToUniversalTime( currentDate );
            Log.Information( timeFmt, "Current UTC:", currentUTC );

            // Display the daylight saving time range for the current year.
            // Log.Information("\nDaylight saving time for year {0}:", currentYear );
            // Log.Information( "{0:yyyy-MM-dd HH:mm} to " + "{1:yyyy-MM-dd HH:mm}, delta: {2}", 
            //     daylight.Start, daylight.End, daylight.Delta );
        }
    }
}