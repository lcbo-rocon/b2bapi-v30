﻿
using AutoMapper;
using Dapper;
using Dapper.Oracle;
using Microsoft.Extensions.Logging;
using Oracle.ManagedDataAccess.Client;
using RoconLibrary.Domains;
using RoconLibrary.Models;
using RoconListener.Models;
using RoconLibrary.Utility;
using RoconLibrary.Services;
using RoconLibrary.Services.Implementation;
using Serilog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace RoconListener.Utility
{
    class SQLHelper
    {
        
        public static List<InvoiceInfo> GetInvoiceInfo(
                                                Order order,
                                                OrderStatusEntity orderStatusEntity,
                                                string sQuery,
                                                IDbConnection connection)
        {
            var invoiceInfos = new List<InvoiceInfo>();
            string orderID = orderStatusEntity.OrderNumber;

            Log.Debug($"=====> InvoiceInfo Order Id is: {orderID}");
            Log.Debug($"======> InvoiceInfo Stored procedure: {sQuery}");

            var param = new OracleDynamicParameters();
            param.Add("lv_sorOrderNumber", orderID);
            param.Add("lv_lco_status", orderStatusEntity.Status);
            param.Add("lv_prod_invoice", dbType: OracleMappingType.RefCursor, direction: ParameterDirection.Output);

            var invoiceInfoEntities = connection.Query<InvoiceInfoEntity>(sQuery,
                                                                        param: param,
                                                                        commandType: CommandType.StoredProcedure).ToList();

            foreach(InvoiceInfoEntity invoiceInfoEntity in invoiceInfoEntities)
            {
                var invoiceInfo = new InvoiceInfo();
                var systemIdentify =  new SystemIdentify();
                systemIdentify.systemIdentifier = "R";
                systemIdentify.systemName = "R:ROCON";
                invoiceInfo.InvoiceSystemInfo = systemIdentify;
                invoiceInfo.InvoiceNumber = invoiceInfoEntity.InvoiceNumber;
                invoiceInfo.InvoiceType = invoiceInfoEntity.InvoiceType;
                invoiceInfo.InvoiceTotalAmount = invoiceInfoEntity.InvoiceTotalAmount;
                invoiceInfo.InvoiceDiscountAmount = invoiceInfoEntity.InvoiceDiscountAmount;
                invoiceInfo.InvoiceBottleDepositAmount = invoiceInfoEntity.InvoiceBottleDepositAmount;
                invoiceInfo.InvoiceHstAmount = invoiceInfoEntity.InvoiceHstAmount;
                invoiceInfo.createdDate = FormatUtils.AddTimezone(invoiceInfoEntity.CreatedDate); 

                invoiceInfos.Add(invoiceInfo);   
            }
            
            return invoiceInfos;
        }

        public static PaymentEventInfo GetPaymentInfo(
                                                Order order,
                                                OrderStatusEntity orderStatusEntity,
                                                string sQuery,
                                                IDbConnection connection)
        {
            string orderID = orderStatusEntity.OrderNumber;

            Log.Debug($"=====> PaymentEventInfo Order Id is: {orderID}");

            Log.Debug($"======> PaymentEventInfo Stored procedure: {sQuery}");

            var param = new OracleDynamicParameters();
            param.Add("lv_sorOrderNumber", orderID);
            param.Add("lv_paymentData", dbType: OracleMappingType.RefCursor, direction: ParameterDirection.Output);

            var paymentInfoEntity = connection.Query<PaymentInfoEntity>(sQuery,
                                                                        param: param,
                                                                        commandType: CommandType.StoredProcedure).FirstOrDefault();
            var paymentEventInfo = new PaymentEventInfo();
            if (paymentInfoEntity != null)
            {
                PaymentTransaction[] paymentTransactions = new PaymentTransaction[1];
                PaymentTransaction paymentTransaction = new PaymentTransaction();
                var systemIdentify = new SystemIdentify();
                systemIdentify.systemIdentifier = paymentInfoEntity.LV_SOURCESYSTEMID;
                systemIdentify.systemName = paymentInfoEntity.LV_SOURCESYSTEMNAME;
                paymentTransaction.initiatingSystem = systemIdentify;
                paymentTransaction.paymentTenderType = paymentInfoEntity.LV_PAYMENTTENDERTYPE;
                paymentTransaction.paymentProcessingChannel = paymentInfoEntity.LV_PAYMENTCHANNEL;
                paymentTransaction.paymentDate = FormatUtils.AddTimezone(paymentInfoEntity.LV_PAYMENTDATE.ToString());
                // paymentTransaction.paymentTransactionId = "";
                // paymentTransaction.paymentConfirmationId = "";
                paymentTransaction.paymentAmount = Convert.ToDecimal(paymentInfoEntity.LV_PAYMENTAMOUNT);
                var paymentReference = new PaymentReference();
                paymentReference.referenceNumber = paymentInfoEntity.V_PAYREFERENCENUMBER;
                paymentTransaction.paymentReference = paymentReference;
                paymentTransactions[0] = paymentTransaction;
                paymentEventInfo.paymentTransactions = paymentTransactions;
            }
            else
            {
                paymentEventInfo = null;
            }
            return paymentEventInfo;
        }

        public static List<TenderInfo> GetTenderInfoAll(OrderStatusEntity orderStatusEntity, 
                                                            string sQuery,
                                                            IDbConnection connection)
        {
            var orderID = orderStatusEntity.OrderNumber;
            Log.Debug($"=====> TenderInfoAll Order Id is: {orderID}");
            Log.Debug($"======> TenderInfo Stored procedure: {sQuery}");

            var param = new OracleDynamicParameters();
            param.Add("lv_sorOrderNumber", orderID);
            param.Add("lv_tenderData", dbType: OracleMappingType.RefCursor, direction: ParameterDirection.Output);

            var tenderInfoEntities = connection.Query<TenderEventInfoEntity>(sQuery,
                                                                        param: param,
                                                                        commandType: CommandType.StoredProcedure).ToList();

            var tenderInfos = new List<TenderInfo>();

            foreach(TenderEventInfoEntity tenderInfoEntity in tenderInfoEntities)
            {
                var tenderIssuingSystem = new SystemIdentify();
                tenderIssuingSystem.systemIdentifier = tenderInfoEntity.LV_SOUCESYSTEMID;
                tenderIssuingSystem.systemName = tenderInfoEntity.LV_SOURCESYSTEMNAME;
                var tenderInfo = new TenderInfo();
                tenderInfo.tenderIssuingSystem = tenderIssuingSystem;

                tenderInfo.tenderId = tenderInfoEntity.LV_TENDERID;
                tenderInfo.tenderNotes = tenderInfoEntity.LV_TENDERCOMMENT;
                tenderInfo.tenderAmount = Convert.ToDouble(tenderInfoEntity.LV_TENDERAMOUNT);
                tenderInfo.tenderClass = tenderInfoEntity.LV_TENDERCLASS;
                tenderInfo.tenderType = tenderInfoEntity.LV_TENDERTYPE;
                tenderInfo.tenderDate = FormatUtils.AddTimezone(tenderInfoEntity.LV_TENDERDATE.ToString());
                tenderInfo.referenceDocumentType = tenderInfoEntity.LV_REFERENCEDOCUMENTTYPE;
                tenderInfo.referenceDocumentSubType = tenderInfoEntity.LV_REFERENCEDOCUMENTSUBTYPE;
                tenderInfo.referenceDocumentId = tenderInfoEntity.LV_REFERENCEDOCUMENTID;

                tenderInfos.Add(tenderInfo);
            }

            return tenderInfos;
        }

        public static List<TenderInfo> GetTenderInfo(
                                                Order order,
                                                OrderStatusEntity orderStatusEntity,
                                                string sQuery,
                                                IDbConnection connection)
        {
            var tenderInfos = new List<TenderInfo>();

            var tenderInfo = GetTenderInfo(sQuery, connection, orderStatusEntity.OrderNumber, "W", "DEBIT", "NEW");
            if (tenderInfo != null)
            {
                tenderInfos.Add(tenderInfo);
            }

            string[] statuses = { "I", "C" };
            bool orderNumberIC = Array.Exists(statuses, element => element == order.orderHeader.orderStatus.orderStatusCode);
            if (orderNumberIC)
            {
                tenderInfo = GetTenderInfo(sQuery, connection, orderStatusEntity.OrderNumber, "O", "CREDIT", "NEW");
                if (tenderInfo != null)
                {
                    tenderInfos.Add(tenderInfo);
                }
            }

            string [] statusesD = { "D" };
            bool orderNumberD = Array.Exists(statusesD, element => element == order.orderHeader.orderStatus.orderStatusCode);
            if (orderNumberD)
            {
                tenderInfo = GetTenderInfo(sQuery, connection, orderStatusEntity.OrderNumber, "W", "CREDIT", "NEW");
                if (tenderInfo != null)
                {
                    tenderInfos.Add(tenderInfo);
                }
            }

            return tenderInfos;
        }

        private static TenderInfo GetTenderInfo(
                                            string sQuery,
                                            IDbConnection connection,
                                            string orderID,
                                            string eventSourceId,
                                            string tenderType,
                                            string tenderStatus)
        {
            Log.Debug($"=====> TenderInfo Order Id is: {orderID}");
            Log.Debug($"Calling with the following values: [{eventSourceId}], [{tenderType}], [{tenderStatus}]");
            Log.Debug($"======> TenderInfo Stored procedure: {sQuery}");

            var param = new OracleDynamicParameters();
            param.Add("lv_sorOrderNumber", orderID);
            param.Add("lv_eventSourceId", eventSourceId);
            param.Add("lv_tenderType", tenderType);
            param.Add("lv_tenderStatus", tenderStatus);
            param.Add("lv_tenderData", dbType: OracleMappingType.RefCursor, direction: ParameterDirection.Output);

            var tenderInfoEntity = connection.Query<TenderEventInfoEntity>(sQuery,
                                                                        param: param,
                                                                        commandType: CommandType.StoredProcedure).FirstOrDefault();
            var tenderInfo = new TenderInfo();
            if (tenderInfoEntity != null)
            {
                var tenderIssuingSystem = new SystemIdentify();
                tenderIssuingSystem.systemIdentifier = tenderInfoEntity.LV_SOUCESYSTEMID;
                tenderIssuingSystem.systemName = tenderInfoEntity.LV_SOURCESYSTEMNAME;
                tenderInfo.tenderIssuingSystem = tenderIssuingSystem;

                tenderInfo.tenderId = tenderInfoEntity.LV_TENDERID;
                tenderInfo.tenderNotes = tenderInfoEntity.LV_TENDERCOMMENT;
                tenderInfo.tenderAmount = Convert.ToDouble(tenderInfoEntity.LV_TENDERAMOUNT);
                tenderInfo.tenderClass = tenderInfoEntity.LV_TENDERCLASS;
                tenderInfo.tenderType = tenderInfoEntity.LV_TENDERTYPE;
                tenderInfo.tenderDate = FormatUtils.AddTimezone(tenderInfoEntity.LV_TENDERDATE.ToString());
                tenderInfo.referenceDocumentType = tenderInfoEntity.LV_REFERENCEDOCUMENTTYPE;
                tenderInfo.referenceDocumentSubType = tenderInfoEntity.LV_REFERENCEDOCUMENTSUBTYPE;
                tenderInfo.referenceDocumentId = tenderInfoEntity.LV_REFERENCEDOCUMENTID;
            }
            else
            {
                tenderInfo = null;
            }
            return tenderInfo;
        }

        public static DeliveryCharge GetDeliveryCharge(
                                                        OrderStatusEntity orderStatusEntity,
                                                        string sQuery,
                                                        IDbConnection connection)
        {
            string orderID = orderStatusEntity.OrderNumber;
            Log.Debug($"=====> DeliveryCharge Order Id is: {orderID}");

            Log.Debug($"======> DeliveryCharge Stored procedure: {sQuery}");

            var param = new OracleDynamicParameters();
            param.Add("lv_sorOrderNumber", orderID);
            param.Add("lv_deliverycharge", dbType: OracleMappingType.RefCursor, direction: ParameterDirection.Output);

            var deliveryChargeInfoEntity = connection.Query<DeliveryChargeInfoEntity>(sQuery,
                                                                                    param: param,
                                                                                    commandType: CommandType.StoredProcedure).FirstOrDefault();
            var deliveryCharge = new DeliveryCharge();
            if (deliveryChargeInfoEntity != null)
            {
                var deliveryChargeReferenceInfo = new DeliveryChargeReferenceInfo();
                deliveryChargeReferenceInfo.referenceComment = "";
                deliveryChargeReferenceInfo.referenceIdentifier = deliveryChargeInfoEntity.deliveryReferenceId;
                deliveryChargeReferenceInfo.referenceIdentifierSubtype = deliveryChargeInfoEntity.deliveryReferenceIdSubType;
                deliveryChargeReferenceInfo.referenceIdentifierType = deliveryChargeInfoEntity.deliveryReferenceIdType;

                var systemIdentify = new SystemIdentify();
                systemIdentify.systemIdentifier = deliveryChargeInfoEntity.deliveryReferenceSysId;
                systemIdentify.systemName = deliveryChargeInfoEntity.deliveryReferenceSystem;

                deliveryChargeReferenceInfo.referenceSystem = systemIdentify;
                //DeliveryChargeReferenceInfo[] deliveryChargeReferenceInfos = new DeliveryChargeReferenceInfo[1];
                //deliveryChargeReferenceInfos[0] = deliveryChargeReferenceInfo;
                //deliveryCharge.deliveryChargeReferenceInfo = deliveryChargeReferenceInfos;
                deliveryCharge.deliveryChargeReferenceInfo = deliveryChargeReferenceInfo;

                systemIdentify = new SystemIdentify();
                deliveryCharge.deliveryChargeSourceId = deliveryChargeInfoEntity.deliveryChargeSourceid.ToString();
                systemIdentify.systemIdentifier = deliveryChargeInfoEntity.deliverySourceSysId;
                systemIdentify.systemName = deliveryChargeInfoEntity.deliveryReferenceSystem;
                deliveryCharge.deliveryChargeSourceInfo = systemIdentify;
                deliveryCharge.DeliveryChargeType = deliveryChargeInfoEntity.deliveryChargeType;
                deliveryCharge.DeliveryHstAmount = Convert.ToDecimal(deliveryChargeInfoEntity.deliveryHstAmount);
                deliveryCharge.DeliveryBaseAmount = Convert.ToDecimal(deliveryChargeInfoEntity.deliverBaseAmount);
                deliveryCharge.CreatedDate = FormatUtils.AddTimezone(deliveryChargeInfoEntity.createdDate.ToString());
            }
            else
            {
                deliveryCharge = null;
            }
            return deliveryCharge;
        }


        public static async Task<Order> GetOrderByIdAsync(OrderStatusEntity orderStatusEntity,
                                                     IDbConnection connection,
                                                     IMapper mapper)
        {
            var orderEntity = await getOrderEntity(orderStatusEntity, connection);
            // map order entity to order domain ...
            return mapper.Map<Order>(orderEntity);
        }

        private static async Task<OrderEntity> getOrderEntity(OrderStatusEntity orderStatusEntity,
                                                              IDbConnection connection)
        {
            string orderID = orderStatusEntity.OrderNumber;
            Log.Debug($"=====> Order Id is: {orderID}");
            Log.Debug($"=====> Status is: {orderStatusEntity.Status}");

            var sQuery = "LCBO.GET_ORDER_INFO_BY_STATUS_V31";

            Log.Debug($"======> Stored procedure: {sQuery}");

            var param = new OracleDynamicParameters();
            param.Add("lv_sorOrderNumber", orderID);
            param.Add("lv_lco_status", orderStatusEntity.Status);
            param.Add("lv_order", dbType: OracleMappingType.RefCursor, direction: ParameterDirection.Output);

            // var anonymousList = connection.Query(sQuery, param: param, commandType: CommandType.StoredProcedure).ToList();

            var results = await connection.QueryAsync<OrderHeaderEntity, RouteInfoEntity, OrderShipToInfoEntity,
                        OrderCommentsInfoEntity, PackingInvoiceCommentEntity, OrderHeaderEntity>
                        (sQuery, map: (header, route, ship, comment, packing) =>
                        {
                            header.routeInfo = route;
                            header.shipToInfo = ship;

                            if (comment != null)
                            {
                                header.orderCommentsInfo = comment;
                            }

                            if (packing != null && header.orderCommentsInfo != null)
                            {
                                header.orderCommentsInfo.PackingInvoiceComment = packing;
                            }

                            //header.invoiceInfo = inv;
                            //header.invoiceInfo.DeliveryCharge = delivery;
                            return header;
                        }
                            , splitOn: "routeCode,name,bolcomment,textLine1"
                            , param: param, commandType: CommandType.StoredProcedure);

            var orderEntity = new OrderEntity();

            var orderHeaderInfo = results.SingleOrDefault();

            orderEntity.orderHeader = orderHeaderInfo;
            
            var orderItems = await GetOrderItemDetails(orderStatusEntity, connection);

            orderEntity.orderItems = orderItems.ToArray();

            if (orderEntity.orderHeader == null)
            {
                throw new Exception($"The order header was null for order id [{orderID}] and status [{orderStatusEntity.Status}] for query [{sQuery}]");
            }
            Log.Information("*** Calling order status");
            setStatusDetail(orderEntity, orderStatusEntity, connection);
            setReferenceInfo(orderEntity, orderStatusEntity, connection);

            //Please add Delivery Charge Entity here
            //Please add Invoice entity here

            return orderEntity;
        }

        private static void setStatusDetail(OrderEntity orderEntity,
                                        OrderStatusEntity orderStatusEntity,
                                        IDbConnection connection)
        {
            var sql = "SELECT STATUS_DT_TM, REF_VALUE FROM LCBO.LCO_ORDER_STATUS OS"
                    + " INNER JOIN LCBO.LCO_API_REFERENCE AREF ON AREF.REF_KEY = :STATUS_KEY"
                    + " AND AREF.REF_TYPE = 'ORDERSTATUS'"
                    + " WHERE OS.CO_ODNO = rpad(:ORDER_NO, 16)"
                    + " AND OS.STATUS = :STATUS_KEY";
            Log.Information($"*** Status sql: {sql}");
            var command = new OracleCommand(sql, (OracleConnection)connection);
            command.Parameters.Add(new OracleParameter("STATUS_KEY", orderStatusEntity.Status));
            command.Parameters.Add(new OracleParameter("ORDER_NO", orderStatusEntity.OrderNumber));
            var oraclReader = command.ExecuteReader();

            while (oraclReader.Read())
            {
                Log.Information("**** Setting order status detail ....");
                orderEntity.orderHeader.orderStatus = new OrderStatusCodeDetailEntity();
                orderEntity.orderHeader.orderStatus.orderStatusCode = orderStatusEntity.Status;
                orderEntity.orderHeader.orderStatus.orderStatusName = ((string)oraclReader["REF_VALUE"]).Trim();
                orderEntity.orderHeader.orderStatus.statusCreateTime = FormatUtils.AddTimezone(((DateTime)oraclReader["STATUS_DT_TM"]).ToString());
            }
            oraclReader.Close();
        }

        private static void setReferenceInfo(OrderEntity orderEntity,
                                        OrderStatusEntity orderStatusEntity,
                                        IDbConnection connection)
        {
            var cmd = new OracleCommand();
            cmd.Connection = (OracleConnection)connection;
            cmd.CommandText = "LCBO.GET_REFERENCEINFO_V3";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("lv_sorOrderNumber", OracleDbType.NVarchar2).Value = orderStatusEntity.OrderNumber;
            cmd.Parameters.Add("lv_lco_status", OracleDbType.NVarchar2).Value = orderStatusEntity.Status;

            OracleParameter oraP = new OracleParameter();
            oraP.ParameterName = "ref_refinfo";
            oraP.OracleDbType = OracleDbType.RefCursor;
            oraP.Direction = System.Data.ParameterDirection.Output;
            cmd.Parameters.Add(oraP);

            var oraclReader = cmd.ExecuteReader();

            var referenceInfoEntities = new List<ReferenceInfoEntity>();
            Log.Information("\n ====> Setting order reference info ....");

            while (oraclReader.Read())
            {
                Log.Information("**** Looping order reference info ....");
                var referenceInfoEntity = new ReferenceInfoEntity();
                referenceInfoEntity.referenceSystem = new SystemIdentifyEntity();
                referenceInfoEntity.referenceSystem.SystemIdentifier = ((string)oraclReader["systemIdentifier"]).Trim();
                referenceInfoEntity.referenceSystem.SystemName = ((string)oraclReader["systemName"]).Trim();
                referenceInfoEntity.referenceIdentifier = ((string)oraclReader["referenceIdentifier"]).Trim();
                referenceInfoEntity.referenceIdentifierSubtype = ((string)oraclReader["referenceIdentifierSubtype"]).Trim();
                referenceInfoEntity.referenceIdentifierType = ((string)oraclReader["referenceIdentifierType"]).Trim();

                referenceInfoEntity.referenceComment = "";
                if (!(oraclReader["referenceComment"] is DBNull))
                {
                    referenceInfoEntity.referenceComment = ((string)oraclReader["referenceComment"]).Trim();
                }
                referenceInfoEntities.Add(referenceInfoEntity);
            }
            oraclReader.Close();

            Log.Information("\n ====> After Setting order reference info ...." + referenceInfoEntities.Count);
            orderEntity.orderHeader.referenceInfo = referenceInfoEntities.ToArray();
        }

        private static async Task<IEnumerable<OrderItemsEntity>> GetOrderItemDetails(OrderStatusEntity orderStatusEntity,
                                                                        IDbConnection connection)
        {
            string RocOrderID = orderStatusEntity.OrderNumber;
            string OrderInvType = getOrderEventType(orderStatusEntity);

            var sQuery = "LCBO.GET_ORDER_DETAIL_BY_STATUS_V31";

            var param = new OracleDynamicParameters();
            param.Add("lv_sorOrderNumber", RocOrderID);
            param.Add("lv_lco_status", orderStatusEntity.Status);
            param.Add("lv_orderDetails", dbType: OracleMappingType.RefCursor, direction: ParameterDirection.Output);

            // var anonymousList = connection.Query(sQuery, param: param, commandType: CommandType.StoredProcedure).ToList();

            var results = await connection.QueryAsync<OrderItemsEntity, OrderItemPriceInfoEntity, OrderItemsEntity>(sQuery, map: (item, price) =>
            {
                item.priceinfo = price;
                item.itemLineTotal = price.sellingPrice * item.shippedQuantity;
                return item;
            }, splitOn: "sellingPrice", param: param, commandType: CommandType.StoredProcedure);

            return results.ToArray();
        }

        private static string getOrderEventType(OrderStatusEntity orderStatusEntity)
        {
            var orderEventType = "E";
            if (string.Equals(orderStatusEntity.Status, "I") || string.Equals(orderStatusEntity.Status, "C"))
            {
                orderEventType = "A";
            }
            return orderEventType;
        }

        public static void updateDatabase(OrderStatusEntity orderStatus,
            OracleConnection connection)
        {
            ConnectionUtil.openConnection(connection);
            var dateStr = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            Log.Debug("LCO_ORDER_STATUS Date string: " + dateStr);
            var sql = $"Update LCO_ORDER_STATUS set EXTRACT_DT_TM = TO_DATE('{dateStr}', 'YYYY-MM-DD HH24:mi:ss') WHERE CO_ODNO = '{orderStatus.OrderNumber}' AND STATUS = '{orderStatus.Status}'";
            // Log.Information("Update sql: " + sql);
            var oraclCmd = connection.CreateCommand();
            oraclCmd.CommandText = sql;
            int updated = oraclCmd.ExecuteNonQuery();
            Log.Debug("LCO_ORDER_STATUS Quantity updated: " + updated);
        }

        public static void insertToLcoOrderJson(Order order,
                                            string jsonRequest,
                                            string jsonResponse,
                                            string returnCode,
                                            string comments,
                                            OracleConnection connection)
        {
            // Log.Information($"==> Json request: {jsonRequest}");
            ConnectionUtil.openConnection(connection);
            var dateStr = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            Log.Debug("Date string: " + dateStr);
            var messageSource = "RoconListener";
            var messageInOut = "";
            comments = "V3.1:" + comments;

            string sql = "Insert into LCO_ORDER_JSON (LCOORDERNUMBER, SORORDERNUMBER, JSON_BODY, LOAD_DT_TM, MESSAGE_SOURCE, MESSAGE_INOUT, RETURN_CODE, RETURN_JSON, COMMENTS) values " +
              $"('{order.orderHeader.lcoOrderNumber}', '{order.orderHeader.sorOrderNumber}', :JSON_IN, TO_DATE('{dateStr}', 'YYYY-MM-DD HH24:MI:SS'), '{messageSource}'" +
              $", '{messageInOut}', '{returnCode}', :JSON_OUT, '{comments}')";

            Log.Debug($"====> The insert sql is:\n {sql}");
            var oraclCmd = connection.CreateCommand();
            oraclCmd.CommandText = sql;
            oraclCmd.Parameters.Clear();
            oraclCmd.Parameters.Add(":JSON_IN", jsonRequest);
            oraclCmd.Parameters.Add(":JSON_OUT", jsonResponse);
            int inserted = oraclCmd.ExecuteNonQuery();
            Log.Debug("Quantity inserted: " + inserted);
        }

        public static void logToErrorService(string errorInfo, OracleConnection connection)
        {
            ILcoErrorLogService lcoErrorLogService = new LcoErrorLogService(connection);
            var lcoErrorLog = new LcoErrorLog();
            lcoErrorLog.lcoService = "RoconListener";
            lcoErrorLog.lcoModule = "ListenerOrderService";
            lcoErrorLog.errorInfo = errorInfo;
            lcoErrorLog.createdBy = "System";
            lcoErrorLog.errorCode = "002";
            lcoErrorLogService.insertToTable(lcoErrorLog);
        }

        public static void logToErrorService(LcoErrorLog lcoErrorLog, OracleConnection connection)
        {
            ILcoErrorLogService lcoErrorLogService = new LcoErrorLogService(connection);
            lcoErrorLogService.insertToTable(lcoErrorLog);
        }
    }

}
