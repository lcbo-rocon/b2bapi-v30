# README #

### Create RoconListener executable at the RoconListener directory ###

dotnet publish -c Release -r win10-x64

### Deploy to Win065 ###

* *copy* bin\Release\netcoreapp2.1\win10-x64\publish to Y:\Scripts\ROCON\ROCONListener
* In Win065 open command prompt and set the KAFKA_AUTH token value
* cd Y:\Scripts\ROCON\ROCONListener and run roconlistener.exe
