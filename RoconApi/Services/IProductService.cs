﻿using Microsoft.AspNetCore.Mvc;
using RoconLibrary.Domains;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RoconApi.Services
{
    public interface IProductService
    {
        Task<IEnumerable<Product>> GetProducts(string custType, string itemList);

        Task<IEnumerable<ProductInventory>> GetProductInventory(string custType, string itemList);
       // Task<Product> GetProductByIdAsync(string custType, int sku);
    }
}
