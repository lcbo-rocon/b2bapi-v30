﻿using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using System.Linq;
using Microsoft.Extensions.Configuration;
using Dapper.Oracle;
using Dapper;
using AutoMapper;
using System.Collections.Generic;
using System;
using RoconLibrary.Domains;
using RoconLibrary.Models;
using RoconLibrary.Utility;
//using RoconApi.Domains;
//using RoconApi.Models;

using System.Transactions;

namespace RoconApi.Services.Implementation
{
    public class OrderService : IOrderService
    {
        private readonly IConfiguration _configuration;
        private readonly IMapper _mapper;

        public OrderService(IConfiguration configuration, IMapper mapper)
        {
            _configuration = configuration;
            _mapper = mapper;
        }
        public IDbConnection Connection
        {
            get
            {
                return new OracleConnection(_configuration.GetConnectionString("roconconnection"));
            }
        }

        // Silver 2.0, reserve inventory when WCS send order request
        // V3.1, create E Delivery Inv and return back to WCS.
        public Task<Order> ReserveInventory(Order order, IEnumerable<ProductInventory> productList)
        {
            // using (var tran = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))

            IDbConnection conn = Connection;
            conn.Open();

            //--set up price info
            foreach (var curitem in order.orderItems)
            {
                var ProdInv = productList.Where(p => p.sku == curitem.sku).First();
                curitem.priceInfo = _mapper.Map<OrderItemPriceInfo>(ProdInv.priceInfo);
                curitem.item_category = ProdInv.item_category;
            }

            bool isSuccess = false;
            var sErrorList = new List<ErrorDetail>();
            int newOrderNumber = 0;
            string storeProc = "";
            int nextInvNo = 0;
            int newDROrderNumber = 0;

            using (var tran = conn.BeginTransaction())
            {
                //--Insert into dbo.LCO_Orders for new order
                storeProc = "LCBO.RESERVE_LCO_ORDER_HEADER_V2";

                var param = new OracleDynamicParameters();
                param.Add("lv_lcoOrderNumber", order.orderHeader.lcoOrderNumber, dbType: OracleMappingType.Int32, direction: ParameterDirection.Input);
                param.Add("lv_orderRequestedDate", FormatUtils.ConvertToYYYYMMDDHHMISS(order.orderHeader.orderRequestDate), dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                param.Add("lv_orderSource", order.orderHeader.orderSourceInfo.systemIdentifier, dbType: OracleMappingType.Char, direction: ParameterDirection.Input);
                param.Add("lv_orderSourceName", order.orderHeader.orderSourceInfo.systemName, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                param.Add("lv_overwriteShipToInformation", order.orderHeader.shipToInfo.overwriteShipToInformation, dbType: OracleMappingType.Int32, direction: ParameterDirection.Input);
                param.Add("lv_overwriteShipToAddress1", order.orderHeader.shipToInfo.addressLine1, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input, isNullable: true);
                param.Add("lv_overwriteShipToAddress2", order.orderHeader.shipToInfo.addressLine2, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input, isNullable: true);
                param.Add("lv_overwriteShipToCity", order.orderHeader.shipToInfo.city, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input, isNullable: true);
                param.Add("lv_overwriteShipToProvinceCode", order.orderHeader.shipToInfo.provinceCode, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input, isNullable: true);
                param.Add("lv_overwriteShipToPhoneNumber", order.orderHeader.shipToInfo.phoneNumber, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input, isNullable: true);
                param.Add("lv_overwriteShipToName", order.orderHeader.shipToInfo.name, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input, isNullable: true);
                param.Add("lv_overwriteShipToCountry", order.orderHeader.shipToInfo.country, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input, isNullable: true);
                param.Add("lv_overwriteShipToPostalCode", order.orderHeader.shipToInfo.postalCode, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input, isNullable: true);
                param.Add("lv_DeliveryType", order.orderHeader.deliveryType, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input, isNullable: false);
                param.Add("lv_routeCode", order.orderHeader.routeInfo.routeCode, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input, isNullable: false);
                param.Add("lv_routeStop", order.orderHeader.routeInfo.routeStops, dbType: OracleMappingType.Int32, direction: ParameterDirection.Input);
                //param.Add("lv_order_priority", DBNull.Value); // order.orderHeader.orderPriority);
                param.Add("lv_customerNumber", order.orderHeader.customerNumber, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                param.Add("lv_orderRequiredDate", FormatUtils.ConvertToYYYYMMDD(order.orderHeader.orderRequiredDate), dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input, isNullable: false);
                param.Add("lv_orderShipmentDate", FormatUtils.ConvertToYYYYMMDD(order.orderHeader.orderShipmentDate), dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input, isNullable: false);
                param.Add("lv_orderType", order.orderHeader.orderType, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input, isNullable: false);
                param.Add("lv_ShiptoName", order.orderHeader.shipToInfo.name, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input, isNullable: true);
                param.Add("lv_em_no", "LCOWOO", dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                param.Add("lv_sorOrderNumber", dbType: OracleMappingType.Int32, direction: ParameterDirection.Output);

                param.Add("lv_lcoSysIdentifier", order.orderHeader.lcoOrderSystem.systemIdentifier, dbType: OracleMappingType.Char, direction: ParameterDirection.Input);
                param.Add("lv_lcoSysName", order.orderHeader.lcoOrderSystem.systemName, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);

                //--Silver V2.1 fields, to store # of Bags in PackingInvoiceComments.
                if (order.orderHeader.orderCommentsInfo == null)
                {
                    order.orderHeader.orderCommentsInfo = new OrderCommentsInfo();
                }
                if (order.orderHeader.orderCommentsInfo.PackingInvoiceComment == null)
                {
                    order.orderHeader.orderCommentsInfo.PackingInvoiceComment = new PackingInvoiceComment();
                }

                param.Add("lv_packingInvCommLine1", order.orderHeader.orderCommentsInfo.PackingInvoiceComment.TextLine1, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input, isNullable: true);
                param.Add("lv_packingInvCommLine2", order.orderHeader.orderCommentsInfo.PackingInvoiceComment.TextLine2, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input, isNullable: true);
                param.Add("lv_packingInvCommLine3", order.orderHeader.orderCommentsInfo.PackingInvoiceComment.TextLine3, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input, isNullable: true);

                if (order.orderHeader.referenceInfo != null)
                {
                    var refInfos = order.orderHeader.referenceInfo.ToArray();
                    int refNum = 1;
                    string refParm = "lv_ref";
                    foreach (var refInf in refInfos)
                    {
                        param.Add(refParm + refNum.ToString() + "_SysID", refInf.referenceSystem.systemIdentifier, dbType: OracleMappingType.Char, direction: ParameterDirection.Input);
                        param.Add(refParm + refNum.ToString() + "_SysName", refInf.referenceSystem.systemName, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);

                        //--order# in diff system
                        param.Add(refParm + refNum.ToString() + "_Identifier", refInf.referenceIdentifier, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                        param.Add(refParm + refNum.ToString() + "_IdentifierType", refInf.referenceIdentifierType, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                        param.Add(refParm + refNum.ToString() + "_IdentifierSubtype", refInf.referenceIdentifierSubtype, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                        param.Add(refParm + refNum.ToString() + "_Comment", refInf.referenceComment, dbType: OracleMappingType.Varchar2, isNullable: true, direction: ParameterDirection.Input);
                        refNum = refNum + 1;
                    }
                }

                try
                {
                    conn.Execute(storeProc, param: param, commandType: CommandType.StoredProcedure);
                    newOrderNumber = Decimal.ToInt32(param.Get<Decimal>("lv_sorOrderNumber"));

                    //--insert items to Rocon by SKU number so Rocon Product Invoice can order by SKU.
                    //--linenumber in ROCON will be different than WCS orders
                    //--OMS need WCS line # as key to check shortship qty. cannot change linenumber in Rocon. Change back to save order in WCS line #
                    //var sortedOrderItems = order.orderItems.OrderBy(p => p.sku);
                    var sortedOrderItems = order.orderItems.OrderBy(p => p.lineNumber);

                    //--loop through items to secure the inventory
                    int orderedLineNumbers = 0;
                    foreach (var itm in sortedOrderItems.ToArray())
                    {
                        storeProc = "LCBO.RESERVE_LCO_ORDER_DETAILS";

                        var paramList = new OracleDynamicParameters();
                        paramList.Add("lv_co_odno", newOrderNumber.ToString(), dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                        paramList.Add("lv_cod_line", itm.lineNumber, dbType: OracleMappingType.Int16, direction: ParameterDirection.Input);
                        paramList.Add("lv_item", itm.sku.ToString(), dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                        paramList.Add("lv_cod_qty", itm.quantity, dbType: OracleMappingType.Int16, direction: ParameterDirection.Input);
                        paramList.Add("lv_total_allocated", itm.quantity, dbType: OracleMappingType.Int16, direction: ParameterDirection.Input);
                        paramList.Add("lv_unit_price", itm.priceInfo.unit_price, dbType: OracleMappingType.Decimal, direction: ParameterDirection.Input);
                        paramList.Add("lv_co_status", "W", dbType: OracleMappingType.Char, direction: ParameterDirection.Input);
                        paramList.Add("lv_em_no", "LCOWOO", dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                        paramList.Add("lv_rows_affected", dbType: OracleMappingType.Int32, direction: ParameterDirection.Output);

                        conn.Execute(storeProc, param: paramList, commandType: CommandType.StoredProcedure);

                        var rowsaffected = Decimal.ToInt32(paramList.Get<Decimal>("lv_rows_affected"));

                        if (rowsaffected == 1)
                        {
                            orderedLineNumbers = orderedLineNumbers + 1;
                        }
                    }
                    //--Sanity check that number of items have been inserted into ON_CODTL Table
                    //--V3.1, somehow it will not hit Rollback action. Need to enhance.
                    if (orderedLineNumbers != order.orderItems.Count())
                    {
                        isSuccess = false;
                        sErrorList.Add(new ErrorDetail()
                        {
                            errorType = "INVALID_ORDER",
                            errorMessage = "Count of order items is incorrect.",
                            errorData = new ErrorDetail.DataValue()
                            {
                                key = ErrorType.OTHER.ToString(),
                                value = orderedLineNumbers.ToString()
                            }
                        });
                        throw new Exception();
                    }

                    //--Commit to get SORorderNumber so Delivery Inv can have Link_co_no.

                    tran.Commit();
                    isSuccess = true;
                }
                catch (OracleException ex)
                {
                    isSuccess = false;
                    if (ex.Number == 1422)
                    {
                        sErrorList.Add(new ErrorDetail()
                        {
                            errorType = "INVALID_ORDER",
                            errorMessage = order.orderHeader.lcoOrderNumber + " already exist!",
                            errorData = new ErrorDetail.DataValue()
                            {
                                key = ErrorType.OTHER.ToString(),
                                value = order.orderHeader.lcoOrderNumber.ToString()
                            }
                        });
                    }
                    else
                    {
                        sErrorList.Add(new ErrorDetail()
                        {
                            errorType = "OTHERS",
                            errorMessage = "Unhandling exception occurred.",
                            errorData = new ErrorDetail.DataValue()
                            {
                                key = ErrorType.OTHER.ToString(),
                                value = ex.Message.ToString()
                            }
                        });
                    }
                    order.errorInfo = sErrorList.ToArray();
                }
                catch (Exception ex)
                {
                    isSuccess = false;
                    sErrorList.Add(new ErrorDetail()
                    {
                        errorType = "OTHERS",
                        errorMessage = "Unhandling exception occurred.",
                        errorData = new ErrorDetail.DataValue()
                        {
                            key = ErrorType.OTHER.ToString(),
                            value = ex.Message.ToString()
                        }
                    });
                    order.errorInfo = sErrorList.ToArray();
                }
                finally
                { if (!isSuccess) tran.Rollback(); }
            }

            //--V3.1, continue to create E Deliv Inv Num if Order num can be created, return to WCS. Cannot rollback.
            if (isSuccess)
            {
                using (var tran = conn.BeginTransaction())
                {
                    var param = new OracleDynamicParameters();
                    param.Add("lv_nextInvNo", dbType: OracleMappingType.Int32, direction: ParameterDirection.Output);
                    storeProc = "LCBO.GET_NEXT_INVOICE_NO";
                    try
                    {
                        conn.Execute(storeProc, param: param, commandType: CommandType.StoredProcedure);
                        nextInvNo = Decimal.ToInt32(param.Get<Decimal>("lv_nextInvNo"));
                        //No rolling back as this can impact other users getting an inv_no from ROCON
                        tran.Commit();
                        isSuccess = true;
                    }
                    catch (Exception ex)
                    {
                        isSuccess = false;
                        sErrorList.Add(new ErrorDetail()
                        {
                            errorType = "INVALID_ORDER",
                            errorMessage = "ERROR Generating Delivery Charge Invoice Number",
                            errorData = new ErrorDetail.DataValue()
                            {
                                key = ErrorType.OTHER.ToString(),
                                value = ex.Message.ToString()
                            }
                        });
                        order.errorInfo = sErrorList.ToArray();
                    }
                }
            }

            //--V3.1, create E Delivery Charge order and invoice, return back to WCS
            if (isSuccess)
            {
                if (order.orderHeader.deliveryCharges == null)
                {
                    var dcs = new List<DeliveryCharge>();
                    order.orderHeader.deliveryCharges = dcs.ToArray();
                }

                using (var tran = conn.BeginTransaction())
                {
                    try
                    {
                        //var drItemFound = order.orderHeader.deliveryCharges.Any(p => p.DeliveryChargeType == "E");
                        //if (!drItemFound) { --Raise error}

                        var drItemFound = order.orderHeader.deliveryCharges.Where(p => p.DeliveryChargeType == "E");                        
                        if (drItemFound?.Any() == false)
                        {
                            isSuccess = false;
                            sErrorList.Add(new ErrorDetail()
                            {
                                errorType = ErrorType.MISSING_ELEMENT.ToString(),
                                errorMessage = "Missing Estimated Delivery Charge Invoice. "
                            });
                            order.errorInfo = sErrorList.ToArray();
                        }

                        if (isSuccess)
                        {
                            var drItem = order.orderHeader.deliveryCharges.Where(p => p.DeliveryChargeType == "E").First();
                            //V3.1, need to enhance to consider if not E delivery Inv from WCS

                            storeProc = "LCBO.RESERVE_LCO_DELIVERY";
                            var paramDR = new OracleDynamicParameters();
                            paramDR.Add("lv_sorOrderNumber", newOrderNumber.ToString(), dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                            paramDR.Add("lv_createDate", FormatUtils.ConvertToYYYYMMDDHHMISS(drItem.CreatedDate), dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                            paramDR.Add("lv_deliveryBaseAmount", drItem.DeliveryBaseAmount, dbType: OracleMappingType.Decimal, direction: ParameterDirection.Input);
                            paramDR.Add("lv_deliveryHSTAmount", drItem.DeliveryHstAmount, dbType: OracleMappingType.Decimal, direction: ParameterDirection.Input);
                            paramDR.Add("lv_fullCases", order.orderHeader.numberOfFullCases, dbType: OracleMappingType.Int16, direction: ParameterDirection.Input);
                            paramDR.Add("lv_partCases", order.orderHeader.numberOfPartialCases, dbType: OracleMappingType.Int16, direction: ParameterDirection.Input);
                            paramDR.Add("lv_invNumber", nextInvNo, dbType: OracleMappingType.Int32, direction: ParameterDirection.Input);
                            paramDR.Add("lv_sourceSystemId", drItem.deliveryChargeSourceInfo.systemIdentifier, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                            paramDR.Add("lv_deliveryRateId", Convert.ToInt32(drItem.deliveryChargeSourceId), dbType: OracleMappingType.Int32, direction: ParameterDirection.Input);
                            paramDR.Add("lv_sorDeliveryNumber", dbType: OracleMappingType.Int32, direction: ParameterDirection.Output);
                            paramDR.Add("lv_returnCode", dbType: OracleMappingType.Int32, direction: ParameterDirection.Output);

                            conn.Execute(storeProc, param: paramDR, commandType: CommandType.StoredProcedure);
                            var returnCodeDR = Decimal.ToInt32(paramDR.Get<Decimal>("lv_returnCode"));
                            newDROrderNumber = Decimal.ToInt32(paramDR.Get<Decimal>("lv_sorDeliveryNumber"));

                            if (returnCodeDR == 0)
                            {
                                isSuccess = true;
                                tran.Commit();
                            }
                            else
                            {
                                isSuccess = false;
                                var sb = new System.Text.StringBuilder();
                                string errTable = "";
                                switch (returnCodeDR)
                                {
                                    case 1:
                                        errTable = "LCBO.LCO_DELIVERY_CHARGE";
                                        break;
                                    case 2:
                                        errTable = "DBO.ON_INVOICE";
                                        break;
                                    default:
                                        errTable = "Unknown";
                                        break;
                                }
                                sb.AppendLine("Store Proc error: LCBO.RESERVE_LCO_DELIVERY" + " | Return Code: " + returnCodeDR + " | Table: " + errTable);
                                throw new Exception(sb.ToString());
                            }
                        }
                        //tran.Commit();
                    }
                    catch (OracleException ex)
                    {
                        isSuccess = false;
                        sErrorList.Add(new ErrorDetail()
                        {
                            errorType = "OTHERS",
                            errorMessage = "Unhandling exception occurred.",
                            errorData = new ErrorDetail.DataValue()
                            {
                                key = ErrorType.OTHER.ToString(),
                                value = ex.Message.ToString()
                            }
                        });
                        order.errorInfo = sErrorList.ToArray();
                    }
                    catch (Exception ex)
                    {
                        isSuccess = false;
                        sErrorList.Add(new ErrorDetail()
                        {
                            errorType = "OTHERS",
                            errorMessage = "Unhandling exception occurred.",
                            errorData = new ErrorDetail.DataValue()
                            {
                                key = ErrorType.OTHER.ToString(),
                                value = ex.Message.ToString()
                            }
                        });
                        order.errorInfo = sErrorList.ToArray();
                    }
                    finally
                    {
                        if (!isSuccess)
                        {
                            tran.Rollback();
                            //Need to cancel the W order
                            using (var tran1 = conn.BeginTransaction())
                            {
                                storeProc = "LCBO.CANCEL_LCO_ORDER_V3";
                                var parm1 = new OracleDynamicParameters();
                                parm1.Add("lv_orderNumber", newOrderNumber, dbType: OracleMappingType.Int32, direction: ParameterDirection.Input);
                                parm1.Add("lv_ReturnCode", dbType: OracleMappingType.Int32, direction: ParameterDirection.Output);
                                parm1.Add("lv_returnCancel_DT_TM", dbType: OracleMappingType.Date, direction: ParameterDirection.Output);
                                conn.Execute(storeProc, param: parm1, commandType: CommandType.StoredProcedure);
                                tran1.Commit();
                            }
                        }
                    }
                }
            }

            //--If all success, populate delivery reference, product inv info to order payload.
            if (isSuccess)
            {
                order.orderHeader.sorOrderNumber = Decimal.ToInt32(newOrderNumber);
                if (order.orderHeader.sorSystem == null)
                {
                    order.orderHeader.sorSystem = new SystemIdentify();
                    order.orderHeader.sorSystem.systemIdentifier = "R";
                    order.orderHeader.sorSystem.systemName = "R:ROCON";
                }
                order.orderHeader.orderSourceInfo.systemIdentifier = "R";
                order.orderHeader.orderSourceInfo.systemName = "R:ROCON";

                //--V3.1, populate Delivery Invoice Reference Info to WCS
                var dcRefList = new DeliveryChargeReferenceInfo()
                {
                    referenceIdentifier = newDROrderNumber.ToString(),
                    referenceIdentifierType = "INVOICE_NUMBER",
                    referenceIdentifierSubtype = "DELIVERY_CHARGE",
                    referenceComment = "",
                    referenceSystem = new SystemIdentify()
                    {
                        systemIdentifier = "R",
                        systemName = "R:ROCON"
                    }
                };
                foreach (var curDC in order.orderHeader.deliveryCharges)
                {
                    if (curDC.DeliveryChargeType == "E")
                    {
                        curDC.deliveryChargeReferenceInfo = dcRefList;
                    }
                };

                //--V3.1, build orderHeader E prod invoice totals for W status, to return back to WCS for estimate use, not saving to Rocon DB.
                if (order.orderHeader.invoiceInfo == null)
                {
                    order.orderHeader.invoiceInfo = new InvoiceInfo();
                }
                if (order.orderHeader.invoiceInfo.InvoiceSystemInfo == null)
                {
                    order.orderHeader.invoiceInfo.InvoiceSystemInfo = new SystemIdentify();
                }
                order.orderHeader.invoiceInfo.InvoiceSystemInfo.systemIdentifier = "R";
                order.orderHeader.invoiceInfo.InvoiceSystemInfo.systemName = "R:ROCON";
                order.orderHeader.invoiceInfo.InvoiceNumber = order.orderHeader.sorOrderNumber.ToString();
                order.orderHeader.invoiceInfo.InvoiceType = "E";
                order.orderHeader.invoiceInfo.InvoiceTotalAmount = order.orderItems.Sum(p => p.priceInfo.selling_price * p.quantity);
                order.orderHeader.invoiceInfo.InvoiceDiscountAmount = order.orderItems.Sum(p => p.priceInfo.discount * p.quantity);
                order.orderHeader.invoiceInfo.InvoiceBottleDepositAmount = order.orderItems.Sum(p => p.priceInfo.bottle_deposit * p.quantity);
                order.orderHeader.invoiceInfo.InvoiceHstAmount = order.orderItems.Sum(p => p.priceInfo.hst_tax * p.quantity);
                order.orderHeader.invoiceInfo.InvoiceLicMuAmount = order.orderItems.Sum(p => p.priceInfo.liquor_markup * p.quantity);
                order.orderHeader.invoiceInfo.InvoiceLevyAmount = 0m;
                order.orderHeader.invoiceInfo.InvoiceRetailAmount = order.orderItems.Sum(p => p.priceInfo.retail_price * p.quantity);
                order.orderHeader.invoiceInfo.createdDate = DateTime.Now.ToString();
            }

            return Task.FromResult(order);
        }

        // Silver 2.0, change Order status to be O:Open when OMS send order request.
        // V3.1, commit payment, tender for order.
        public Task<Order> CreateOrder(Order order, IEnumerable<ProductInventory> productList)
        {
            // -- V3.0, remove the check from ProdInv since available Prod list might change between W -> O. 
            // -- Should not stop order flow by RoconAPI. Business user can zero ship the unvaliable item from Rocon GUI. 
            //foreach (var curitem in order.orderItems)
            //{
            //    var ProdInv = productList.Where(p => p.sku == curitem.sku).First();
            //    curitem.priceInfo = _mapper.Map<OrderItemPriceInfo>(ProdInv.priceInfo);
            //    curitem.item_category = ProdInv.item_category;
            //}

            //--build orderHeader invoice totals
            if (order.orderHeader.invoiceInfo == null)
            {
                order.orderHeader.invoiceInfo = new InvoiceInfo();
            }
            if (order.orderHeader.invoiceInfo.InvoiceSystemInfo == null)
            {
                order.orderHeader.invoiceInfo.InvoiceSystemInfo = new SystemIdentify();
            }
            order.orderHeader.invoiceInfo.InvoiceSystemInfo.systemIdentifier = "R";
            order.orderHeader.invoiceInfo.InvoiceSystemInfo.systemName = "R:ROCON";
            order.orderHeader.invoiceInfo.InvoiceNumber = order.orderHeader.sorOrderNumber.ToString();
            order.orderHeader.invoiceInfo.InvoiceType = "E";
            order.orderHeader.invoiceInfo.InvoiceTotalAmount = order.orderItems.Sum(p => p.priceInfo.selling_price * p.quantity);
            order.orderHeader.invoiceInfo.InvoiceDiscountAmount = order.orderItems.Sum(p => p.priceInfo.discount * p.quantity);
            order.orderHeader.invoiceInfo.InvoiceBottleDepositAmount = order.orderItems.Sum(p => p.priceInfo.bottle_deposit * p.quantity);
            order.orderHeader.invoiceInfo.InvoiceHstAmount = order.orderItems.Sum(p => p.priceInfo.hst_tax * p.quantity);
            order.orderHeader.invoiceInfo.InvoiceLicMuAmount = order.orderItems.Sum(p => p.priceInfo.liquor_markup * p.quantity);
            order.orderHeader.invoiceInfo.InvoiceLevyAmount = 0m;
            order.orderHeader.invoiceInfo.InvoiceRetailAmount = order.orderItems.Sum(p => p.priceInfo.retail_price * p.quantity);
            //order.orderHeader.invoiceInfo.createdDate = ??

            if (order.orderHeader.deliveryCharges == null)
            {
                var dcs = new List<DeliveryCharge>();
                order.orderHeader.deliveryCharges = dcs.ToArray();
            }

            if (order.orderHeader.deliveryType == DeliveryType.PICK_UP.ToString())
            {
                if (order.orderHeader.orderCommentsInfo == null)
                {
                    order.orderHeader.orderCommentsInfo = new OrderCommentsInfo()
                    {
                        BolComment = "PICK UP ORDER"
                    };
                }
            }

            //--ok now we start writing to the tables
            IDbConnection conn = Connection;
            conn.Open();
            bool isSuccess = false;
            int nextInvNo = 0;
            string storeProc = "";
            var sErrorList = new List<ErrorDetail>();

            //-- Get Inv_no for Product Invoice. No rollback.
            using (var tran = conn.BeginTransaction())
            {
                var param = new OracleDynamicParameters();
                param.Add("lv_nextInvNo", dbType: OracleMappingType.Int32, direction: ParameterDirection.Output);
                storeProc = "LCBO.GET_NEXT_INVOICE_NO";
                try
                {
                    conn.Execute(storeProc, param: param, commandType: CommandType.StoredProcedure);
                    nextInvNo = Decimal.ToInt32(param.Get<Decimal>("lv_nextInvNo"));
                    //--No rolling back as this can impact other users getting an inv_no from ROCON

                    tran.Commit();
                    isSuccess = true;
                }
                catch (Exception ex)
                {
                    isSuccess = false;
                    sErrorList.Add(new ErrorDetail()
                    {
                        errorType = "INVALID_ORDER",
                        errorMessage = "ERROR Generating Product Invoice Number",
                        errorData = new ErrorDetail.DataValue()
                        {
                            key = ErrorType.OTHER.ToString(),
                            value = ex.Message.ToString()
                        }
                    });
                }
                finally
                {
                    order.errorInfo = sErrorList.ToArray();
                }
            }

            //-- V3.0, Create E Prod inv. 
            //-- V3.1, add payment to COMMIT_order SP, due to ON_INV_PAY table has parent key to link to ON_INV.inv_no. Need to commit and rollback at the same SP.
            if (isSuccess)
            {
                using (var tran = conn.BeginTransaction())
                {
                    storeProc = "LCBO.COMMIT_NEW_ORDER_V3";

                    if (order.orderHeader.orderStatus == null)
                    {
                        order.orderHeader.orderStatus = new OrderStatusCodeDetail
                        {
                            orderStatusCode = "O",
                            orderStatusName = "O:OPEN"
                        };
                    }

                    var parmlist = new OracleDynamicParameters();
                    parmlist.Add("lv_invNumber", nextInvNo, dbType: OracleMappingType.Int32, direction: ParameterDirection.Input);
                    parmlist.Add("lv_orderNumber", order.orderHeader.sorOrderNumber, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                    parmlist.Add("lv_OrderType", order.orderHeader.orderType, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                    parmlist.Add("lv_startStage", 0, dbType: OracleMappingType.Int16, direction: ParameterDirection.Input);
                    parmlist.Add("lv_invoiceAmount", order.orderHeader.invoiceInfo.InvoiceTotalAmount, dbType: OracleMappingType.Decimal, direction: ParameterDirection.Input);
                    parmlist.Add("lv_deliveryCharge", 0, dbType: OracleMappingType.Decimal, direction: ParameterDirection.Input);
                    parmlist.Add("lv_deliveryTax1", 0m, dbType: OracleMappingType.Decimal, direction: ParameterDirection.Input);
                    parmlist.Add("lv_deliveryTax2", 0, dbType: OracleMappingType.Decimal, direction: ParameterDirection.Input);
                    parmlist.Add("lv_fullCases", 0, dbType: OracleMappingType.Int16, direction: ParameterDirection.Input);
                    parmlist.Add("lv_partCases", 0, dbType: OracleMappingType.Int16, direction: ParameterDirection.Input);

                    //--Silver V2.0 new fields
                    parmlist.Add("lv_orderSource", order.orderHeader.orderSourceInfo.systemIdentifier, dbType: OracleMappingType.Char, direction: ParameterDirection.Input);
                    parmlist.Add("lv_orderSourceName", order.orderHeader.orderSourceInfo.systemName, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                    parmlist.Add("lv_orderStatusCode", order.orderHeader.orderStatus.orderStatusCode, dbType: OracleMappingType.Char, direction: ParameterDirection.Input);
                    parmlist.Add("lv_orderStatusName", order.orderHeader.orderStatus.orderStatusName, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                    //--Will not use statusTime value from other system
                    //parmlist.Add("lv_status_DT_TM", FormatUtils.ConvertToYYYYMMDDHHMISS(order.orderHeader.orderStatus.statusCreateTime), dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);

                    //--lco and sor ref should not change during order's life cycle. Info were saved to LCO_ORDER_REF table during 'W' status.
                    //parmlist.Add("lv_lcoSysIdentifier", order.orderHeader.lcoOrderSystem.systemIdentifier, dbType: OracleMappingType.Char, direction: ParameterDirection.Input);
                    //parmlist.Add("lv_lcoSysName", order.orderHeader.lcoOrderSystem.systemName, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                    //parmlist.Add("lv_sorSysIdentifier", order.orderHeader.sorSystem.systemIdentifier, dbType: OracleMappingType.Char, direction: ParameterDirection.Input);
                    //parmlist.Add("lv_sorSysName", order.orderHeader.sorSystem.systemName, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);


                    //--V3.1, add delivery invoice. 
                    //--OMS will not pass delivery order number (deliveryReferenceNumber). Rocon SP should find it internally.
                    var delivOrderNum = "0";
                    decimal delivInvTotal = 0;
                    if (order.orderHeader.deliveryCharges.Count() > 0)
                    {
                        var drItem = order.orderHeader.deliveryCharges.Where(p => p.DeliveryChargeType == "E").First();
                        delivInvTotal = drItem.DeliveryBaseAmount + drItem.DeliveryHstAmount;
                    }
                    parmlist.Add("lv_deliverychargeTotalAmount", delivInvTotal, dbType: OracleMappingType.Decimal, direction: ParameterDirection.Input);

                    //--V3.1, add Payment. Might have only payment or only tender. Need to check both.
                    //--Using order create date as payment date 
                    //--if payment.amt+tender.amt not match prod.inv+deliv.inv, don't stop order flow. raise alert.

                    string pDT = null;
                    decimal pAmnt = 0;
                    decimal invAmnt = 0;
                    bool hasPayment = false;
                    bool hasDebitTender = false;
                    if (order.orderHeader.paymentEventInfo != null)
                    {
                        if (order.orderHeader.paymentEventInfo.paymentTransactions.Count() > 0)
                        {
                            hasPayment = true;
                            pAmnt = Convert.ToDecimal(order.orderHeader.paymentEventInfo.paymentTransactions[0].paymentAmount);
                            //pDT = FormatUtils.ConvertToYYYYMMDDHHMISS(order.orderHeader.paymentEventInfo.paymentTransactions[0].paymentDate);
                        }
                    }
                    if (order.orderHeader.tenderInfo != null)
                    {
                        if (order.orderHeader.tenderInfo.Count() > 0)
                        {
                            var tdItem = order.orderHeader.tenderInfo.Where(p => p.tenderType == "DEBIT").First();
                            if (tdItem != null)
                            {
                                hasDebitTender = true;
                                pAmnt = pAmnt + Convert.ToDecimal(tdItem.tenderAmount);
                            }
                        }
                    }
                    if (order.orderHeader.invoiceInfo != null)
                    {
                        invAmnt = order.orderHeader.invoiceInfo.InvoiceTotalAmount;
                    }
                    invAmnt = invAmnt + delivInvTotal;

                    if ((hasPayment || hasDebitTender) && (pAmnt == invAmnt))
                    { pDT = FormatUtils.ConvertToYYYYMMDDHHMISS(order.orderHeader.orderCreationDate); } 
                    else
                    {
                        //-- Don't stop order flow, raise alert
                        var conn1 = new OracleConnection(_configuration.GetConnectionString("roconconnection"));
                        ConnectionUtil.openConnection(conn1);
                        var lcoErrorLog = new LcoErrorLog();
                        lcoErrorLog.lcoService = "RoconAPI";
                        lcoErrorLog.lcoModule = "New Event";
                        lcoErrorLog.errorInfo = storeProc + ": order" + order.orderHeader.sorOrderNumber + " doesn't have payment and tender info. ";
                        lcoErrorLog.createdBy = "System";
                        lcoErrorLog.errorCode = "101";
                        SqlHelper.logToErrorService(lcoErrorLog, conn1);
                    }

                    parmlist.Add("lv_payDate", pDT, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                    parmlist.Add("lv_customerNumber", order.orderHeader.customerNumber, dbType: OracleMappingType.Int32, direction: ParameterDirection.Input);

                    parmlist.Add("lv_ReturnCode", dbType: OracleMappingType.Int32, direction: ParameterDirection.Output);
                    parmlist.Add("lv_returnCreation_DT_TM", dbType: OracleMappingType.Date, direction: ParameterDirection.Output);

                    if (order.orderHeader.referenceInfo != null)
                    {
                        var refInfos = order.orderHeader.referenceInfo.ToArray();
                        int refNum = 1;
                        string refParm = "lv_ref";
                        foreach (var refInf in refInfos)
                        {
                            parmlist.Add(refParm + refNum.ToString() + "_SysID", refInf.referenceSystem.systemIdentifier, dbType: OracleMappingType.Char, direction: ParameterDirection.Input);
                            parmlist.Add(refParm + refNum.ToString() + "_SysName", refInf.referenceSystem.systemName, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);

                            //--order# in diff system
                            parmlist.Add(refParm + refNum.ToString() + "_Identifier", refInf.referenceIdentifier, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                            parmlist.Add(refParm + refNum.ToString() + "_IdentifierType", refInf.referenceIdentifierType, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                            parmlist.Add(refParm + refNum.ToString() + "_IdentifierSubtype", refInf.referenceIdentifierSubtype, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                            parmlist.Add(refParm + refNum.ToString() + "_Comment", refInf.referenceComment, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                            refNum = refNum + 1;
                        }
                    }

                    try
                    {
                        conn.Execute(storeProc, param: parmlist, commandType: CommandType.StoredProcedure);

                        var returnCode = Decimal.ToInt32(parmlist.Get<Decimal>("lv_ReturnCode"));
                        if (returnCode == 0)
                        {
                            isSuccess = true;
                            var returnCreationTime = FormatUtils.ConvertToYYYYMMDDHHMISS(parmlist.Get<DateTime>("lv_returnCreation_DT_TM").ToString());
                            order.orderHeader.orderCreationDate = returnCreationTime;
                            //--Change orderstatus.creationtime to be ROCON O time.
                            order.orderHeader.orderStatus.statusCreateTime = returnCreationTime;
                            order.orderHeader.invoiceInfo.createdDate = returnCreationTime;
                        }
                        else
                        {
                            var sb = new System.Text.StringBuilder();
                            string errTable = "";
                            switch (returnCode)
                            {
                                case 1:
                                    errTable = "DBO.ON_INVOICE";
                                    break;
                                case 2:
                                    errTable = "DBO.ON_INV_CO";
                                    break;
                                case 3:
                                    errTable = "DBO.ON_INV_CODTL";
                                    break;
                                case 4:
                                    errTable = "DBO.ON_INV_CODTL_RULE";
                                    break;
                                case 5:
                                    errTable = "DBO.ON_COHDR (ORDER NOT ABLE TO BE O)";
                                    break;
                                case 6:
                                    errTable = "DBO.ON_COHDR (ORDER ALREADY IN CANCEL STATUS)";
                                    break;
                                case 7:
                                    errTable = "DBO.ON_COHDR (ORDER NOT IN W STATUS)";
                                    break;
                                case 8:
                                    errTable = "DBO.ON_INV_PAY (PRODUCT PAYMENT)";
                                    break;
                                case 9:
                                    errTable = "DBO.ON_INV_PAY (DELIVERY PAYMENT)";
                                    break;
                                default:
                                    errTable = "Unknown";
                                    break;
                            }
                            sb.AppendLine("Store Proc error: " + storeProc + " | Return Code: " + returnCode + " | table: " + errTable);
                            throw new Exception(sb.ToString());
                        }

                        // -- V3.1, post Prod and Deliv Payment
                        //if (order.orderHeader.paymentEventInfo != null)
                        if (1 == 2)
                        {
                            if (order.orderHeader.paymentEventInfo.paymentTransactions.Count() > 0)
                            {
                                if (isSuccess)
                                {
                                    storeProc = "LCBO.UPDATE_ORDER_PAYMENT";
                                    var parm1 = new OracleDynamicParameters();

                                    parm1.Add("lv_sorOrderNumber", order.orderHeader.sorOrderNumber, dbType: OracleMappingType.Int32, direction: ParameterDirection.Input);
                                    parm1.Add("lv_invNumber", nextInvNo, dbType: OracleMappingType.Int32, direction: ParameterDirection.Input);
                                    parm1.Add("lv_customerNumber", order.orderHeader.customerNumber, dbType: OracleMappingType.Int32, direction: ParameterDirection.Input);
                                    parm1.Add("lv_invoiceTotalAmount", order.orderHeader.invoiceInfo.InvoiceTotalAmount, dbType: OracleMappingType.Decimal, direction: ParameterDirection.Input);
                                    parm1.Add("lv_payDate", FormatUtils.ConvertToYYYYMMDDHHMISS(order.orderHeader.paymentEventInfo.paymentTransactions[0].paymentDate), dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                                    parm1.Add("lv_returnCode", dbType: OracleMappingType.Int32, direction: ParameterDirection.Output);

                                    conn.Execute(storeProc, param: parm1, commandType: CommandType.StoredProcedure);
                                    returnCode = Decimal.ToInt32(parm1.Get<Decimal>("lv_ReturnCode"));
                                    if (returnCode == 0)
                                    { isSuccess = true; }
                                    else
                                    {
                                        isSuccess = false;
                                        throw new Exception("Error creating product payment");
                                    }
                                }

                                if (isSuccess)
                                {
                                    storeProc = "LCBO.UPDATE_DELIVERY_PAYMENT";
                                    var parm1 = new OracleDynamicParameters();
                                    delivOrderNum = "0";
                                    delivInvTotal = 0;
                                    if (order.orderHeader.deliveryCharges.Count() > 0)
                                    {
                                        var drItem = order.orderHeader.deliveryCharges.Where(p => p.DeliveryChargeType == "E").First();
                                        delivOrderNum = drItem.deliveryChargeReferenceInfo.referenceIdentifier.ToString();
                                        delivInvTotal = drItem.DeliveryBaseAmount + drItem.DeliveryHstAmount;
                                    }

                                    parm1.Add("lv_sorOrderNumber", order.orderHeader.sorOrderNumber, dbType: OracleMappingType.Int32, direction: ParameterDirection.Input);
                                    parm1.Add("lv_customerNumber", order.orderHeader.customerNumber, dbType: OracleMappingType.Int32, direction: ParameterDirection.Input);
                                    parm1.Add("lv_deliveryReferenceNumber", delivOrderNum, dbType: OracleMappingType.Int32, direction: ParameterDirection.Input);
                                    parm1.Add("lv_deliverychargeTotalAmount", delivInvTotal, dbType: OracleMappingType.Decimal, direction: ParameterDirection.Input);
                                    parm1.Add("lv_payDate", FormatUtils.ConvertToYYYYMMDDHHMISS(order.orderHeader.paymentEventInfo.paymentTransactions[0].paymentDate), dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                                    parm1.Add("lv_returnCode", dbType: OracleMappingType.Int32, direction: ParameterDirection.Output);

                                    conn.Execute(storeProc, param: parm1, commandType: CommandType.StoredProcedure);
                                    returnCode = Decimal.ToInt32(parm1.Get<Decimal>("lv_ReturnCode"));
                                    if (returnCode == 0)
                                    { isSuccess = true; }
                                    else
                                    {
                                        isSuccess = false;
                                        throw new Exception("Error creating delivery payment");
                                    }
                                }
                            }
                        }
                        tran.Commit();
                    }
                    catch (OracleException ex)
                    {
                        isSuccess = false;
                        sErrorList.Add(new ErrorDetail()
                            {
                                errorType = "OTHERS",
                                errorMessage = "Unhandling exception occurred.",
                                errorData = new ErrorDetail.DataValue()
                                {
                                    key = ErrorType.OTHER.ToString(),
                                    value = ex.Message.ToString()
                                }
                            });
                        order.errorInfo = sErrorList.ToArray();
                    }
                    catch (Exception ex)
                    {
                        isSuccess = false;
                        sErrorList.Add(new ErrorDetail()
                        {
                            errorType = "INVALID_ORDER",
                            errorMessage = "Order " + order.orderHeader.lcoOrderNumber + " status cannot be changed to OPEN!",
                            errorData = new ErrorDetail.DataValue()
                            {
                                key = ErrorType.OTHER.ToString(),
                                value = ex.Message.ToString()
                            }
                        });
                        order.errorInfo = sErrorList.ToArray();
                    }
                    finally
                    {
                        if (!isSuccess) tran.Rollback();
                    }
                }
            }

            return Task.FromResult(order);
        }

        // Gold 3.0. WCS can call RoconAPI to Cancel order in 'W' status
        public Task<Order> CancelOrder(Order order)
        {
            IDbConnection conn = Connection;
            conn.Open();
            bool isSuccess = false;
            isSuccess = true;
            using (var tran = conn.BeginTransaction())
            {
                string storeProc = "LCBO.CANCEL_LCO_ORDER_V3";
                // --V3.1, need to consider if payment is made by credit note on 'W' status, should offset the payment.
                
                var param = new OracleDynamicParameters();
                param.Add("lv_orderNumber", order.orderHeader.sorOrderNumber, dbType: OracleMappingType.Int32, direction: ParameterDirection.Input);
                param.Add("lv_ReturnCode", dbType: OracleMappingType.Int32, direction: ParameterDirection.Output);
                param.Add("lv_returnCancel_DT_TM", dbType: OracleMappingType.Date, direction: ParameterDirection.Output);

                try
                {
                    conn.Execute(storeProc, param: param, commandType: CommandType.StoredProcedure);
                    var returnCode = Decimal.ToInt32(param.Get<Decimal>("lv_ReturnCode"));
                    
                    if (returnCode == 0)
                    {
                        tran.Commit();
                        var returnCancelTime = FormatUtils.ConvertToYYYYMMDDHHMISS(param.Get<DateTime>("lv_returnCancel_DT_TM").ToString());
                        if (order.orderHeader.orderStatus == null)
                        {
                            order.orderHeader.orderStatus = new OrderStatusCodeDetail
                            {
                                orderStatusCode = "D",
                                orderStatusName = "D:DELETED_CANCELLED",
                                statusCreateTime = returnCancelTime
                        };
                        }
                    }
                    else
                    {
                        isSuccess = false;
                        var sb = new System.Text.StringBuilder();
                        string errMsg = "";
                        switch (returnCode)
                        {
                            case 1:
                                errMsg = "Order is already cancelled.";
                                break;
                            case 2:
                                errMsg = "Order not in W status to be cancelled";
                                break;
                            case 3:
                                errMsg = "Cannot update ON_COHDR table";
                                break;
                            default:
                                errMsg = "Unknown";
                                break;
                        }
                        sb.AppendLine("Store Proc error: LCBO.CANCEL_LCO_ORDER" + " | " + "Return Code:" + returnCode + " | " + errMsg );
                        throw new Exception(sb.ToString());
                    }
                }
                catch (Exception ex)
                {
                    isSuccess = false;
                    var sErrorList = new List<ErrorDetail>();
                    sErrorList.Add(new ErrorDetail()
                    {
                        errorType = "ACTION_FAILED",
                        errorMessage = "ERROR on Cancelling order " + order.orderHeader.sorOrderNumber,
                        errorData = new ErrorDetail.DataValue()
                        {
                            key = ErrorType.ACTION_FAILED.ToString(),
                            value = ex.Message.ToString()
                        }
                    });
                    order.errorInfo = sErrorList.ToArray();
                }
                finally
                {
                    if (!isSuccess) tran.Rollback();
                }
            }
            return Task.FromResult(order);
        }
    
    // Golde 3.1. OMS will call RoconAPI to Invoice order in 'I' status
    public Task<Order> InvoicedOrder(Order order)
        {
            bool isSuccess = false;
            string storeProc = "";
            var sErrorList = new List<ErrorDetail>();
            var param = new OracleDynamicParameters();
            var paramDR = new OracleDynamicParameters();
            var paramDRP = new OracleDynamicParameters();
            var paramPI = new OracleDynamicParameters();
            var paramPD = new OracleDynamicParameters();
            var paramUPOS = new OracleDynamicParameters();
            int nextSEQ = 0;
            var bInsertInvHeader = false;
            var bUpdateInvDetail = true;

            IDbConnection conn = Connection;
            conn.Open();

            //-- Get 'A' Product invoice from payload
            if (order.orderHeader.invoiceInfoFinal == null)
            {
                order.orderHeader.invoiceInfoFinal = new InvoiceInfo();
            }
        
            if (order.orderHeader.invoiceInfoFinal.InvoiceSystemInfo == null)
            {
                order.orderHeader.invoiceInfoFinal.InvoiceSystemInfo = new SystemIdentify();
            }
            //-- Get 'A' Delivery invoice amount from payload. 
            var dcs = new List<DeliveryCharge>();
            if (order.orderHeader.deliveryCharges == null)
            {
                order.orderHeader.deliveryCharges = dcs.ToArray();
            }

            //-- ON_INV_CODTL table has parent key to link to ON_INV_CO.inv_no,co_odno, co_inv_type. Need to commit ON_INV_CO first.
            //-- If failed, need to delete ON_INV_CO A record.
            //-- Commit AR SEQ first. Then commit Invoice header. Then Inv detail, then AR SEQ, Post_delivery_inv, commit again. Then update order status.

            // Get next AR SEQ number. Cannot rollback.
            using (var tran = conn.BeginTransaction())
            {
                param.Add("lv_ARSEQ", dbType: OracleMappingType.Int32, direction: ParameterDirection.Output);
                storeProc = "LCBO.GET_NEXT_ARSEQ";
                try
                {
                    conn.Execute(storeProc, param: param, commandType: CommandType.StoredProcedure);
                    nextSEQ = Decimal.ToInt32(param.Get<Decimal>("lv_ARSEQ"));
                    tran.Commit();
                    isSuccess = true;
                }
                catch (Exception ex)
                {
                    isSuccess = false;
                    sErrorList.Add(new ErrorDetail()
                    {
                        errorType = "OTHER",
                        errorMessage = "ERROR Generating AR SEQ number for Actual Delivery Invoice.",
                        errorData = new ErrorDetail.DataValue()
                        {
                            key = ErrorType.OTHER.ToString(),
                            value = ex.Message.ToString()
                        }
                    });
                    order.errorInfo = sErrorList.ToArray();
                }
            }

            //-- create and commit 'A' product inv header 
            //-- if return =1, already have 'A' inv. need to raise error to err_log table. return=2, fail to insert on_inv_co.
            if (isSuccess)
            {
                using (var tran = conn.BeginTransaction())
                {   storeProc = "LCBO.UPDATE_ORDER_INVOICE_HEADER";

                    paramPI.Add("lv_sorOrderNumber", order.orderHeader.sorOrderNumber, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                    paramPI.Add("lv_createDate", FormatUtils.ConvertToYYYYMMDDHHMISS(order.orderHeader.invoiceInfoFinal.createdDate), dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                    paramPI.Add("lv_invoiceAmount", order.orderHeader.invoiceInfoFinal.InvoiceTotalAmount, dbType: OracleMappingType.Decimal, direction: ParameterDirection.Input);
                    paramPI.Add("lv_fullCases", order.orderHeader.numberOfFullCases, dbType: OracleMappingType.Int16, direction: ParameterDirection.Input);
                    paramPI.Add("lv_partCases", order.orderHeader.numberOfPartialCases, dbType: OracleMappingType.Int16, direction: ParameterDirection.Input);
                    paramPI.Add("lv_returnCode", dbType: OracleMappingType.Int32, direction: ParameterDirection.Output);
                    try
                    {
                        conn.Execute(storeProc, param: paramPI, commandType: CommandType.StoredProcedure);
                        var returnCodePI = Decimal.ToInt32(paramPI.Get<Decimal>("lv_returnCode"));

                        if (returnCodePI == 0)
                        {
                            isSuccess = true;
                            bInsertInvHeader = true;
                        }
                        else if (returnCodePI == 1)
                        {  //-- already has 'A' prod invoice in ON_INV_CO. order flow can continue but need to add errlog and raise email alert to Admin.
                            isSuccess = true;
                            bInsertInvHeader = false;

                            var conn1 = new OracleConnection(_configuration.GetConnectionString("roconconnection"));
                            ConnectionUtil.openConnection(conn1);
                            var lcoErrorLog = new LcoErrorLog();
                            lcoErrorLog.lcoService = "RoconAPI";
                            lcoErrorLog.lcoModule = "INVOICED Event";
                            lcoErrorLog.errorInfo = storeProc + ": Order " + order.orderHeader.sorOrderNumber + " already has 'A' product invoice. No update in table ON_INV_CO.  ";
                            lcoErrorLog.createdBy = "System";
                            lcoErrorLog.errorCode = "102";
                            SqlHelper.logToErrorService(lcoErrorLog, conn1);
                        }
                        else
                        {
                            isSuccess = false;
                            var sb = new System.Text.StringBuilder();
                            string errTable = "";
                            bInsertInvHeader = false;
                            switch (returnCodePI)
                            {
                                case 2:
                                    errTable = "DBO.ON_INV_CO";
                                    break;
                                default:
                                    errTable = "Unknown";
                                    break;
                            }
                            sb.AppendLine("Store Proc error: UPDATE_ORDER_INVOICE_HEADER" + " | Return Code: " + returnCodePI + " | Table: " + errTable);
                            throw new Exception(sb.ToString());
                        }
                        tran.Commit();
                    }
                    catch (OracleException ex)
                    {
                        isSuccess = false;
                        bInsertInvHeader = false;
                        sErrorList.Add(new ErrorDetail()
                        {
                            errorType = "OTHERS",
                            errorMessage = "Unhandling exception occurred on SP: " + storeProc + ".",
                            errorData = new ErrorDetail.DataValue()
                            {
                                key = ErrorType.OTHER.ToString(),
                                value = ex.Message.ToString()
                            }
                        });
                        order.errorInfo = sErrorList.ToArray();
                    }
                    catch (Exception ex)
                    {
                        isSuccess = false;
                        bInsertInvHeader = false;
                        bUpdateInvDetail = false;
                        sErrorList.Add(new ErrorDetail()
                        {
                            errorType = "ACTION_FAILED",
                            errorMessage = "ERROR on invoicing order " + order.orderHeader.sorOrderNumber + " | SP: " + storeProc + ".",
                            errorData = new ErrorDetail.DataValue()
                            {
                                key = ErrorType.ACTION_FAILED.ToString(),
                                value = ex.Message.ToString()
                            }
                        });
                        order.errorInfo = sErrorList.ToArray();
                    }
                    finally
                    {
                        if (!isSuccess) tran.Rollback();
                    }
                }
            }

            if (isSuccess)
            {
                using (var tran = conn.BeginTransaction())
                {  
                    try
                    {   
                        //--create 'A' product inv detail
                        if (isSuccess)
                        {
                            //-- create detail. if invoice header created by Rocon GUI, don't update detail.
                            if (isSuccess && bInsertInvHeader)
                            {
                                var OrderItems = order.orderItems.OrderBy(p => p.lineNumber);
                                int orderedLineNumbers = 0;
                                foreach (var itm in OrderItems.ToArray())
                                {
                                    storeProc = "LCBO.UPDATE_ORDER_INVOICE_DETAILS";
                                    var paramList = new OracleDynamicParameters();

                                    paramPD.Add("lv_sorOrderNumber", order.orderHeader.sorOrderNumber, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                                    paramPD.Add("lv_orderType", order.orderHeader.orderType, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                                    paramPD.Add("lv_customerType", order.orderHeader.customerType, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                                    paramPD.Add("lv_itemLineNumber", itm.lineNumber, dbType: OracleMappingType.Int16, direction: ParameterDirection.Input);
                                    paramPD.Add("lv_itemSku", itm.sku.ToString(), dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                                    paramPD.Add("lv_itemQuantity", itm.quantity, dbType: OracleMappingType.Int16, direction: ParameterDirection.Input);
                                    paramPD.Add("lv_shippedQuantity", itm.shippedQuantity, dbType: OracleMappingType.Int16, direction: ParameterDirection.Input);
                                    paramPD.Add("lv_itemLineTotal", itm.itemLineTotal, dbType: OracleMappingType.Decimal, direction: ParameterDirection.Input);
                                    paramPD.Add("lv_itemSellPrice", itm.priceInfo.selling_price, dbType: OracleMappingType.Decimal, direction: ParameterDirection.Input);
                                    paramPD.Add("lv_itemBasicPrice", itm.priceInfo.unit_price, dbType: OracleMappingType.Decimal, direction: ParameterDirection.Input);
                                    paramPD.Add("lv_itemBottleDeposit", itm.priceInfo.bottle_deposit, dbType: OracleMappingType.Decimal, direction: ParameterDirection.Input);
                                    paramPD.Add("lv_itemDiscountAmount", itm.priceInfo.discount, dbType: OracleMappingType.Decimal, direction: ParameterDirection.Input);
                                    paramPD.Add("lv_itemLicMarkAmount", itm.priceInfo.liquor_markup, dbType: OracleMappingType.Decimal, direction: ParameterDirection.Input);
                                    paramPD.Add("lv_itemHSTAmount", itm.priceInfo.hst_tax, dbType: OracleMappingType.Decimal, direction: ParameterDirection.Input);
                                    paramPD.Add("lv_returnCode", dbType: OracleMappingType.Int32, direction: ParameterDirection.Output);

                                    conn.Execute(storeProc, param: paramPD, commandType: CommandType.StoredProcedure);
                                    orderedLineNumbers = orderedLineNumbers + 1;

                                    var rtn = Decimal.ToInt32(paramPD.Get<Decimal>("lv_returnCode"));
                                    if (rtn != 0)
                                    {
                                        isSuccess = false;
                                        bUpdateInvDetail = false;
                                        string errTable = "";
                                        var sb = new System.Text.StringBuilder();
                                        switch (rtn)
                                        {
                                            case 1:
                                                errTable = "DBO.ON_INV_CODTL";
                                                break;
                                            case 2:
                                                errTable = "DBO.ON_INV_CODTL_RULE";
                                                break;
                                            default:
                                                errTable = "Unknown";
                                                break;
                                        }
                                        sb.AppendLine("Store Proc error: " + storeProc + " | Return Code: " + rtn + " | Table: " + errTable);
                                        throw new Exception(sb.ToString());
                                    }
                                }
                                //--V3.1, somehow it will not hit Rollback action. Need to enhance.
                                if (orderedLineNumbers != order.orderItems.Count())
                                {
                                    isSuccess = false;
                                    bUpdateInvDetail = false;
                                    sErrorList.Add(new ErrorDetail()
                                    {
                                        errorType = "INVALID_ORDER",
                                        errorMessage = "Count of order items is incorrect.",
                                        errorData = new ErrorDetail.DataValue()
                                        {
                                            key = ErrorType.OTHER.ToString(),
                                            value = orderedLineNumbers.ToString()
                                        }
                                    });
                                    throw new Exception();
                                }
                            }
                        }

                        //var drItemFound = order.orderHeader.deliveryCharges.Where(p => p.DeliveryChargeType == "A");
                        //if (drItemFound?.Any() != null)
                        //{
                        var drItem = (order.orderHeader.deliveryCharges.Where(p => p.DeliveryChargeType == "A").First());
                        //}

                        if (drItem != null)
                        {
                            //-- Create 'A' delivery inv
                            if (isSuccess)
                            {
                                storeProc = "LCBO.CREATE_DELIVERY_INVOICE";

                                paramDR.Add("lv_sorOrderNumber", order.orderHeader.sorOrderNumber.ToString(), dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                                paramDR.Add("lv_createDate", FormatUtils.ConvertToYYYYMMDDHHMISS(drItem.CreatedDate), dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                                //paramDR.Add("lv_deliveryReferenceNumber", drItem.deliveryChargeReferenceInfo.referenceIdentifier, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                                paramDR.Add("lv_deliveryBaseAmount", drItem.DeliveryBaseAmount, dbType: OracleMappingType.Decimal, direction: ParameterDirection.Input);
                                paramDR.Add("lv_deliveryHSTAmount", drItem.DeliveryHstAmount, dbType: OracleMappingType.Decimal, direction: ParameterDirection.Input);
                                paramDR.Add("lv_fullCases", order.orderHeader.numberOfFullCases, dbType: OracleMappingType.Int16, direction: ParameterDirection.Input);
                                paramDR.Add("lv_partCases", order.orderHeader.numberOfPartialCases, dbType: OracleMappingType.Int16, direction: ParameterDirection.Input);
                                paramDR.Add("lv_sourceSystemId", drItem.deliveryChargeSourceInfo.systemIdentifier, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                                paramDR.Add("lv_deliveryRateId", Convert.ToInt32(drItem.deliveryChargeSourceId), dbType: OracleMappingType.Int32, direction: ParameterDirection.Input);
                                paramDR.Add("lv_returnCode", dbType: OracleMappingType.Int32, direction: ParameterDirection.Output);

                                conn.Execute(storeProc, param: paramDR, commandType: CommandType.StoredProcedure);
                                var rtnCD = Decimal.ToInt32(paramDR.Get<Decimal>("lv_returnCode"));

                                //-- return code = 1, no Estimate delivery invoice. Order flow can continue.
                                //-- but might need alert to Admin.
                                if (rtnCD == 0)
                                {
                                    isSuccess = true;
                                }
                                else if (rtnCD == 1)
                                {
                                    isSuccess = true;

                                    var conn1 = new OracleConnection(_configuration.GetConnectionString("roconconnection"));
                                    ConnectionUtil.openConnection(conn1);
                                    var lcoErrorLog = new LcoErrorLog();
                                    lcoErrorLog.lcoService = "RoconAPI";
                                    lcoErrorLog.lcoModule = "INVOICED Event";
                                    lcoErrorLog.errorInfo = storeProc+ ": Order " + order.orderHeader.sorOrderNumber + " doesn't have estimate Delivery Invoice. No update on ON_INV_CO.  ";
                                    lcoErrorLog.createdBy = "System";
                                    lcoErrorLog.errorCode = "103";
                                    SqlHelper.logToErrorService(lcoErrorLog, conn1);
                                }
                                else
                                {
                                    isSuccess = false;
                                    var sb = new System.Text.StringBuilder();
                                    string errTable = "";
                                    switch (rtnCD)
                                    {
                                        case 2:
                                            errTable = "LCBO.LCO_DELIVERY_CHARGE";
                                            break;
                                        case 3:
                                            errTable = "DBO.ON_INV_CO";
                                            break;
                                        default:
                                            errTable = "Unknown";
                                            break;
                                    }
                                    sb.AppendLine("Store Proc error: LCBO.CREATE_DELIVERY_INVOICE" + " | Return Code: " + rtnCD + " | Table: " + errTable);
                                    throw new Exception(sb.ToString());
                                }
                            }

                            //-- Post delivery inv
                            if (isSuccess)
                            {
                                storeProc = "LCBO.POST_DELIVERY_INVOICE";
                                paramDRP.Add("lv_ARSEQ", nextSEQ, dbType: OracleMappingType.Int32, direction: ParameterDirection.Input);
                                paramDRP.Add("lv_customerNumber", order.orderHeader.customerNumber, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                                paramDRP.Add("lv_sorOrderNumber", order.orderHeader.sorOrderNumber, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                                //paramDRP.Add("lv_sorDeliveryNumber", drItem.deliveryChargeReferenceInfo.referenceIdentifier.ToString(), dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                                paramDRP.Add("lv_deliveryBaseAmount", drItem.DeliveryBaseAmount, dbType: OracleMappingType.Decimal, direction: ParameterDirection.Input);
                                paramDRP.Add("lv_deliveryHSTAmount", drItem.DeliveryHstAmount, dbType: OracleMappingType.Decimal, direction: ParameterDirection.Input);
                                paramDRP.Add("lv_user", "oms", dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                                paramDRP.Add("lv_returnCode", dbType: OracleMappingType.Int32, direction: ParameterDirection.Output);

                                conn.Execute(storeProc, param: paramDRP, commandType: CommandType.StoredProcedure);
                                var returnCodeDRP = Decimal.ToInt32(paramDRP.Get<Decimal>("lv_returnCode"));

                                if (returnCodeDRP == 0)
                                {
                                    isSuccess = true;
                                }
                                else if (returnCodeDRP == 6)
                                {
                                    isSuccess = true;
                                    var conn1 = new OracleConnection(_configuration.GetConnectionString("roconconnection"));
                                    ConnectionUtil.openConnection(conn1);
                                    var lcoErrorLog = new LcoErrorLog();
                                    lcoErrorLog.lcoService = "RoconAPI";
                                    lcoErrorLog.lcoModule = "INVOICED Event";
                                    lcoErrorLog.errorInfo = storeProc + ": Order " + order.orderHeader.sorOrderNumber + " doesn't have estimate Delivery Invoice. Not able to post delivery invoice. ";
                                    lcoErrorLog.createdBy = "System";
                                    lcoErrorLog.errorCode = "104";
                                    SqlHelper.logToErrorService(lcoErrorLog, conn1);
                                }
                                else
                                {
                                    isSuccess = false;
                                    var sb = new System.Text.StringBuilder();
                                    string errTable = "";
                                    switch (returnCodeDRP)
                                    {
                                        case 1:
                                            errTable = "DBO.ON_ARTRAN";
                                            break;
                                        case 2:
                                            errTable = "DBO.ON_GLTRAN-DR";
                                            break;
                                        case 3:
                                            errTable = "DBO.ON_GLTRAN-CR";
                                            break;
                                        case 4:
                                            errTable = "DBO.ON_GLTRAN-CRHST";
                                            break;
                                        case 5:
                                            errTable = "DBO.ON_INV_CO";
                                            break;
                                        default:
                                            errTable = "Unknown";
                                            break;
                                    }
                                    sb.AppendLine("Store Proc error: LCBO.POST_DELIVERY_INVOICE" + " | Return Code: " + returnCodeDRP + " | Table: " + errTable);
                                    throw new Exception(sb.ToString());
                                }
                            }
                        }

                        //--Update order status to 'I'
                        if (isSuccess)
                        {
                            storeProc = "LCBO.UPDATE_ORDER_STATUS_INVOICED";
                            paramUPOS.Add("lv_sorOrderNumber", order.orderHeader.sorOrderNumber.ToString(), dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                            conn.Execute(storeProc, param: paramUPOS, commandType: CommandType.StoredProcedure);
                        }

                        if (isSuccess)
                        { tran.Commit(); }

                    }
                    catch (OracleException ex)
                    {
                        isSuccess = false;
                        sErrorList.Add(new ErrorDetail()
                        {
                            errorType = "OTHERS",
                            errorMessage = "Unhandling exception occurred on SP: " + storeProc + ".",
                            errorData = new ErrorDetail.DataValue()
                            {
                                key = ErrorType.OTHER.ToString(),
                                value = ex.Message.ToString()
                            }
                        });
                        order.errorInfo = sErrorList.ToArray();
                    }
                    catch (Exception ex)
                    {
                        isSuccess = false;
                        sErrorList.Add(new ErrorDetail()
                        {
                            errorType = "ACTION_FAILED",
                            errorMessage = "ERROR on invoicing order " + order.orderHeader.sorOrderNumber + " | SP: " + storeProc + ".",
                            errorData = new ErrorDetail.DataValue()
                            {
                                key = ErrorType.ACTION_FAILED.ToString(),
                                value = ex.Message.ToString()
                            }
                        });
                        order.errorInfo = sErrorList.ToArray();
                    }
                    finally
                    {
                        if (!isSuccess)
                        {
                            tran.Rollback();

                            //--Need to delete the 'A' invoice header on ON_INV_CO header only if insert detail failed.
                            if  (!bUpdateInvDetail)
                            { 
                            using (var tran1 = conn.BeginTransaction())
                            {
                                storeProc = "LCBO.DELETE_LCO_INV_HEADER";
                                var parm1 = new OracleDynamicParameters();
                                parm1.Add("lv_sorOrderNumber", order.orderHeader.sorOrderNumber, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                                conn.Execute(storeProc, param: parm1, commandType: CommandType.StoredProcedure);
                                tran1.Commit();
                            }
                            }

                        }
                    }
                }
            }
            return Task.FromResult(order);
        }

    public async Task<Order> GetOrderByIdAsync(string orderID, string orderInvType)
        {
            using (IDbConnection conn = Connection)
            {
                string RocOrderID = orderID;
                //string OrderInvType = "E"; //use 'A' if order is already invoiced

                //--if orderID is lcoOrderNumber use GET_ORDER_INFO_BY_REF instead
                string storeProc = "LCBO.GET_ORDER_INFO_V2";

                var param = new OracleDynamicParameters();
                param.Add("lv_sorOrderNumber", RocOrderID);
                param.Add("lv_co_inv_type", orderInvType);
                param.Add("lv_order", dbType: OracleMappingType.RefCursor, direction: ParameterDirection.Output);

                string sQuery = storeProc;

                if (conn.State != ConnectionState.Open)
                {
                    conn.Open();
                }

                if (conn.State == ConnectionState.Open)
                {
                    //  var results = await conn.QueryAsync<OrderHeaderEntity>(sQuery, param: param, commandType: CommandType.StoredProcedure);
                    //-- Not returning OrderSourceInfo, OrderEvent, OrderStatus, PublishedStatus, ReferenceInfo, lcoSystem, sorSystem,
                    var results = await conn.QueryAsync<OrderHeaderEntity, RouteInfoEntity, OrderShipToInfoEntity,
                        OrderCommentsInfoEntity, PackingInvoiceCommentEntity, InvoiceInfoEntity, DeliveryChargeEntity,
                        OrderHeaderEntity>(sQuery, map: (header, route, ship, comment, packing, inv, delivery) =>
                         {
                             header.routeInfo = route;
                             header.shipToInfo = ship;
                             if (comment != null)
                             {
                                 header.orderCommentsInfo = comment;
                             }

                             if (packing != null)
                             {
                                 header.orderCommentsInfo.PackingInvoiceComment = packing;
                             }

                             //header.invoiceInfo = inv;
                             //header.invoiceInfo.DeliveryCharge = delivery;
                             return header;
                         }
                            , splitOn: "routeCode,name,bolcomment,textLine1,invoiceNumber,deliveryChargeType"
                            , param: param, commandType: CommandType.StoredProcedure);

                    var order = new OrderEntity();

                    var orderHeaderInfo = results.SingleOrDefault();

                    order.orderHeader = orderHeaderInfo;

                    //-- Not returning shippedQTY
                    var orderItems = await GetOrderItemDetails(orderID);

                    order.orderItems = orderItems.ToArray();

                    return _mapper.Map<Order>(order);

                };

            }

            return null; //otherwise return null
        }

    public async Task<IEnumerable<OrderItemsEntity>> GetOrderItemDetails(string orderID)
        {
            using (IDbConnection conn = Connection)
            {
                string RocOrderID = orderID;
                string OrderInvType = "E"; //use 'A' if order is already invoiced

                //if orderID is lcoOrderNumber use GET_ORDER_INFO_BY_REF instead
                string storeProc = "LCBO.GET_ORDER_DETAIL_INFO_V2";

                var param = new OracleDynamicParameters();
                param.Add("lv_sorOrderNumber", RocOrderID);
                param.Add("lv_co_inv_type", OrderInvType);
                param.Add("lv_orderdetails", dbType: OracleMappingType.RefCursor, direction: ParameterDirection.Output);

                string sQuery = storeProc;

                if (conn.State != ConnectionState.Open)
                {
                    conn.Open();
                }

                if (conn.State == ConnectionState.Open)
                {
                    var results = await conn.QueryAsync<OrderItemsEntity, OrderItemPriceInfoEntity, OrderItemsEntity>(sQuery, map: (item, price) =>
                       {
                           item.priceinfo = price;
                           return item;
                       }, splitOn: "sellingPrice", param: param, commandType: CommandType.StoredProcedure);


                    return results.ToArray();
                }
            }
            return null;
        }

    //Get order history status in sequence
    public async Task<IEnumerable<OrderStatusDetailEntity>> GetOrderPublishedStatus(string orderID)
        {
            using (IDbConnection conn = Connection)
            {
                string RocOrderID = orderID;
                string storeProc = "LCBO.GET_ORDER_PUBLISHED_STATUS";

                var param = new OracleDynamicParameters();
                param.Add("lv_sorOrderNumber", RocOrderID);
                param.Add("ref_orderPublishedStatus", dbType: OracleMappingType.RefCursor, direction: ParameterDirection.Output);

                if (conn.State != ConnectionState.Open)
                {
                    conn.Open();
                }

                if (conn.State == ConnectionState.Open)
                {
                    var results = await conn.QueryAsync<OrderStatusDetailEntity>(storeProc, param: param, commandType: CommandType.StoredProcedure);

                    return results.ToArray();
                }
            }
            return null;

        }

    //Get all possible order status life cycle path
    public async Task<IEnumerable<OrderStatusDetailEntity>> GetSystemOrderStatusCycle()
        {
            using (IDbConnection conn = Connection)
            {
                string storeProc = "LCBO.GET_ORDER_STATUS_CYCLE";

                var param = new OracleDynamicParameters();
                param.Add("lv_orderStatusCycle", dbType: OracleMappingType.RefCursor, direction: ParameterDirection.Output);

                if (conn.State != ConnectionState.Open)
                {
                    conn.Open();
                }

                if (conn.State == ConnectionState.Open)
                {
                    var results = await conn.QueryAsync<OrderStatusDetailEntity>(storeProc, param: param, commandType: CommandType.StoredProcedure);
                    return results.ToArray();
                }
            }
            return null;

        }


    }
}
 