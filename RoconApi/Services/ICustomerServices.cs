﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using RoconLibrary.Domains;
using RoconLibrary.Models;

namespace RoconApi.Services
{
    public interface ICustomerServices
    {
       // Task<IEnumerable<Customer>> GetAllCustomersAsync(string custType);
        Task<Customer> GetCustomerByNumberAsync(string custNum);
        Task<IEnumerable<AvailableShippingDateEntity>> GetAvailableShippingDatesAsync(string routeCode);
        
    }
}
