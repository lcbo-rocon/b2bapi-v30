﻿using AutoMapper;
using RoconLibrary.Models;
using RoconLibrary.Domains;
using RoconLibrary.Utility;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RoconApi.Utility
{
    public class MapperProfile : Profile
    {
        public MapperProfile()
        {
            ///--Order Entity
            CreateMap<OrderEntity, Order>()
               //.ForMember(dest => dest.Self, opt => opt.MapFrom(src =>
               //      Link.To(nameof(Controllers.OrdersController.GetOrderByID),null)))
               ;
            CreateMap<OrderHeaderEntity, OrderHeader>()
                .ForMember(dest => dest.orderRequestDate, opt => opt.MapFrom(src =>
                        FormatUtils.AddTimezone(src.orderRequestDate)))
                .ForMember(dest => dest.orderCreationDate, opt => opt.MapFrom(src =>
                         FormatUtils.AddTimezone(src.orderCreationDate)))
                .ForMember(dest => dest.orderRequiredDate, opt => opt.MapFrom(src =>
                         FormatUtils.ConvertToYYYYMMDD(src.orderRequiredDate)))
                .ForMember(dest => dest.orderShipmentDate, opt => opt.MapFrom(src =>
                       FormatUtils.ConvertToYYYYMMDD(src.orderShipmentDate)))
                //.ForMember(dest => dest.orderEvent, opt => opt.MapFrom(src =>
                //       src.orderEvent.ToString()))
            ;

            CreateMap<OrderItemsEntity, OrderItemDetail>();
            CreateMap<OrderItemPriceInfoEntity, OrderItemPriceInfo>()
               .ForMember(dest => dest.selling_price, opt => opt.MapFrom(src => src.sellingPrice))
               .ForMember(dest => dest.unit_price, opt => opt.MapFrom(src => src.unitPrice))
               .ForMember(dest => dest.bottle_deposit, opt => opt.MapFrom(src => src.itemBottleDeposit))
               .ForMember(dest => dest.discount, opt => opt.MapFrom(src => src.itemDiscountAmount))
               .ForMember(dest => dest.liquor_markup, opt => opt.MapFrom(src => src.licMarkUp))
               .ForMember(dest => dest.hst_tax, opt => opt.MapFrom(src => src.hstTax))
               .ForMember(dest => dest.retail_price, opt => opt.MapFrom(src => src.retailPrice))
               ;
            CreateMap<PriceInfo, OrderItemPriceInfo>();

            CreateMap<OrderShipToInfoEntity, OrderShipToInfo>();
            CreateMap<OrderCommentsInfoEntity, OrderCommentsInfo>();
            CreateMap<InvoiceInfoEntity, InvoiceInfo>();
            CreateMap<PackingInvoiceCommentEntity, PackingInvoiceComment>();
            CreateMap<DeliveryChargeEntity, DeliveryCharge>();

            CreateMap<OrderSourceInfoEntity, OrderSourceInfo>();
            CreateMap<OrderStatusCodeDetailEntity, OrderStatusCodeDetail>()
                .ForMember(dest => dest.statusCreateTime, opt => opt.MapFrom(src =>
                        FormatUtils.AddTimezone(src.statusCreateTime)))
                //.ForMember(dest => dest.statusCreateTime, opt => opt.MapFrom(src => 
                //        FormatUtils.ConvertToYYYYMMDDHHMISS(src.statusCreateTime)))
                ;
            CreateMap<OrderStatusDetailEntity, OrderStatusDetail>()
                .ForMember(dest => dest.statusCreateTime, opt => opt.MapFrom(src => FormatUtils.ConvertToYYYYMMDDHHMISS(src.statusCreateTime)));
            CreateMap<SystemIdentifyEntity, SystemIdentify>();
            CreateMap<ReferenceInfoEntity, ReferenceInfo>();

            CreateMap<Order, OrderForm>();
            CreateMap<OrderForm, Order>()
            //    .ForMember(dest => dest.Self, opt => opt.MapFrom(src =>
            //             ConvertToDateYYYYMMDD(src.orderHeader.orderRequestDate))
            //)
            ;

            ///--Product Entity
            ///--V3.1, remove href link for Product.
            CreateMap<ProductEntity, Product>();
              //.ForMember(dest => dest.Self, opt => opt.MapFrom(src =>
              //    Link.ToCollection(
              //        nameof(Controllers.ItemsController.GetProducts),
              //        new { custType = src.priceInfo.custType, items = src.sku })));

            CreateMap<PriceInfoEntity, PriceInfo>()
                .ForMember(dest => dest.last_update, opt => opt.MapFrom(src =>
                  FormatUtils.AddTimezone(src.last_update)));

            CreateMap<ProductInventoryEntity, ProductInventory>()
            //    .ForMember(dest => dest.Self, opt => opt.MapFrom(src =>
            //        Link.ToCollection(
            //            nameof(Controllers.ItemsController.GetProductInventory),
            //            new { custType = src.priceInfo.custType, items = src.sku }))
            //)
            ;

            //CreateMap<Item, Item>()
            //     .ForMember(dest => dest.Self, opt => opt.MapFrom(src =>
            //          Link.To(nameof(Controllers.ItemsController.GetSkuInfo),
            //           new { ItemSku = src.Sku })));

            //CreateMap<PriceEntity, Prices>()
            //    .ForMember(dest => dest.Self, opt => opt.MapFrom(src =>
            //          Link.To(nameof(Controllers.ItemsController.GetPriceById),
            //           new { ItemSku = src.ItemSku })));

            ///--Customer Entity
            //Link park is for href value only. Need to match the parameters for controll funtion so all parms can be displayed in the href link. 
            CreateMap<CustomerEntity, Customer>()
                    .ForMember(dest => dest.Self, opt => opt.MapFrom(src =>
                        Link.To(nameof(Controllers.CustomersController.GetCustomerByNumber), null)));
            CreateMap<AvailableShippingDateEntity, AvailableShippingDate>()
                .ForMember(dest => dest.cutOffTime, opt => opt.MapFrom(src => FormatUtils.AddTimezone(src.CUTOFF_DT)))
                .ForMember(dest => dest.orderRequiredDate, opt => opt.MapFrom(src => FormatUtils.ConvertToYYYYMMDD(src.DELIVERY_DT)));
            CreateMap<RouteInfoEntity, RouteInfo>();
            CreateMap<CustomerContactEntity, CustomerContact>();
            CreateMap<BillToInfoEntity, Address>()
               .ForMember(dest => dest.name, opt => opt.MapFrom(src => src.billToName))
               .ForMember(dest => dest.addressLine1, opt => opt.MapFrom(src => src.billToAddressLine1))
               .ForMember(dest => dest.addressLine2, opt => opt.MapFrom(src => src.billToAddressLine2))
               .ForMember(dest => dest.city, opt => opt.MapFrom(src => src.billToCity))
               .ForMember(dest => dest.provinceCode, opt => opt.MapFrom(src => src.billToProvinceCode))
               .ForMember(dest => dest.country, opt => opt.MapFrom(src => src.billToCountry))
               .ForMember(dest => dest.postalCode, opt => opt.MapFrom(src => src.billToPostalCode))
               .ForMember(dest => dest.phoneNumber, opt => opt.MapFrom(src => src.billToPhoneNumber))
               ;
            CreateMap<ShipToInfoEntity, Address>()
              .ForMember(dest => dest.name, opt => opt.MapFrom(src => src.shipToName))
              .ForMember(dest => dest.addressLine1, opt => opt.MapFrom(src => src.shipToAddressLine1))
              .ForMember(dest => dest.addressLine2, opt => opt.MapFrom(src => src.shipToAddressLine2))
              .ForMember(dest => dest.city, opt => opt.MapFrom(src => src.shipToCity))
              .ForMember(dest => dest.provinceCode, opt => opt.MapFrom(src => src.shipToProvinceCode))
              .ForMember(dest => dest.country, opt => opt.MapFrom(src => src.shipToCountry))
              .ForMember(dest => dest.postalCode, opt => opt.MapFrom(src => src.shipToPostalCode))
              .ForMember(dest => dest.phoneNumber, opt => opt.MapFrom(src => src.shipToPhoneNumber))
              ;

            CreateMap<AddressEntity, Address>();

           // CreateMap<LCODeliveryChargeEntity, LCODeliveryCharge>();

        }

        // Moved this to FormatUtils static class
        //string AddTimezone(string strDate)
        //{
        //    string dateWithTimeZone = strDate;
        //    var dt = new DateTime();


        //    if (DateTime.TryParse(strDate, out dt))
        //    {
        //        // dateWithTimeZone = dt.ToString("yyyy-MM-ddThh:mm:ss") + "-0500";
        //        var dtz = new DateTimeOffset(dt);
        //        dateWithTimeZone = dtz.ToString("yyyy-MM-ddTHH:mm:sszzz");

        //    }

        //    return dateWithTimeZone;
        //}
        //string ConvertToDateYYYYMMDD(string strDatewithTimeZone)
        //{
        //    string dateonly = strDatewithTimeZone;
        //    var dt = new DateTime();


        //    if (DateTime.TryParse(strDatewithTimeZone, out dt))
        //    {
        //        // dateWithTimeZone = dt.ToString("yyyy-MM-ddThh:mm:ss") + "-0500";
        //        var dtz = new DateTimeOffset(dt);
        //        dateonly = dtz.ToString("yyyy-MM-dd");

        //    }

        //    return dateonly;
        //}

    }
}
