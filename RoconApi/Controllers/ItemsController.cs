﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NJsonSchema.Converters;
using Newtonsoft.Json;
using RoconApi.Services;
using RoconLibrary.Domains;
using RoconLibrary.Utility;
using RoconLibrary.Models;
using Microsoft.AspNetCore.Authorization;
using System.Net;
using Authorization = RoconApi.Utility.Authorization;
using System.Text.RegularExpressions;
using System.Collections;

namespace RoconApi.Controllers
{
    [ApiVersion("3.0")]
    [Route("v{v:apiVersion}/rocon/[controller]")]
    //[Route("/v2/rocon/[controller]")]
    [ApiController]
    public class ItemsController : ControllerBase
    {
        private readonly IProductService _productService;
        private readonly IConfigurationProvider _mappingConfiguration;

        public ItemsController(IProductService prodserv
            , IConfigurationProvider mappingConfiguration)
        {
            _productService = prodserv;
            _mappingConfiguration = mappingConfiguration;


        }

        [HttpGet(Name = nameof(GetProducts))]
        [ProducesResponseType(200)]
        [ProducesResponseType(401)]
        [ProducesResponseType(400)]
        public async Task<ActionResult<ProductList<Product>>> GetProducts([FromQuery] string custType, [FromQuery] string items)
        {
            //In Gold V3.0, remove bearer token. Use Kong API Gateway.
            //if (Environment.GetEnvironmentVariable("CHECKSOUP") == "1")
            //{
            //    var bToken = Request.Headers["AUTHORIZATiON"];
            //    if (!Authorization.isAuthorized(bToken.ToString()))
            //    {
            //        return Unauthorized();
            //    }
            //}
            
            //add validation to check for
            var paramCustType = custType;
            var paramItem = items;


            if (String.IsNullOrEmpty(paramCustType))
            {
                //Send BadError
                //TODO: Formulate Error model to wrap around resource
                return BadRequest("custtype is required!");
            }

            var collection = new List<IEnumerable<Product>>();
            if (String.IsNullOrEmpty(paramItem))
            {
                paramItem = "";

                collection.Add(await _productService.GetProducts(paramCustType, paramItem));

            }
            else
            {
                
                //validate if list of skus are invalid
                bool isNumeric = Regex.IsMatch(Regex.Replace(items, ",", ""), "^\\d+$");
                
                if (!isNumeric)
                {
                    return BadRequest("Invalid list of items found!");
                }

                //need to split the list to max 500 skus per call
                List<string> skulists = Regex.Split(items, ",").ToList();
                int fetchsize = 500; //get this value from settings in future
                var listOfList = FormatUtils.splitList<string>(skulists, fetchsize);

                foreach (var sList in listOfList.ToList())
                {
                    var newList = string.Join(",", sList.ToList());
                    var products = await _productService.GetProducts(paramCustType, newList);

                    collection.Add(products);
                }

            } // else (itemlist is not empty)

            if (collection.Count == 0)
            {
                return NotFound();
            }
            //merge the list
            ProductList<Product> lstProducts = new ProductList<Product>
            {
                Products = collection.SelectMany(i => i).ToArray()
            };

            
            return lstProducts;

        } //GetProducts


        //[HttpGet("Inventory", Name = nameof(GetProductInventory))]
        //[ProducesResponseType(200)]
        //public async Task<ActionResult<ProductList<ProductInventory>>> GetProductInventory([FromQuery] string custType, [FromQuery] string items)
        //{

        //    //add validation to check for
        //    var paramCustType = custType;
        //    var paramItem = items;

        //    if (String.IsNullOrEmpty(paramCustType))
        //    {
        //        //Send BadError
        //        //TODO: Formulate Error model to wrap around resource
        //        return BadRequest("custtype is required!");
        //    }

        //    if (String.IsNullOrEmpty(paramItem))
        //    {
        //        paramItem = "";
        //    }


        //    //  var products = await _productService.GetProducts(custType, item);
        //    var products = await _productService.GetProductInventory(paramCustType, paramItem);

        //    var collection = new ProductList<ProductInventory>
        //    {
        //        Self = Link.To(nameof(GetProductInventory)),
        //        Products = products.ToArray()
        //    };

        //    return collection;

        //}


        //// GET /items?custtype{custType}&items{sku}
        //[HttpGet("{custType?}",Name = nameof(GetProductById))]
        //[ProducesResponseType(200)]
        //[ProducesResponseType(404)]
        //// [ResponseCache(CacheProfileName = "Resource")]

        //public async Task<ActionResult<Product>> GetProductById([FromQuery] string custType="A3",int sku=18)
        //{
        //    var entity = await _productService.GetProductByIdAsync(custType,sku);
        //    if (entity == null) return NotFound();

        //    var mapper = _mappingConfiguration.CreateMapper();
        //    var prod = mapper.Map<Product>(entity);

        //    var jsonD = JsonConvert.SerializeObject(entity, Formatting.None,
        //        new JsonSerializerSettings()
        //        {
        //            ReferenceLoopHandling = ReferenceLoopHandling.Serialize
        //        });

        //    return entity;

        //}


    } // class ItemsController
} //namespace

