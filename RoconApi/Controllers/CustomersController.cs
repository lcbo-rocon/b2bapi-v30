﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using RoconLibrary.Domains;
using RoconLibrary.Models;
using RoconLibrary.Utility;
using RoconApi.Services;
using RoconApi.Utility;
using Microsoft.AspNetCore.Authorization;

namespace RoconApi.Controllers
{
    [ApiVersion("3.0")]
    [Route("v{v:apiVersion}/rocon/[controller]")]
    //[Route("/v2/rocon/[controller]")]
    [ApiController]
    public class CustomersController : ControllerBase
    {
        private readonly ICustomerServices _customerservices;
        private readonly IConfigurationProvider _mappingConfiguration;
        public CustomersController(ICustomerServices customerServices,IConfigurationProvider mappingConfiguration)
        {
            _customerservices = customerServices;
            _mappingConfiguration = mappingConfiguration;
        }

        //GET /customers
        [HttpGet(Name = nameof(GetAllCustomers))]
        [ProducesResponseType(200)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        public async Task<ActionResult<CustomerList<Customer>>>GetAllCustomers(string custType = "0")
        {
            //In Gold V3.0, remove bearer token. Use Kong API Gateway.
            //if (Environment.GetEnvironmentVariable("CHECKSOUP") == "1")
            //{
            //    var bToken = Request.Headers["AUTHORIZATiON"];
            //    if (!Authorization.isAuthorized(bToken.ToString()))
            //    {
            //        return Unauthorized();
            //    }
            //}

            return BadRequest("Get Customer List is not available in Bronze.");
            //throw new NotImplementedException();
            //var pcustType = custType;
            //var data = await _customerservices.GetAllCustomersAsync(pcustType);
            //if (data == null) return NotFound();
            //var collection = new CustomerList<Customer>
            //{
            //    Self = Link.To(nameof(GetAllCustomers)),
            //    Customers = data.ToArray()
            //};
            //return collection;

            
                       
        }

        // GET /customers/{custnumber}
        [HttpGet("{custNum}", Name = nameof(GetCustomerByNumber))]
        [ProducesResponseType(200)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        public async Task<ActionResult<Customer>> GetCustomerByNumber(string custNum)
        {
            //In Gold V3.0, remove bearer token. Use Kong API Gateway.
            //if (Environment.GetEnvironmentVariable("CHECKSOUP") == "1")
            //{
            //    var bToken = Request.Headers["AUTHORIZATiON"];
            //    if (!Authorization.isAuthorized(bToken.ToString()))
            //    {
            //        return Unauthorized();
            //    }
            //}
            var cNum = custNum;
            var data = await _customerservices.GetCustomerByNumberAsync(cNum);
            if (data == null) return NotFound();
            
            var mapper = _mappingConfiguration.CreateMapper();
            var cust = mapper.Map<Customer>(data);

            if (cust.errorInfo == null)
            {
                //add validation before sending back 
                return cust;
            }
            else
            {
                if (cust.errorInfo.Count() > 0)
                {
                    return BadRequest(cust);
                }
                else
                {
                    return cust;
                }
            }
        }
    }
}
