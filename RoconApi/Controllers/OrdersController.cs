﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RoconApi.Services;
//using RoconApi.Models;
//using RoconApi.Utility;
using RoconLibrary.Domains;
using RoconLibrary.Models;
using RoconLibrary.Utility;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using System.IO;
using Newtonsoft.Json;
using System.Collections;
using System.Text;

using Newtonsoft.Json.Linq;
//using Newtonsoft.Json.Schema;
using System.Text.RegularExpressions;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using Microsoft.Extensions.Configuration;
using System.Net.Http;
using Dapper.Oracle;
using System.Net;

namespace RoconApi.Controllers
{
    [ApiVersion("3.0")]
    [Route("v{v:apiVersion}/rocon/[controller]")]
    //[Route("/v2/rocon/[controller]")]
    [ApiController]
    public class OrdersController : ControllerBase
    {
        private readonly IOrderService _orderService;
        private readonly AutoMapper.IConfigurationProvider _mappingConfiguration;
        private readonly IProductService _productService;
        private readonly ICustomerServices _customerServices;
        private readonly IHostingEnvironment _environment;
        private readonly IConfiguration _configuration;
        //private readonly JSchema _jschema;

        public OrdersController(IOrderService orderService
            , AutoMapper.IConfigurationProvider mappingConfiguration
            , IProductService productService
            , ICustomerServices customerService
            , IHostingEnvironment environment
            , IConfiguration configuration)
        {
            _orderService = orderService;
            _mappingConfiguration = mappingConfiguration;
            _productService = productService;
            _customerServices = customerService;
            _environment = environment;
            _configuration = configuration;

            //var schemaFile = Path.Combine(_environment.ContentRootPath, "rocon.order.schema.json");
            //_jschema = JSchema.Parse(System.IO.File.ReadAllText(schemaFile));
        }

        public IDbConnection Connection
        {
            get
            {
                return new OracleConnection(_configuration.GetConnectionString("roconconnection"));
            }
        }

        // POST: api/Orders
        // In Silver Phase, trigger by WCS to create order in Wait W status, event is INV_HOLD, reserve inventory for this order. 
        // In Silver phase, trigger by OMS to update order to Open O status, event is New.

        [HttpPost(Name = nameof(SubmitNewOrder))]
        //public async Task<ActionResult<Order>> SubmitNewOrder([FromBody] OrderForm newOrder)
        public async Task<ActionResult<Order>> SubmitNewOrder([FromBody] JObject orderStr)
        {
            //In Gold 3.0, Remove bearer token. Using Kong API gateway.
            //if (Environment.GetEnvironmentVariable("CHECKSOUP") == "1")
            //{
            //    var bToken = Request.Headers["AUTHORIZATiON"];
            //    if (!RoconApi.Utility.Authorization.isAuthorized(bToken.ToString()))
            //    {
            //        return Unauthorized();
            //    }
            //}

            IDbConnection conn = Connection;
            conn.Open();

            var newOrder = new OrderForm();
            var newOrderErrorInfo = new List<ErrorDetail>();

            var payload = orderStr.ToString();
            string resp = "Response Body";
            try
            {
                newOrder = JsonConvert.DeserializeObject<OrderForm>(payload);
            }
            catch (Exception e)
            {
                newOrderErrorInfo.Add(
                    new ErrorDetail()
                    {
                        errorType = ErrorType.OTHER.ToString(),
                        errorMessage = "Order Format is not right. Message = " + e.Message
                    }
                );
                resp = JsonConvert.SerializeObject(newOrderErrorInfo);
            }
            //If cannot convert to Order Json, no point going to check further
            if (newOrderErrorInfo.Count > 0)
            {
                insertToLcoOrderJson(null, payload, resp, ((int)HttpStatusCode.BadRequest).ToString(), "", conn);
                return BadRequest(newOrderErrorInfo);
            }

            var order = newOrder;
            bool hasErrors = false;

            //--validate the order form data. 
            //--If missing numberOfLineItems, will get value 0, will fail min 1 requirement.
            //--If missing property as string, will get null, no expection when trying to get value.
            var useSchema = Environment.GetEnvironmentVariable("LCO_USEJSCHEMA");
            if (useSchema == "1")
            {
                //provide path to JsonSchema file
                var jsonSchemaFile = Path.Combine(_environment.ContentRootPath, "lco.order.schema.gold.json");

                IList<string> errors = OrderJsonValidator.ValidateOrderJson(jsonSchemaFile, order);
                if (errors != null)
                {
                    //build error details
                    //var JsonErrors = new List<ErrorDetail>();
                    foreach (var err in errors)
                    {
                        newOrderErrorInfo.Add(
                            new ErrorDetail()
                            {
                                errorType = ErrorType.INVALID_ORDER.ToString(),
                                errorMessage = err
                            }
                        );
                    }
                }
            }

            //--Need to check orderItems,  priceinfo.sellingprice, curitem.quantity
            //--In orderService, null Int will be 0, null string can pass, null obj will raise exception.            
            //--Pre-Check objects for orderService: ShipToInfo, orderSourceInfo, routeInfo, lcoOrderSystem,orderStatus. referenceInfo is array of object, cannot new.  
            //--In schema, required: orderType, customerNumber.
            //--In domain, JsonRequired defined: orderSourceInfo, orderevent, ordertype, customerNumber, lcoordersystem, 
            //--orderRequestDate, orderRequiredDate, itemlineNumber, itemSku, itemQuantity, routeInfo.routecode (new in V2)

            if (order.orderHeader.orderSourceInfo == null)
            {
                // var tt = order.orderHeader.orderSourceInfo.GetType(); //Will get error.
                order.orderHeader.orderSourceInfo = new OrderSourceInfo();
            }

            if (order.orderHeader.shipToInfo == null)
            {
                order.orderHeader.shipToInfo = new OrderShipToInfo();
            }

            if (order.orderHeader.routeInfo == null)
            {
                order.orderHeader.routeInfo = new RouteInfo();
                newOrderErrorInfo.Add(
                                new ErrorDetail()
                                {
                                    errorType = ErrorType.MISSING_ELEMENT.ToString(),
                                    errorMessage = "Missing route info."
                                }
                            );
            }

            if (order.orderHeader.referenceInfo == null)
            {
                //--Expecting at least one reference info from WCS or OMS
                newOrderErrorInfo.Add(
                                new ErrorDetail()
                                {
                                    errorType = ErrorType.MISSING_ELEMENT.ToString(),
                                    errorMessage = "Missing reference info."
                                }
                            );
            }

            //--In W, don't need to display null orderStatus. 
            //if (order.orderHeader.orderStatus == null)
            //{
            //    order.orderHeader.orderStatus = new OrderStatusCodeDetail();
            //}

           
            if (newOrderErrorInfo.Count > 0)
            {
                //no point going to check further
                order.errorInfo = newOrderErrorInfo.ToArray();
                resp = JsonConvert.SerializeObject(order);
                insertToLcoOrderJson(order, payload, resp, ((int)HttpStatusCode.BadRequest).ToString(), "", conn);
                return BadRequest(order);
            }

            // Prepare Item list for item validation and price info.
            string custType = order.orderHeader.orderType;

            var keylist = order.orderItems.Select(k => k.sku).ToArray();
            string items = String.Join(",", keylist);

            // Need to split call for orders with more than 500 items
            // need to split the list to max 500 skus per call
            List<string> skulists = Regex.Split(items, ",").ToList();
            int fetchsize = 500; //get this value from settings in future
            var listOfList = FormatUtils.splitList<string>(skulists, fetchsize);
            var collection = new List<IEnumerable<ProductInventory>>();

            foreach (var sList in listOfList.ToList())
            {
                var newList = string.Join(",", sList.ToList());
                var products = await _productService.GetProductInventory(custType, newList);

                collection.Add(products);
            }

            var productInventoryList = collection.SelectMany(i => i);

            //check orderEvent to determine next steps
            //for Silver phase, we will implement 'INV_HOLD' from WCS,'NEW' from OMS
            //INV_HOLD will create 'W' status and get ROCON order number.
            //NEW will update order from 'W' to 'O' status, and create Invoice.
            //Gold V3.0, will have cancel function when WCS calling to cancel order in 'W' status.
            //Gold V3.1, will have INVOICED event when OMS calling RoconAPI

            var mapper = _mappingConfiguration.CreateMapper();
            var rocOrder = mapper.Map<Order>(order);

            if (order.orderHeader.orderEvent.ToLower() == OrderEvent.Inv_hold.ToString().ToLower())
            {
                //check for valid order types
                if (!(custType == "AGY" || custType == "A3" || custType == "LCO" || custType == "LIC" || custType == "REG"))
                {
                    newOrderErrorInfo.Add(
                        new ErrorDetail()
                        {
                            errorType = ErrorType.INVALID_ORDER.ToString(),
                            errorMessage = nameof(order.orderHeader.orderType).ToString() + " provided is invalid!",
                            errorData = new ErrorDetail.DataValue()
                            {
                                key = nameof(order.orderHeader.orderType).ToString(),
                                value = "{AGY,A3,LCO,LIC,REG}"
                            }
                        });
                }

                //check lineitems field matches
                if (newOrder.orderItems.Count() != newOrder.orderHeader.numberOfLineItems)
                {
                    newOrderErrorInfo.Add(
                        new ErrorDetail()
                        {
                            errorType = ErrorType.INVALID_ORDER.ToString(),
                            errorMessage = "Total Line Items in Header does not match numbers in orderItems collection!",
                            errorData = new ErrorDetail.DataValue()
                            {
                                key = nameof(order.orderHeader.numberOfLineItems),
                                value = newOrder.orderItems.Count().ToString()
                            }
                        });
                }

                //check for duplicate items
                var duplicateItems = newOrder.orderItems.GroupBy(i => i.sku)
                    .Where(g => g.Count() > 1)
                    .Select(g => g.Key);

                if (duplicateItems.Any())
                {
                    StringBuilder sb = new StringBuilder();
                    foreach (var dup in duplicateItems)
                    {
                        if (sb.Length != 0)
                        {
                            sb.Append(",");
                        }
                        sb.Append("SKU: " + dup);
                        //get the list of line items with duplicates
                        var errLineNo = String.Join(",", order.orderItems.Where(x => x.sku == dup)
                                                         .Select(y => y.lineNumber).ToArray());

                        sb.Append(" lineNumber: " + errLineNo);
                    }
                    newOrderErrorInfo.Add(
                      new ErrorDetail()
                      {

                          errorType = OrderItemErrorType.INVALID_ORDER.ToString(),
                          errorMessage = "Duplicate Items found in order!",
                          errorData = new ErrorDetail.DataValue()
                          {
                              key = nameof(order.orderItems),
                              value = sb.ToString()
                          }
                      });
                }

                //Validate Customer information - customer is still active and is in good standing and check provided
                //available delivery dates selected is still valid
                var customer = await _customerServices.GetCustomerByNumberAsync(order.orderHeader.customerNumber);
                if (customer == null)
                {
                    newOrderErrorInfo.Add(
                    new ErrorDetail()
                    {
                        errorType = ErrorType.OTHER.ToString(),
                        errorMessage = "Customer not found!",
                        errorData = new ErrorDetail.DataValue()
                        {
                            key = "CustomerNumber",
                            value = "Customer not found in system."
                        }
                    });
                }
                else
                {
                    if (customer.customerStatus != "ACTIVE")
                    {
                        newOrderErrorInfo.Add(
                            new ErrorDetail()
                            {
                                errorType = ErrorType.OTHER.ToString(),
                                errorMessage = "Customer status changed and is prohibited from ordering!",
                                errorData = new ErrorDetail.DataValue()
                                {
                                    key = "CustomerStatus",
                                    value = customer.customerStatus
                                }
                            });
                    }
                    else
                    {
                        if (!(order.orderHeader.routeInfo.routeCode == null))
                        {
                            if (customer.routeInfo.routeCode != order.orderHeader.routeInfo.routeCode)
                            {
                                newOrderErrorInfo.Add(
                                    new ErrorDetail()
                                    {
                                        errorType = ErrorType.OTHER.ToString(),
                                        errorMessage = "Customer route code changed!",
                                        errorData = new ErrorDetail.DataValue()
                                        {
                                            key = "RouteCode",
                                            value = customer.routeInfo.routeCode
                                        }
                                    });
                            }
                        }
                        if (customer.availableShippingDates.Count() != 0)
                        {
                            //If order required date is earlier than earlist available shipping date, it means order pass the cutoff time. 
                            //Need to reject the order and provide next available ship date.
                            var availableDate = customer.availableShippingDates.Where(p => FormatUtils.ToDate(p.cutOffTime) >= DateTime.Now).First();
                            if (FormatUtils.ToDate(order.orderHeader.orderRequiredDate) <
                                FormatUtils.ToDate(availableDate.orderRequiredDate))
                            {
                                newOrderErrorInfo.Add(
                                    new ErrorDetail()
                                    {
                                        errorType = ErrorType.INVALID_DATE.ToString(),
                                        errorMessage = "Invalid Order Required Date!",
                                        errorData = new ErrorDetail.DataValue()
                                        {
                                            key = "OrderRequiredDate",
                                            value = FormatUtils.ConvertToYYYYMMDD(availableDate.orderRequiredDate)
                                        }
                                    });
                            }
                            else
                            {
                                //V3.0, Add business rule, not allow customer to place second order for the same ship date.
                                var orderSelectedShipDate = FormatUtils.ConvertToYYYYMMDD(order.orderHeader.orderRequiredDate);
                                var availableShipDate = customer.availableShippingDates.Where(p => p.orderRequiredDate == orderSelectedShipDate); // .First();
                                if (availableShipDate?.Any() == false)
                                //if (availableShipDate == null)
                                {
                                    newOrderErrorInfo.Add(
                                           new ErrorDetail()
                                           {
                                               errorType = ErrorType.INVALID_DATE.ToString(),
                                               errorMessage = "Invalid Order Required Date!",
                                               errorData = new ErrorDetail.DataValue()
                                               {
                                                   key = "OrderRequiredDate",
                                                   value = FormatUtils.ConvertToYYYYMMDD(availableDate.orderRequiredDate)
                                               }
                                           });
                                }
                            }
                        }
                        else
                        {
                            newOrderErrorInfo.Add(
                                   new ErrorDetail()
                                   {
                                       errorType = ErrorType.INVALID_DATE.ToString(),
                                       errorMessage = "No available delivery dates for this customer " + customer.customerNumber,
                                       errorData = new ErrorDetail.DataValue()
                                       {
                                           //Gold 3.0 using customer number not route code to get delivery date.
                                           key = "CustomerNumber",
                                           value = customer.customerNumber
                                       }
                                   });
                        }

                    }
                }

                //validate if list of skus are invalid
                bool isNumeric = Regex.IsMatch(Regex.Replace(items, ",", ""), "^\\d+$");
                if (!isNumeric)
                {
                    newOrderErrorInfo.Add(
                    new ErrorDetail()
                    {
                        errorType = OrderItemErrorType.OTHER.ToString(),
                        errorMessage = "Invalid item number found!",
                    });
                }

                if (newOrderErrorInfo.Count > 0)
                {
                    //no point going to check further
                    order.errorInfo = newOrderErrorInfo.ToArray();
                    resp = JsonConvert.SerializeObject(order);
                    insertToLcoOrderJson(order, payload, resp, ((int)HttpStatusCode.BadRequest).ToString(), "", conn);
                    return BadRequest(order);
                }

                //check price and inventory
                foreach (var curitem in order.orderItems)
                {
                    var itemfound = productInventoryList.Any(x => x.sku == curitem.sku);

                    var sErrorList = new List<ErrorDetail>();

                    //validate item available for sale to that customer type
                    if (!itemfound)
                    {
                        //item is not valid for sale to this customer type
                        var keyProperty = (typeof(OrderItemPriceInfo).GetProperties().Select(p => p.GetCustomAttribute<JsonPropertyAttribute>()).Select(jp => jp.PropertyName));
                        sErrorList.Add(new ErrorDetail()
                        {
                            errorType = OrderItemErrorType.SKU_ERROR.ToString(),
                            errorMessage = "SKU not for sale to this customer type",
                            errorData = new ErrorDetail.DataValue()
                            {
                                key = "itemSku",
                                value = curitem.sku.ToString()
                            }
                        });

                    } //no point in continuing the validation since item data is not available
                    else
                    {
                        var rocitem = productInventoryList.Where(x => x.sku == curitem.sku).First();
                        // woo commerce is not ready to pass us the unit price 
                        // this check should be implemented in gold as an additional check, when OMS sends the unit price. 

                        //if (rocitem.priceInfo.unit_price != curitem.priceInfo.unitPrice)
                        //{

                        //    sErrorList.Add(new ErrorDetail()
                        //    {
                        //        errorType = OrderItemErrorType.PRICE_ERROR.ToString(),
                        //        errorMessage = "Price mismatch",
                        //        errorData = new ErrorDetail.DataValue()
                        //        {
                        //            key = "itemBasicPrice",
                        //            value = rocitem.priceInfo.unit_price.ToString()
                        //        }

                        //    });
                        //}

                        if (rocitem.priceInfo.selling_price != curitem.priceInfo.selling_price)
                        {

                            //rocitem.GetType().GetProperty("itemSellingPrice").GetValue(rocitem, null);
                            foreach (var prop in rocitem.priceInfo.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance))
                            {
                                var propertyName = prop.Name;
                                
                                if ((propertyName != "custType") && (propertyName != "last_update") && (propertyName != "status"))
                                {
                                    var keyProperty = (typeof(OrderItemPriceInfo).GetProperties().Select(p => p.GetCustomAttribute<JsonPropertyAttribute>()).Select(jp => jp.PropertyName));
                                    var jName = prop.GetCustomAttribute<JsonPropertyAttribute>().PropertyName.ToString();
                                    var propertyValue = rocitem.priceInfo.GetType().GetProperty(propertyName).GetValue(rocitem.priceInfo, null);

                                    sErrorList.Add(new ErrorDetail()
                                    {
                                        errorType = OrderItemErrorType.PRICE_ERROR.ToString(),
                                        errorMessage = "Price mismatch",
                                        errorData = new ErrorDetail.DataValue()
                                        {
                                            key = jName,
                                            value = propertyValue.ToString()
                                        }
                                    });
                                }
                            }

                            //sErrorList.Add(new ErrorDetail()
                            //{
                            //    errorType = OrderItemErrorType.PRICE_ERROR.ToString(),
                            //    errorMessage = "Price mismatch",
                            //    errorData = new ErrorDetail.DataValue()
                            //    {
                            //        key = "itemSellingPrice",
                            //        value = rocitem.priceInfo.selling_price.ToString()
                            //    }
                            //});
                        }

                        if (rocitem.Inventory.AvailableForSale < curitem.quantity)
                        {
                            var available = rocitem.Inventory.AvailableForSale / rocitem.selling_increments * rocitem.selling_increments;
                            if (available < rocitem.selling_increments)
                            {
                                available = 0;
                            }
                            sErrorList.Add(new ErrorDetail()
                            {
                                errorType = OrderItemErrorType.INVENTORY_ERROR.ToString(),
                                errorMessage = "There is insufficient inventory for this item",
                                errorData = new ErrorDetail.DataValue()
                                {
                                    key = "quantity",
                                    value = available.ToString()
                                }
                            });
                        }

                        //check if ordered item is by the selling increments
                        if (curitem.quantity % rocitem.selling_increments > 0)
                        {
                            sErrorList.Add(new ErrorDetail()
                            {
                                errorType = OrderItemErrorType.OTHER.ToString(),
                                errorMessage = "item needs to be bought by increments of " + rocitem.selling_increments,
                                errorData = new ErrorDetail.DataValue()
                                {
                                    key = "quantity",
                                    value = rocitem.selling_increments.ToString()
                                }
                            });
                        }
                    }

                    if (sErrorList.Count > 0)
                    {
                        curitem.errorInfo = sErrorList.ToArray<ErrorDetail>();
                        hasErrors = true;
                    }
                }

                if (hasErrors || newOrderErrorInfo.Count > 0)
                {
                    newOrderErrorInfo.Add(
                         new ErrorDetail()
                         {
                             errorType = ErrorType.INVALID_ORDER.ToString(),
                             errorMessage = "Please correct the errors above and resubmit"
                         }
                    );

                    order.errorInfo = newOrderErrorInfo.ToArray();
                    resp = JsonConvert.SerializeObject(order);
                    insertToLcoOrderJson(order, payload, resp, ((int)HttpStatusCode.BadRequest).ToString(), "", conn);
                    return BadRequest(order);
                }

                // If all ok, CreateOrderHere with "W" status. 
                // Response No OrderStatus and PublishedStatus. 
                // Response sorSystem, refSystem, order source change, orderShipDate
                // Response calculate invoice, not stored in DB
                // Response will not change ordercreationdate, keep wcs passing value.
                // V3.1, response Delivery Charge E inv. 
                rocOrder.orderHeader.orderShipmentDate = rocOrder.orderHeader.orderRequiredDate;

                rocOrder = await _orderService.ReserveInventory(rocOrder, productInventoryList);

                //SZL - orderShipmentDate needs to be stored in DB and not for display purposes only
                //updated orderShipmentDate before call to ReserveInventory see above
                //
                //if (string.IsNullOrEmpty(rocOrder.orderHeader.orderShipmentDate))
                //{
                //    rocOrder.orderHeader.orderShipmentDate = rocOrder.orderHeader.orderRequiredDate;
                //}

                if (rocOrder.errorInfo.Count() > 0)
                {
                    order.errorInfo = rocOrder.errorInfo;
                    resp = JsonConvert.SerializeObject(rocOrder);
                    insertToLcoOrderJson(order, payload, resp, ((int)HttpStatusCode.BadRequest).ToString(), "", conn);
                    return BadRequest(order);
                }
               //rocOrder = await _orderService.GetOrderByIdAsync(rocOrder.orderHeader.sorOrderNumber.ToString());
            }
            else if (order.orderHeader.orderEvent.ToLower() == OrderEvent.New.ToString().ToLower())
            {
                //Get request from OMS, event New, create invoice in ROCON and update to O status.
                //Response No order status and published status.
                //Response invoice, 2 refSystems, published status
                //If Response need to change order statuscreattime with ROCON time, will need a seperate call to get ON_COHDR.timestamp for updatedtime.
                //response will not change ordercreationdate, will keep OMS passing value. if need change, will need seperate call to get ROCON timestamp.
                rocOrder = await _orderService.CreateOrder(rocOrder, productInventoryList);
                if (rocOrder.errorInfo.Count() > 0)
                {
                    order.errorInfo = rocOrder.errorInfo;
                    resp = JsonConvert.SerializeObject(rocOrder);
                    insertToLcoOrderJson(order, payload, resp, ((int)HttpStatusCode.BadRequest).ToString(), "", conn);
                    return BadRequest(order);
                }
     
                ////get published order status
                //try
                //{
                //    var oe1 = new OrderEntity();
                //    var ohe1 = new OrderHeaderEntity();
                //    oe1.orderHeader = ohe1;
                //    var te1 = new Collection<OrderStatusDetailEntity>();
                //    oe1.orderHeader.publishedOrderStatus = te1.Value;

                //    var orderPub = await _orderService.GetOrderPublishedStatus(order.orderHeader.sorOrderNumber.ToString());
                //    if (orderPub.Count() > 0)
                //    {
                //        oe1.orderHeader.publishedOrderStatus = orderPub.ToArray();
                //        var o1 = mapper.Map<Order>(oe1);
                //        if (rocOrder.orderHeader.publishedOrderStatus == null)
                //        {
                //            var t = new Collection<OrderStatusDetail>();
                //            rocOrder.orderHeader.publishedOrderStatus = t.Value;
                //        }
                //        rocOrder.orderHeader.publishedOrderStatus = o1.orderHeader.publishedOrderStatus;
                //    }
                //}
                //catch
                //{ 
                //    newOrderErrorInfo.Add(
                //        new ErrorDetail()
                //        {
                //            errorType = OrderItemErrorType.INVALID_ORDER.ToString(),
                //            errorMessage = "Getting published order status has issue."
                //        }
                //         );
                //}
                //finally
                //{ }
            }
            else if (order.orderHeader.orderEvent.ToLower() == OrderEvent.Cancel.ToString().ToLower())
            {

                hasErrors = false;
                 
                //When Rocon response WCS for 'W' order, no orderStatus in order payload.

                rocOrder = await _orderService.CancelOrder(rocOrder);
                if (rocOrder.errorInfo.Count() > 0)
                {
                    order.errorInfo = rocOrder.errorInfo;
                    resp = JsonConvert.SerializeObject(rocOrder);
                    insertToLcoOrderJson(order, payload, resp, ((int)HttpStatusCode.BadRequest).ToString(), "", conn);
                    return BadRequest(order);
                }
            }
            else if (order.orderHeader.orderEvent.ToLower() == OrderEvent.Invoiced.ToString().ToLower())
            {
                rocOrder = await _orderService.InvoicedOrder(rocOrder);
                if (rocOrder.errorInfo.Count() > 0)
                {
                    order.errorInfo = rocOrder.errorInfo;
                    resp = JsonConvert.SerializeObject(rocOrder);
                    insertToLcoOrderJson(order, payload, resp, ((int)HttpStatusCode.BadRequest).ToString(), "", conn);
                    return BadRequest(order);
                }
            }
            else if (order.orderHeader.orderEvent.ToLower() == OrderEvent.Return.ToString().ToLower())
            {
                newOrderErrorInfo.Add(
                new ErrorDetail()
                {
                    errorType = ErrorType.INVALID_ORDER.ToString(),
                    errorMessage = "Return order is not implemented yet"
                }
                 );
            }
            else if (! Enum.IsDefined(typeof(OrderEvent), order.orderHeader.orderEvent.ToLower() ))
            {
                newOrderErrorInfo.Add(
                new ErrorDetail()
                {
                    errorType = ErrorType.INVALID_ORDER.ToString(),
                    errorMessage = "Order event " + order.orderHeader.orderEvent.ToUpper() + " is not implemented yet"
                }
                 );
                order.errorInfo = newOrderErrorInfo.ToArray();
                //resp = JsonConvert.SerializeObject(order).Replace("'", "\"");
                resp = JsonConvert.SerializeObject(order);
                insertToLcoOrderJson(order, payload, resp, ((int)HttpStatusCode.BadRequest).ToString(), "", conn);
                return BadRequest(order);
            }

            //OK I guess everything went well - let's clean up the errorInfo
            rocOrder.errorInfo = null;
            resp = JsonConvert.SerializeObject(rocOrder);
            insertToLcoOrderJson(order, payload, resp, ((int)HttpStatusCode.OK).ToString(), "", conn);
            return rocOrder;
        }

        //--Method to save Json body to database
        //szl: update sql to use parms for JSON body 
        public static void insertToLcoOrderJson(OrderForm order,
                                            string jsonRequest,
                                            string jsonResponse,
                                            string returnCode,
                                            string comments,
                                            IDbConnection connection)
        {
            bool success = true;
            try
            {   //ConnectionUtil.openConnection(connection);
                var dateStr = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                var messageSource = "RoconAPI";
                var messageInOut = "";
                var sql = "";
                comments = "V3.1.1: " + comments;
                //jsonResponse = jsonResponse.Replace("'", "\"");
                if (order == null)
                {
                    sql = "Insert into LCO_ORDER_JSON (LCOORDERNUMBER, SORORDERNUMBER, JSON_BODY, LOAD_DT_TM, MESSAGE_SOURCE, MESSAGE_INOUT, RETURN_CODE, RETURN_JSON, COMMENTS) values " +
                        $"(Null, Null, :JSON_REQ, TO_DATE('{dateStr}', 'YYYY-MM-DD HH24:MI:SS'), '{messageSource}'" +
                        $", '{messageInOut}', :RETN_CODE, :JSON_RESP, :JSON_COMM)";
                }
                else
                {
                    sql = "Insert into LCO_ORDER_JSON (LCOORDERNUMBER, SORORDERNUMBER, JSON_BODY, LOAD_DT_TM, MESSAGE_SOURCE, MESSAGE_INOUT, RETURN_CODE, RETURN_JSON, COMMENTS) values " +
                      $"('{order.orderHeader.lcoOrderNumber}', '{order.orderHeader.sorOrderNumber}', :JSON_REQ, TO_DATE('{dateStr}', 'YYYY-MM-DD HH24:MI:SS'), '{messageSource}'" +
                      $", '{messageInOut}', :RETN_CODE, :JSON_RESP, :JSON_COMM)";
                }
                

                var oraclCmd = connection.CreateCommand();
                oraclCmd.CommandText = sql;
                oraclCmd.Parameters.Clear();
                oraclCmd.Parameters.Add(new OracleParameter(":JSON_REQ", jsonRequest));
                oraclCmd.Parameters.Add(new OracleParameter(":RETN_CODE", returnCode));
                oraclCmd.Parameters.Add(new OracleParameter(":JSON_RESP", jsonResponse));
                oraclCmd.Parameters.Add(new OracleParameter(":JSON_COMM", comments));
                int inserted = oraclCmd.ExecuteNonQuery();
                success = true;
            } catch(Exception e)
            {
                //newOrderErrorInfo.Add(
                //        new ErrorDetail()
                //        {
                //            errorType = OrderItemErrorType.INVALID_ORDER.ToString(),
                //            errorMessage = "A general error exception happened when saving Json. Message = " + e.Message
                //        });
                success = false;
            }
            //return success;
        }
    }
}
