﻿using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Versioning;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using NSwag.AspNetCore;
using RoconLibrary.Filters;
using RoconApi.Services;
using RoconApi.Utility;
using RoconApi.Services.Implementation;
using Newtonsoft.Json.Schema;

namespace RoconApi
{
    //public class JwtAuthentication
    //{
    //    public string SecurityKey { get; set; }
    //    public string ValidIssuer { get; set; }
    //    public string ValidAudience { get; set; }

    //    public SymmetricSecurityKey SymmetricSecurityKey => new SymmetricSecurityKey(Encoding.UTF8.GetBytes(@"Wl: of5[PMK] * 6EfhzjGSEUJqqZLEBu12"));
    //    public SigningCredentials SigningCredentials => new SigningCredentials(SymmetricSecurityKey, SecurityAlgorithms.HmacSha256);
    //}

    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            //adding JSON Schema License Key
            string licenseKey = "4030-KRCTDByBvfRZ3aXEppoBtH9nLfGtHxhe0eca6HnP9U3KzF14Rhc2gipGFQcjaUZgXuIlH/r0SuSWuiVMNSLr3FhLrELh1dakjJbWUTTzUaRm89kpO5+lw9JymUpskvipx1nn7gOONoh4hBRQTlfK63LJEyc209MrysKEKmrcWLN7IklkIjo0MDMwLCJFeHBpcnlEYXRlIjoiMjAyMC0wOS0xN1QyMDowMjo0My40ODU5MzIxWiIsIlR5cGUiOiJKc29uU2NoZW1hU2l0ZSJ9";
            License.RegisterLicense(licenseKey);
        }

        public IConfiguration Configuration { get; }
      

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Use in-memory database for quick dev and testing
            // TODO: Swap out for a real database in production
            //services.AddDbContext<DataContext>(options => options.UseInMemoryDatabase("myApiDb"));
            services.AddScoped<ICustomerServices, CustomerServices>();
            services.AddScoped<IProductService, ProductService>();
            services.AddScoped<IOrderService, OrderService>();


            //var sharedKey = new SymmetricSecurityKey(
            //            Encoding.UTF8.GetBytes("Wl:of5[PMK]*6EfhzjGSEUJqqZLEBu12"));
            //services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
            //.AddJwtBearer(options =>
            //{
            //    options.TokenValidationParameters = new TokenValidationParameters
            //    {
            //        // Clock skew compensates for server time drift.
            //        // We recommend 5 minutes or less:
            //        ClockSkew = TimeSpan.FromMinutes(5),
            //        // Specify the key used to sign the token:
            //        IssuerSigningKey = sharedKey,
            //        RequireSignedTokens = true,
            //        // Ensure the token hasn't expired:
            //        RequireExpirationTime = true,
            //        ValidateLifetime = true,
            //        // Ensure the token audience matches our audience value (default true):
            //        ValidateAudience = true,
            //        ValidAudiences = new List<string>
            //        {
            //            Configuration["Authentication:AppIdUri"],
            //            Configuration["Authenication:ClientId"]
            //        },
            //        // Ensure the token was issued by a trusted authorization server (default true):
            //        ValidateIssuer = true,
            //        ValidIssuer = Configuration["Authentication:Authority"]
            //    };
            //    options.RequireHttpsMetadata = false;
            //});

            //services.AddAuthorization(options =>
            //{
            //    options.DefaultPolicy =
            //         new AuthorizationPolicyBuilder(JwtBearerDefaults.AuthenticationScheme)
            //        .RequireAuthenticatedUser()
            //        .Build();
            //});

            services.AddMvc(options =>
            {
                options.Filters.Add<JsonExceptionFilter>();
               // options.Filters.Add<RequireHttpsOrClosedAttribute>();
                options.Filters.Add<LinkRewritingFilter>();
               // options.Filters.Add(new AuthorizeFilter("default"));
                
            })
           .SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddRouting(options => options.LowercaseUrls = true);
                        
            services.AddApiVersioning(options =>
            {
                options.AssumeDefaultVersionWhenUnspecified = true;
                options.DefaultApiVersion = new ApiVersion(3, 0);
                options.ApiVersionReader = new MediaTypeApiVersionReader();
                options.ApiVersionSelector = new CurrentImplementationApiVersionSelector(options);
                options.ReportApiVersions = true;

            });

            //CORS to allow cross origin resource sharing -
            //may need to enable for woocommerce or wcs if on different domain
            services.AddCors(options =>
            {
                options.AddPolicy("AllowMyApp",
                  // policy => policy.WithOrigins("htts://woocommerce.url - wpengine"))
                  policy => policy.AllowAnyOrigin());
            });

            ////Set the connectionstring configuration
            //services.Configure<MySettingsModel>(Configuration.GetSection("MySettings"));

            //Automapping entity to resource
            services.AddAutoMapper(
                options => options.AddProfile<MapperProfile>());

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwaggerUi3WithApiExplorer(options =>
                {
                    options.GeneratorSettings.DefaultPropertyNameHandling
                       = NJsonSchema.PropertyNameHandling.CamelCase;
                });

            }
            else
            {
                app.UseHsts();
            }

            //app.UseHttpsRedirection();
            //app.UseAuthentication();
            app.UseMvc();
        }
    }
}
