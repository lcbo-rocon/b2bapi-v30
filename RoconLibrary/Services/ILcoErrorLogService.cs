using RoconLibrary.Models;
using RoconLibrary.Domains;

namespace RoconLibrary.Services
{
    public interface ILcoErrorLogService
    {
        void insertToTable(LcoErrorLog lcoErrorLog);
    }
}