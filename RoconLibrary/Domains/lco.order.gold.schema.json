{
	"$schema": "http://json-schema.org/draft-07/schema#",
	"$id": "https://hcmdevblobsa.blob.core.windows.net/order/v1/order.lco.json",
	"title": "LCO order",
	"description": "LCO order structure describing Order Header and Order Items",
	"definitions": {
		"tenderCode" : {
            "description": "Tender type: Example values:Cash, Personal Cheque, Certified Cheque,LCBO Cheque,CDN Trav Cheque,US Cash,US Trav Cheque,Debit Card,Mastercard, Visa, American Express,EGC Redeem, Gift Certificate (redeem), Head Office Invoice,Head Office Cheque,Trade Voucher,EGC Issue (not implemented)",
            "type": "string"
		},
		"tenderDetail": {
			"type": "object",
			"properties": {
				"tenderIssuingSystem":  {
					"description": "System issuing the tender",
					"$ref": "#/definitions/orderSystemDetail"
				},
                "tenderId": {
                    "description": "Unique tender identifier",
                    "type": "string"
                },
                "tenderNotes": {
                    "description": "Textual notes on the tender"
                },
                "tenderAmount": {
                    "description": "Positive or negative amount on the tender",
                    "type": "number"
                },
                "tenderClass" :{
                    "$ref": "#/definitions/tenderCode"
				},
				"tenderType": {
					"description": "Type of tender",
					"type": "string",
					"enum": ["CREDIT", "DEBIT"]
				},
				"tenderDate" :{
					"description": "Date and time when tender was issued format YYYY-MM-DDTHH24:MI:SS:mmm-ZZZZ. Example: 2019-11-19T15:17:24-0500",
					"type":"string"
				},
				"referenceDocumentType" :{
					"type": "string",
					"enum": ["INVOICE","CHEQUE","ORDER","TENDER","OTHER","N/A"]
				},
				"referenceDocumentSubType": {
					"type": "string",
					"enum": ["PRODUCT","DELIVERY","OTHER","N/A"]
				},
				"referenceDocumentId": {
					"type": "string",
					"description": "Identifier of the refrence document. Example: Product Invoice Id, Delivery Invoice Id"
				}

			}
		},
		"paymentProcessingChannelCode" :{
			"type": "string",
			"enum": ["VERSAPAY","INTERNAL-LCBO","OTHER","N/A"],
			"default": "VERSAPAY"
		},
		"orderStatusCodeDetail" : {
			"type": "object",
			"properties": {
				"orderStatusCode" : {
					"description": "LCO Order Status Identifier codes, Example: W=Wait, O=Open, R=Ready for Pick, P=Pick completed, I=Invoiced, C=Closed, D=Deleted/Cancelled, Z=Zero Pick, E=Error",
					"type":"string",
					"enum":["W","O","R","P","I","C","D","Z","E"],
					"minLength": 1,
					"maxLength": 1,
					"default": "W"
				},
				"orderStatusName" :{
					"description" : "Order Status Code name. Example: Wait, Open, Ready for Pick, etc",
					"type": "string",
					"enum": ["W:WAIT","O:OPEN","R:READY FOR PICK", "P:PICK COMPLETED", "I:INVOICED", "C:CLOSED", "D:DELETED_CANCELLED", "Z:ZERO_PICK", "E:ERROR"]
				},
				"statusCreateTime": {
					"description": "Order status creation time in format yyyy-mm-ddT24:MM:SS-0500",
					"type": "string",
					"minLength": 16,
					"maxLength": 25
				}
			},
			"required": ["orderStatusCode", "orderStatusName","statusCreateTime"]
		},
		"orderStatusDetail": {
			"type":"object",
			"properties": {
				"statusPathName": {
					"type": "string"
				},
				"statusSequenceNo": {
					"type":"integer"
				},
				"statusIdentifier":{
					"type":"string"
				},
				"statusCreateTime": {
					"description": "Order status creation time in format yyyy-mm-ddT24:MM:SS-0500",
					"type": "string",
					"minLength": 16,
					"maxLength": 25
				}
			}
		},
		"systemCode": {
			"description": "Code of the systems participating in LCO order management. Example: W=webstore, O=woocommerce, C=eCommerce WCS, M=OMS, R=ROCON",
			"type": "string",
			"enum": ["W","O","C","M","R"],
			"default": "C"
		},
		"referenceDetail": {
            "type":"object",
            "properties": {
                "referenceSystem": {
                    "description": "Code of the system providing reference order details",
                    "$ref": "#/definitions/orderSystemDetail"
                },
                "referenceIdentifier": {
                    "description": "Reference identifier value",
                    "type": "string"
                },
                "referenceIdentifierType": {
                    "description": "Reference identifier type",
                    "type":"string",
                    "enum": ["ORDER_NUMBER","REFERENCE_NUMBER","INVOICE_NUMBER", "OTHER"],
                    "default": "ORDER_NUMBER"
                },
                "referenceIdentifierSubtype":{
                    "description": "Detailed purpose designation of the reference value",
                    "type":"string",
                    "enum": ["LCO_ORDER","DELIVERY_CHARGE", "ACCOUNT_CREDIT","OTHER"]
                },
                "referenceComment":{
                    "description": "Additional comment text related to the reference identifier value",
                    "type":"string"
                }
			},
			"required": ["referenceSystem", "referenceIdentifier", "referenceIdentifierType", "referenceIdentifierSubtype"]
		},
		"orderSystemDetail" : {
			"type":"object",
			"properties": {
				"systemIdentifier": {
					"description": "Code of the systems participating in LCO order management. Example: W=webstore, O=woocommerce, C=eCommerce WCS, M=OMS, R=ROCON",
					"$ref": "#/definitions/systemCode"
				},
				"systemName":{
					"description": "Order management system name",
					"type": "string",
					"enum": ["W:WEBSTORE", "O:WOO_COMMERCE", "C:WCS", "M:OMS", "R:ROCON"],
					"default": "C:WCS"
				},
				"systemOrderStatusDetail" :{
					"description": "Allowed order status lifecycles for a particular source system ",
					"type": "array",
					"items": {
						"$ref": "#/definitions/orderStatusDetail"
					}
				}
			},
			"required": ["systemIdentifier", "systemName"]
		},
		"settlementStatusDetail": {
			"type": "object",
			"properties": {
				"settlementStatus" :{
					"description": "Actual settlement/clearing status of an invoice submoitted to the payment processing channel ",
					"type":"string",
					"enum": ["CLEARED","NOT CLEARED"]
				},
				"settlementTime": {
					"description": "Date and time of the settlement status change in format YYYY-MM-DDTH24:MI:SS.mmmm-ZZZZ",
					"type": "string"
				},
				"settlementNotificationText" : {
					"description": "otificstion text received from payment processing channel",
					"type": "string"
				}
			}
		},
		"paymentDetail": {
			"description": "Invoice payment details",
			"paymentTransactions" :{
				"description" : "Transactions recorded towards payment of the Customer invoice. This is used for internal LCBO accounting",
				"type": "array",
				"items": {
					"initiatingSystem" :{
						"description": "System which initiated payment to paymentChannel",
						"$ref": "#/definitions/orderSystemDetail"
					},
					"paymentTenderType": {
						"description": "Payment tender used to pay the order invoice",
						"type": "string",
						"enum": ["VERSAPAY","CREDIT_MEMO", "CASH", "CREDIT_CARD","GIFT_CARD","CHEQUE"]
					},
					"paymentProcessingChannel" :{
						"description": "Payment Channel name. Example: VERSAPAY",
						"$ref": "#/definitions/paymentProcessingChannelCode"
					},
					"paymentDate" :{
						"description": "Date and time when payment was made in format YYYY-MM-DDTHH24:MI:SS:mmm-ZZZZ. Example: 2019-11-19T15:17:24-0500",
						"type": "string"
					},
					"paymentTransactionId" :{
						"type": "string"
					},
					"paymentConfirmationId": {
						"type": "string"
					},
					"paymentAmount" :{
						"type": "number"
					},
					"paymentReference" :{
						"referenceNumber" : {
							"type": "string"
						},
						"referenceText": {
							"type": "string"
						} 
					},
					"paymentText": {
						"type": "string"
					}
				}
			},
			"paymentInvoiceReference" :{
				"description": "Structure of the invoice presented to Customer",
				"type": "object",
				"properties" :{
					"invoicingSystem" :{
						"description": "System preparing the invoice",
						"$ref": "#/definitions/orderSystemDetail"
					},
					"invoiceSubmissionDate": {
						"description": "Date and time when invoice is submitted for payment to invoicePaymentChannel. Format YYYY-MM-DDTH24:MI:SS.mmmm-ZZZZ",
						"type": "string"
					},
					"invoicePaymentDate": {
						"description": "Date and time when invoice is payed on invoicePaymentChannel. Format YYYY-MM-DDTH24:MI:SS.mmmm-ZZZZ",
						"type": "string"
					},					
					"invoicePaymentChannel": {
						"description": "System used to process full Invoice payment",
						"$ref": "#/definitions/paymentProcessingChannelCode"
					},
					"invoicePaymentReponseMsg": {
						"description": "Response object/message from invoicePaymentChannel after invoice payment is processed",
						"type": "object"
					},
					"internalInvoiceView" :{
						"description" : "Internal LCBO invoice structure shown to the Customer on invoicingSystem",
						"type": "object"
					},
					"externalInvoiceView": {
						"description": "Reference invoice structure as submitted to invoicePaymentChannel",
						"type": "object"
					},
					"settlementStatus" :{
						"description": "Settlement/clearing status of the invoice",
						"type": "array",
						"items" :{
							"$ref": "#/definitions/settlementStatusDetail"
						}
					}
				}
			}
		},
		"invoiceDetail": {
			"description": "Product Invoice information",
			"type": "object",
			"properties": {
				"invoiceSystemInfo": {
					"description": "Product Invoice producing system info",
					"$ref": "#/definitions/orderSystemDetail"
				},
				"invoiceType": {
					"description": "Invoice can be estimated or Actual. Value Examples: A - Actual, E - Estimate",
					"type": "string",
					"enum":["A","E","OTHER"]
				},
				"invoiceNumber": {
					"description": "Invoice identifier",
					"type": "string"
				},
				"invoiceTotalAmount": {
					"type" : "number",
					"minimum": 1,
					"maximum": 9999999.99
				},
				"invoiceDiscountAmount": {
					"type":"number"
				},
				"invoiceBottleDepositAmount": {
					"type": "number"
				},
				"invoiceHstAmount" : {
					"type": "number"
				},
				"invoiceLicMUAmount": {
					"type": "number"
				},
				"invoiceLevyAmount": {
					"type": "number"
				},
				"invoiceRetailAmount": {
					"type": "number"
				},
				"createdDate":{
					"decription": "Date and time of invoice creation",
					"type": "string"
				},
				"updatedDate": {
					"decription": "Date and time of the last invoice data update",
					"type": "string"
				},
				"createdBy": {
					"decription": "Identification of a Person who created the invoice",
					"type": "string"
				},
				"updatedBy": {
					"decription": "Identification of the Person who last updated the invoice",
					"type": "string"
				}						
			},
			"required": [
				"invoiceType",
				"invoiceNumber",
				"invoiceTotalAmount",
				"invoiceDiscountAmount",
				"invoiceBottleDepositAmount",
				"invoiceHstAmount"
			]	
		},
		"deliveryChargeDetail" :{
			"type": "object",
			"properties": {
				"deliveryChargeSourceInfo": {
					"description": "System which produced delivery charge details",
					"$ref": "#/definitions/orderSystemDetail"
				},
				"deliveryChargeSourceId": {
                    "description": "Charge identifier as produced on the delivery charge Source system",
                    "type" : "string"
				},
				"deliveryChargeReferenceInfo" :{
					"description": "Additional reference information related to the delivery charge. Example: ROCON Delivery Charge ID, OMS Delivery Charge ID",
					"$ref": "#/definitions/referenceDetail"
				},
				"deliveryChargeType" : {
					"description" :"Delivery charge type: A-Actual, E-Estimate, N/A- Not applicable",
					"type": "string",
					"enum": ["E","A","OTHER","N/A"],
					"default": "E"
				},
				"deliveryComment": {
					"description": "Comments for this delivery charge",
					"type": "string"
				},
				"deliveryCreditAmount":{
					"description": "Total Credit applied to delivery charge",
					"type": "number"
				},
				"deliveryBaseAmount": {
					"description": "Delivery charge amount without any taxes included",
					"type": "number"
				},
				"deliveryHstAmount": {
					"description": "HST Tax amount",
					"type": "number"
				},
				"postingDetails": {
					"type": "array",
					"items" : {
						"postingDate" :{
							"descrption": "Date and time when this delivery charge is posted to accounting system",
							"type": "string"
						},
						"postedToSystem" :{
							"description": "Short code of the system where this delivery charge is posted for accounting purposes. Example: SCSGL–GL for GTA Service Centre(941); SA–Sales Audit",
							"type": "string",
							"enum":["SCSGL","SA"]
						}
					}
				},
				"createdDate":{
					"decription": "Date and time of delivery charge creation",
					"type": "string"
				},
				"updatedDate": {
					"decription": "Date and time of last delivery charge update",
					"type": "string"
				},
				"createdBy": {
					"decription": "Identification of the Person who created delivery charge",
					"type": "string"
				},
				"updatedBy": {
					"decription": "Identification of the last Person who updated delivery charge",
					"type": "string"
				}
			}
		},
		"orderHeaderDetail": {
			"type": "object",
			"properties": {
				"orderSourceInfo":{
					"description": "Identify the source system of the order capture - W for webstore, O for woocommerce, C for eCommerce WCS",
					"$ref": "#/definitions/orderSystemDetail"
				},
				"orderEvent":{
					"description": "New Order, Cancel Order, Inventory Hold, Return Order, Order status change, Other events",
					"type": "string",
					"enum": ["NEW","RETURN","CANCEL","INV_HOLD","STATUS_UPDATE","DELIVERY_CHARGE","OTHER"],
					"default": "NEW"
				},
				"orderStatus": {
                    "description" : "LCO Order Status",
					"$ref": "#/definitions/orderStatusCodeDetail"
				},
				"publishedOrderStatus": {
                    "description": "List of published order statuses. Used in combination with statusLifeCyclePath to manage proper order status sequencing and to prevent misplaced order status propagation",
                    "type":"array",
                    "items" : {
                        "$ref": "#/definitions/orderStatusDetail"
					}
				},
				"orderType": {
					"description" : "LCO Order type. Example: AGY ",
					"type": "string",
					"enum": ["LCO", "AGY"],
					"default": "AGY"
				},
				"customerNumber" : {
					"description" : "LCO Customer (Agency) identifier",
					"type": "string"
				},
				"customerType": {
					"description": "B2B Customer Type - Agency/LCO, Licensee, Grocery..etc",
					"type":"string",
					"minLength": 1,
					"maxLength": 20,
					"default": "AGENCY"
				},
				"customerAccountInfo": {
					"accountNumber": {
						"description": "Customer's financial account identifier",
						"type":"string"
					},
					"accountBalanceItems" :{
						"type": "array",
						"items": {
							"accountBalanceType" :{
								"type": "string",
								"enum": ["ACCOUNT_CREDIT","INVOICE_CREDIT","N/A","OTHER"]
							},
							"accountBalanceTypeTotalAmount": {
								"description": "total amount for the specific balance type (total delivery charge credits, total product invoice credits,etc",
								"type": "number"
							}
						}
					},
					"accountTotalBalanceAmount":{
						"description": "Financial Account total balance of the Customer submitting the order",
						"type": "number"
					}
				},
				"lcoOrderNumber": {
					"description": "Order number generated by Order Generation System",
					"type": "integer",
					"minimum": 1,
					"maximum": 99999999
				},
				"lcoOrderSystem":{
					"description": "System which generated lcoOrderNumber (Order Capture System)",
					"$ref": "#/definitions/orderSystemDetail"
				},
				"sorOrderNumber" :{
					"description": "Order number generated by Order System Of Record, to be populated by ROCON in response",
					"type": "integer",
					"minimum": 1,
					"maximum": 99999999
				},
				"sorSystem": {
					"description": "System designated as System Of Record for LCO orders",
					"#ref": "#/definitions/orderSystemDetail"
				},
				"referenceInfo":{
                    "description": "Additional reference identifiers (example: invoice number, etc) generated by other participating systems in order processing stream.",
                    "type":"array",
                    "items": {
                        "$ref": "#/definitions/referenceDetail"
					}
				},
				"orderRequestDate": {
					"description": "Order request date in format yyyy-mm-ddT24:MM:SS-0500, to be populated by calling app in request",
					"type": "string",
					"minLength": 16,
					"maxLength": 25
				},
				"orderCreationDate": {
					"description": "Order creation date in format yyyy-mm-ddT24:MM:SS-0500, to be populated by ROCON in response",
					"type": "string",
					"minLength": 16,
					"maxLength": 25
				},
				"orderRequiredDate": {
					"description": "Date when order will be shipped in format yyyy-mm-dd, to be populated by calling app in request",
						"type": "string",
						"minLength": 8,
						"maxLength": 10,
						"pattern": "^[2-9]{1}[0-9]{3}-[0-1]{1}[0-9]{1}-[0-3]{1}[0-9]{1}$"
				},
				"orderShipmentDate": {
					"description": "Date when order will be shipped in format yyyy-mm-dd, to be populated by ROCON in response",
					"type": "string",
					"minLength": 8,
					"maxLength": 10,
					"pattern": "^[2-9]{1}[0-9]{3}-[0-1]{1}[0-9]{1}-[0-3]{1}[0-9]{1}$"
				},
				"routeInfo": {
					"type": "object",
					"properties": {
						"routeCode": {
							"type": "string",
							"maxLength": 10
						},
						"routeStops" : {
							"type": "integer",
							"minimum": 0,
							"maximum": 9999999999,
							"default": 0
						}
					}
				},
				"shipToInfo": {
					"type": "object",
					"properties": {
						"overwriteShipToInformation" :{
							"description": "false indicates shipTo address on file; true indicates overwriting a new address for this order",
							"type": "boolean"
						},
						"name": {
							"type": "string",
							"minLength": 1,
							"maxLength": 36
						},
						"addressLine1": {
							"type": "string",
							"minLength": 1,
							"maxLength": 36
						},
						"addressLine2": {
							"type": "string",
							"maxLength": 36
						},
						"city": {
							"type": "string",
							"minLength": 1,
							"maxLength": 30
						},
						"provinceCode": {
							"type": "string",
							"minLength": 1,
							"maxLength": 2
						},
						"country": {
							"type": "string",
							"minLength": 1,
							"maxLength": 30
						},
						"postalCode": {
							"type": "string",
							"minLength": 6,
							"maxLength": 7
						},
						"phoneNumber": {
							"type": "string",
							"maxLength": 12 
						},
						"faxNumber": {
							"type": "string",
							"maxLength": 12 
						},
						"emailAddress": {
							"type": "string",
							"maxLength": 40
						},
						"contact": {
							"type": "string",
							"maxLength": 40
						}

					}
				},
				"deliveryType": {
					"description": "Delivery types accepted by ROCON for Agency/LCO",
					"type": "string",
					"enum": ["SHIP_TO_ADDR_ON_FILE", "PICK_UP"],
					"default": "SHIP_TO_ADDR_ON_FILE"
				},
				"orderCommentsInfo": {
					"type": "object",
					"properties": {
						"packingInvoiceComment": {
							"type": "object",
								"properties": {
								"textLine1" : {
									"type": "string",
									"maxLength": 80
								},
								"textLine2" : {
									"type": "string",
									"maxLength": 80
								},
								"textLine3" : {
									"type": "string",
									"maxLength": 80
								}
							}
						},
						"bolComment": {
							"type": "string",
							"maxLength": 25
						},
						"pickerComment": {
							"type": "string",
							"maxLength": 25
						},
						"shipLabelComment": {
							"type": "string",
							"maxLength": 25
						}
					}
				},
				"invoiceInfo": {
					"description": "Product Invoice information",
					"$ref": "#/definitions/invoiceDetail"
				},
				"invoiceInfoFinal": {
					"description": "Final (Actual) Product Invoice information",
					"$ref": "#/definitions/invoiceDetail"
				},
				"deliveryCharges": {
					"description": "Delivery charges for a Customer Order. There could be multiple delivery charges for a single Customer Order",
					"type": "array",
					"uniqueItems": true,
					"items": {
						"$ref": "#/definitions/deliveryChargeDetail"
					}
				},
				"tenderInfo" : {
					"type": "array",
					"items": {
						"$ref": "#/definitions/tenderDetail"
					}
				},
				"paymentEventInfo" :{
					"description": "Invoice payment details submitted to payment channel",
					"$ref": "#/definitions/paymentDetail"
				},
				"trafficPlanningIndicator" : {
					"description": "to be populated by ROCON in response",
					"type": "string",
					"enum": ["Y","N"],
					"default": "Y"
				},
				"uncommitedItemQuantityIndicator": {
					"description": "to be populated by ROCON in response",
					"type": "string",
					"enum": ["Y","N"],
					"default": "N"
				},				
				"numberOfLineItems": {
					"description": "Total number of order line items",
					"type" : "integer",
					"minimum": 1
				},
				"numberOfFullCases" :{
					"description": "Number of full cases shipped with original items (SKUs) inside",
					"type": "integer",
					"minimum": 0
				},
				"numberOfPartialCases" :{
					"description": "Number of the full cases shipped with mixed items (SKUs) inside",
					"type": "integer",
					"minimum": 0
				},
				"airmilesNo": {
					"type": "string",
					"maxLength": 12
				},
        "orderPriority": {
          "type": "string",
          "maxLength": 6
        },
				"hostOrderNumber": {
					"type": "string",
					"maxLength": 22 
				},
				"referenceNumber" : {
					"type": "string",
					"maxLength": 16
				},
				"carrierId" :{
					"type": "string",
					"maxLength": 15
				},
				"serviceLevel": {
					"type": "string",
					"maxLength": 3
				},
				"paymentMethod": {
					"description": "Payment methods accepted by ROCON for Agency/LCO",
					"type": "string",
					"enum": ["CHECK", "CASH_ON_DELIVERY","N/A","OTHER"],
					"default": "CASH_ON_DELIVERY"
				}		
			},
			"required": [
				"orderType",
				"customerNumber"
			]	
		},
		"orderItemDetail" : {
			"type": "object",
			"properties": {
				"itemLineNumber": {
					"type": "integer",
					"minimum": 1,
					"maximum": 9999999999
				},
				"itemLineStatus": {
					"description": "Status of the order line item",
					"type": "string",
					"enum": ["NEW","UPDATED","DELETED","OTHER","N/A"],
					"default": "NEW"
				},
				"itemSku": {
					"type": "integer"
				},
				"itemQuantity": {
					"description": "Number of SKU items included in the order item",
					"type": "integer",
					"minimum": 1,
					"maximum": 9999999999
				},
				"shippedQuantity": {
					"description": "Number of SKU items picked for shipping.",
					"type": "integer",
					"minimum": 0,
					"maximum": 9999999999
				},
				"priceInfo": {
					"type": "object",
					"properties": {
						"itemSellingPrice": {
							"description": "SKU item selling price",
							"type": "number",
							"minimum": 0,
							"maximum": 9999999.99
						},
						"itemBasicPrice": {
							"description": "SKU item basic/unit price",
							"type": "number",
							"minimum": 0,
							"maximum": 9999999.99
						},
						"itemBottleDeposit": {
							"description": "Amount charged for bottle return",
							"type": "number",
							"minimum": 0,
							"maximum": 9999999.99
						},						
						"itemDiscountAmount" :{
							"type": "number"
						},
						"itemLicMarkupAmount": {
							"description": "SKU item Lic Markup price",
							"type": "number",
							"minimum": 0,
							"maximum": 9999999.99
						},
						"itemHSTAmount": {
							"description": "SKU item HST tax",
							"type": "number",
							"minimum": 0,
							"maximum": 9999999.99
						},
						"itemRetailPrice": {
							"description": "SKU item retail/end user price",
							"type": "number",
							"minimum": 0,
							"maximum": 9999999.99
						},
						"lastUpdateDate": {
							"description": "SKU item prices last updated date",
							"type": "string"   
						}
					}
				},
				"errorInfo": {
					"description": "Error processing information to be populated by ROCON. For internal usage only",
					"type": "array",
					"items": {
						"$ref": "#/definitions/errorDetail"
					}
				}				
			},
			"required": ["itemLineNumber", "itemSku", "itemQuantity", "priceInfo"]
		},
		"errorDetail": {
			"type": "object",
			"errorType" : {
				"description": "Error type as specified in the enumeration. For internal use only",
				"type": "string",
				"enum": ["INVALID_ORDER","PRICE_ERROR","INVENTORY_ERROR","SKU_ERROR","MISSING_ELEMENT","WRONG_FORMAT","UNRECOGNIZED_ELEMENT","FILE_CREATE_ERROR","FILE_DROP_ERROR", "OUT_OF_SEQ","INVALID_STATUS","OTHER"]
			},
			"errorMessage": {
				"description": "Detailed description of the error. For internal use only",
				"type": "string"
			},
			"errorData": {
				"type": "object",
				"key": {
					"type": "string"
					},
				"value": {
					"type": "string"
					}
			},
		    "required":["errorType", "errorMessage"]
		}
	},
	"type" : "object",
	"properties": {
		"orderHeader": {
			"type": "object",
			"$ref": "#/definitions/orderHeaderDetail"
		},
		"orderItems": {
			"type": "array",
			"minItems": 1,
			"items" : {
				"$ref": "#/definitions/orderItemDetail"
			}
		},
		"errorInfo": {
			"description": "Error processing information. For internal usage only",
			"type": "array",
			"items": {
				"$ref": "#/definitions/errorDetail"
			}
		},
		"errorFileName": {
			"description": "Generated error file name. Populated in case of an error only. For internal usage only",
			"type": "string"
		},
		"errorFileContent": {
			"description": "generated error file content. Populated in case of an error only. For internal usage only ",
			"type": "string"
		}
	},
	"required": [
		"orderHeader", 
		"orderItems"
	],
	"additionalProperties": true
}