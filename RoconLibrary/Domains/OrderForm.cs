﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using RoconLibrary.Models;

namespace RoconLibrary.Domains
{
    public class OrderForm: Resource
    {
        [Required]
        public OrderHeader orderHeader { get; set; }

        [Required]
        public OrderItemDetail[] orderItems { get; set; }

        //[JsonProperty("errorDetails", NullValueHandling = NullValueHandling.Ignore)]
        //public ErrorDetail[] errorInfo { get; set; }
    }


}
