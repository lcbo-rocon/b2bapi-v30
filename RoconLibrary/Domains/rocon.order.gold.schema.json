{
  "$schema": "http://json-schema.org/draft-07/schema#",
  "$id": "https://hcmdevblobsa.blob.core.windows.net/order/v1/order.lco.json",
  "title": "LCO order",
  "description": "LCO order structure describing Order Header and Order Items",
  "definitions": {
    "orderStatusCodeDetail": {
      "type": "object",
      "properties": {
        "orderStatusCode": {
          "description": "LCO Order Status Identifier codes, Example: W=Wait, O=Open, R=Ready for Pick, P=Pick completed, I=Invoiced, C=Closed, D=Deleted/Cancelled, Z=Zero Pick, E=Error",
          "type": "string",
          "enum": [ "W", "O", "R", "P", "I", "C", "D", "Z", "E" ],
          "minLength": 1,
          "maxLength": 1,
          "default": "W"
        },
        "orderStatusName": {
          "description": "Order Status Code name. Example: Wait, Open, Ready for Pick, etc",
          "type": "string",
          "enum": [ "W:WAIT", "O:OPEN", "R:READY FOR PICK", "P:PICK COMPLETED", "I:INVOICED", "C:CLOSED", "D:DELETED_CANCELLED", "Z:ZERO_PICK", "E:ERROR" ]
        },
        "statusCreateTime": {
          "description": "Order status creation time in format yyyy-mm-ddT24:MM:SS-0500",
          "type": "string",
          "minLength": 16,
          "maxLength": 25
        }
      },
      "required": [ "orderStatusCode", "orderStatusName", "statusCreateTime" ]
    },
    "orderStatusDetail": {
      "type": "object",
      "properties": {
        "statusPathName": {
          "type": "string"
        },
        "statusSequenceNo": {
          "type": "integer"
        },
        "statusIdentifier": {
          "type": "string"
        },
        "statusCreateTime": {
          "description": "Order status creation time in format yyyy-mm-ddT24:MM:SS-0500",
          "type": "string",
          "minLength": 16,
          "maxLength": 25
        }
      }
    },
    "systemCode": {
      "description": "Code of the systems participating in LCO order management. Example: W=webstore, O=woocommerce, C=eCommerce WCS, M=OMS, R=ROCON",
      "type": "string",
      "enum": [ "W", "O", "C", "M", "R" ],
      "default": "C"
    },
    "orderSystemDetail": {
      "type": "object",
      "properties": {
        "systemIdentifier": {
          "description": "Code of the systems participating in LCO order management. Example: W=webstore, O=woocommerce, C=eCommerce WCS, M=OMS, R=ROCON",
          "$ref": "#/definitions/systemCode"
        },
        "systemName": {
          "description": "Order management system name",
          "type": "string",
          "enum": [ "W:WEBSTORE", "O:WOO_COMMERCE", "C:WCS", "M:OMS", "R:ROCON" ],
          "default": "C:WCS"
        },
        "systemOrderStatusDetail": {
          "description": "Allowed order status lifecycles for a particular source system ",
          "type": "array",
          "items": {
            "$ref": "#/definitions/orderStatusDetail"
          }
        }
      },
      "required": [ "systemIdentifier", "systemName" ]
    },
    "referenceDetail": {
      "type": "object",
      "properties": {
        "referenceSystem": {
          "description": "Code of the system providing reference order details",
          "$ref": "#/definitions/orderSystemDetail"
        },
        "referenceIdentifier": {
          "description": "Reference identifier value",
          "type": "string"
        },
        "referenceIdentifierType": {
          "description": "Reference identifier type",
          "type": "string",
          "enum": [ "ORDER_NUMBER", "REFERENCE_NUMBER", "OTHER" ],
          "default": "ORDER_NUMBER"
        },
        "referenceIdentifierSubtype": {
          "description": "Detailed purpose designation of the reference value",
          "type": "string",
          "enum": [ "LCO_ORDER", "DELIVERY_CHARGE", "INVOICE_NUMBER", "OTHER" ]
        },
        "referenceComment": {
          "description": "Additional comment text related to the reference identifier value",
          "type": "string"
        }
      },
      "required": [ "referenceSystem", "referenceIdentifier", "referenceIdentifierType", "referenceIdentifierSubtype" ]
    },
    "orderHeaderDetail": {
      "type": "object",
      "properties": {
        "orderSourceInfo": {
          "description": "Identify the source system of the order capture - W for webstore, O for woocommerce, C for eCommerce WCS",
          "$ref": "#/definitions/orderSystemDetail"
        },
        "orderEvent": {
          "description": "New Order, Cancel Order, Inventory Hold, Return Order, Order status change, Other events",
          "type": "string",
          "enum": [ "NEW", "RETURN", "CANCEL", "INV_HOLD", "STATUS_UPDATE", "OTHER" ],
          "default": "NEW"
        },
        "orderStatus": {
          "description": "LCO Order Status",
          "$ref": "#/definitions/orderStatusCodeDetail"
        },
        "publishedOrderStatus": {
          "description": "List of published order statuses. Used in combination with statusLifeCyclePath to manage proper order status sequencing and to prevent misplaced order status propagation",
          "type": "array",
          "items": {
            "$ref": "#/definitions/orderStatusDetail"
          }
        },
        "orderType": {
          "description": "LCO Order type. Example: AGY ",
          "type": "string",
          "enum": [ "LCO", "AGY" ],
          "default": "AGY"
        },
        "customerNumber": {
          "description": "LCO Customer (Agency) identifier",
          "type": "string"
        },
        "customerType": {
          "description": "B2B Customer Type - Agency/LCO, Licensee, Grocery..etc",
          "type": "string",
          "minLength": 1,
          "maxLength": 20,
          "default": "AGENCY"
        },
        "lcoOrderNumber": {
          "description": "Order number generated by Order Generation System",
          "type": "integer",
          "minimum": 1,
          "maximum": 99999999
        },
        "lcoOrderSystem": {
          "description": "System which generated lcoOrderNumber (Order Capture System)",
          "$ref": "#/definitions/orderSystemDetail"
        },
        "sorOrderNumber": {
          "description": "Order number generated by Order System Of Record, to be populated by ROCON in response",
          "type": "integer",
          "minimum": 1,
          "maximum": 99999999
        },
        "sorSystem": {
          "description": "System designated as System Of Record for LCO orders",
          "#ref": "#/definitions/orderSystemDetail"
        },
        "referenceInfo": {
          "description": "Additional reference identifiers (example: delivery charge number, invoice number etc) generated by other participating systems in order processing stream.",
          "type": "array",
          "items": {
            "$ref": "#/definitions/referenceDetail"
          }
        },
        "orderRequestDate": {
          "description": "Order request date in format yyyy-mm-ddT24:MM:SS-0500, to be populated by calling app in request",
          "type": "string",
          "minLength": 16,
          "maxLength": 25
        },
        "orderCreationDate": {
          "description": "Order creation date in format yyyy-mm-ddT24:MM:SS-0500, to be populated by ROCON in response",
          "type": "string",
          "minLength": 16,
          "maxLength": 25
        },
        "orderRequiredDate": {
          "description": "Date when order will be shipped in format yyyy-mm-dd, to be populated by calling app in request",
          "type": "string",
          "minLength": 8,
          "maxLength": 10,
          "pattern": "^[2-9]{1}[0-9]{3}-[0-1]{1}[0-9]{1}-[0-3]{1}[0-9]{1}$"
        },
        "orderShipmentDate": {
          "description": "Date when order will be shipped in format yyyy-mm-dd, to be populated by ROCON in response",
          "type": "string",
          "minLength": 8,
          "maxLength": 10,
          "pattern": "^[2-9]{1}[0-9]{3}-[0-1]{1}[0-9]{1}-[0-3]{1}[0-9]{1}$"
        },
        "routeInfo": {
          "type": "object",
          "properties": {
            "routeCode": {
              "type": "string",
              "maxLength": 10
            },
            "routeStops": {
              "type": "integer",
              "minimum": 0,
              "maximum": 9999999999,
              "default": 0
            }
          }
        },
        "shipToInfo": {
          "type": "object",
          "properties": {
            "overwriteShipToInformation": {
              "description": "false indicates shipTo address on file; true indicates overwriting a new address for this order",
              "type": "boolean"
            },
            "name": {
              "type": "string",
              "minLength": 1,
              "maxLength": 36
            },
            "addressLine1": {
              "type": "string",
              "minLength": 1,
              "maxLength": 36
            },
            "addressLine2": {
              "type": "string",
              "maxLength": 36
            },
            "city": {
              "type": "string",
              "minLength": 1,
              "maxLength": 30
            },
            "provinceCode": {
              "type": "string",
              "minLength": 1,
              "maxLength": 2
            },
            "country": {
              "type": "string",
              "minLength": 1,
              "maxLength": 30
            },
            "postalCode": {
              "type": "string",
              "minLength": 6,
              "maxLength": 7
            },
            "phoneNumber": {
              "type": "string",
              "maxLength": 12
            },
            "faxNumber": {
              "type": "string",
              "maxLength": 12
            },
            "emailAddress": {
              "type": "string",
              "maxLength": 40
            },
            "contact": {
              "type": "string",
              "maxLength": 40
            }

          }
        },
        "deliveryType": {
          "description": "Delivery types accepted by ROCON for Agency/LCO",
          "type": "string",
          "enum": [ "SHIP_TO_ADDR_ON_FILE", "PICK_UP" ],
          "default": "SHIP_TO_ADDR_ON_FILE"
        },
        "orderCommentsInfo": {
          "type": "object",
          "properties": {
            "packingInvoiceComment": {
              "type": "object",
              "properties": {
                "textLine1": {
                  "type": "string",
                  "maxLength": 80
                },
                "textLine2": {
                  "type": "string",
                  "maxLength": 80
                },
                "textLine3": {
                  "type": "string",
                  "maxLength": 80
                }
              }
            },
            "bolComment": {
              "type": "string",
              "maxLength": 25
            },
            "pickerComment": {
              "type": "string",
              "maxLength": 25
            },
            "shipLabelComment": {
              "type": "string",
              "maxLength": 25
            }
          }
        },
        "invoiceInfo": {
          "description": "Invoice information",
          "type": "object",
          "properties": {
            "invoiceType": {
              "description": "Invoice can be estimated or Actual. Value Examples: A - Actual, E - Estimate",
              "type": "string",
              "enum": [ "A", "E", "OTHER" ]
            },
            "invoiceNumber": {
              "type": "string"
            },
            "invoiceTotalAmount": {
              "type": "number",
              "minimum": 0,
              "maximum": 9999999.99
            },
            "invoiceDiscountAmount": {
              "type": "number"
            },
            "invoiceBottleDepositAmount": {
              "type": "number"
            },
            "invoiceHstAmount": {
              "type": "number"
            },
            "invoiceLicMUAmount": {
              "type": "number"
            },
            "invoiceLevyAmount": {
              "type": "number"
            },
            "invoiceRetailAmount": {
              "type": "number"
            },
            "deliveryCharge": {
              "type": "object",
              "properties": {
                "deliveryChargeType": {
                  "type": "string"
                },
                "deliveryComment": {
                  "type": "string"
                },
                "deliveryBaseAmount": {
                  "type": "number"
                },
                "deliveryHstAmount": {
                  "type": "number"
                }
              },
              "required": [
                "deliveryChargeType",
                "deliveryBaseAmount",
                "deliveryHstAmount"
              ]
            }
          },
          "required": [
            "invoiceType",
            "invoiceNumber",
            "invoiceTotalAmount",
            "invoiceDiscountAmount",
            "invoiceBottleDepositAmount",
            "invoiceHstAmount"
          ]
        },
        "trafficPlanningIndicator": {
          "description": "to be populated by ROCON in response",
          "type": "string",
          "enum": [ "Y", "N" ],
          "default": "Y"
        },
        "uncommitedItemQuantityIndicator": {
          "description": "to be populated by ROCON in response",
          "type": "string",
          "enum": [ "Y", "N" ],
          "default": "N"
        },
        "numberOfLineItems": {
          "description": "Total number of order line items",
          "type": "integer",
          "minimum": 1
        },
        "numberOfFullCases": {
          "description": "Number of full cases shipped with original items (SKUs) inside",
          "type": "integer",
          "minimum": 0
        },
        "numberOfPartialCases": {
          "description": "Number of the full cases shipped with mixed items (SKUs) inside",
          "type": "integer",
          "minimum": 0
        },
        "airmilesNo": {
          "type": "string",
          "maxLength": 12
        },
        "orderPriority": {
          "type": "string",
          "maxLength": 6
        },
        "hostOrderNumber": {
          "type": "string",
          "maxLength": 22
        },
        "referenceNumber": {
          "type": "string",
          "maxLength": 16
        },
        "carrierId": {
          "type": "string",
          "maxLength": 15
        },
        "serviceLevel": {
          "type": "string",
          "maxLength": 3
        },
        "paymentMethod": {
          "description": "Payment methods accepted by ROCON for Agency/LCO",
          "type": "string",
          "enum": [ "CHECK", "CASH_ON_DELIVERY", "N/A", "OTHER" ],
          "default": "CASH_ON_DELIVERY"
        }
      },
      "required": [
        "orderType",
        "customerNumber"
      ]
    },
    "orderItemDetail": {
      "type": "object",
      "properties": {
        "itemLineNumber": {
          "type": "integer",
          "minimum": 1,
          "maximum": 9999999999
        },
        "itemLineStatus": {
          "description": "Status of the order line item",
          "type": "string",
          "enum": [ "NEW", "UPDATED", "DELETED", "OTHER", "N/A" ],
          "default": "NEW"
        },
        "itemSku": {
          "type": "integer"
        },
        "itemQuantity": {
          "description": "Number of SKU items included in the order item",
          "type": "integer",
          "minimum": 1,
          "maximum": 9999999999
        },
        "shippedQuantity": {
          "description": "Number of SKU items picked for shipping.",
          "type": "integer",
          "minimum": 0,
          "maximum": 9999999999
        },
        "priceInfo": {
          "type": "object",
          "properties": {
            "itemSellingPrice": {
              "description": "SKU item selling price",
              "type": "number",
              "minimum": 0,
              "maximum": 9999999.99
            },
            "itemBasicPrice": {
              "description": "SKU item basic/unit price",
              "type": "number",
              "minimum": 0,
              "maximum": 9999999.99
            },
            "itemBottleDeposit": {
              "description": "Amount charged for bottle return",
              "type": "number",
              "minimum": 0,
              "maximum": 9999999.99
            },
            "itemDiscountAmount": {
              "type": "number"
            },
            "itemLicMarkupAmount": {
              "description": "SKU item Lic Markup price",
              "type": "number",
              "minimum": 0,
              "maximum": 9999999.99
            },
            "itemHSTAmount": {
              "description": "SKU item HST tax",
              "type": "number",
              "minimum": 0,
              "maximum": 9999999.99
            },
            "itemRetailPrice": {
              "description": "SKU item retail/end user price",
              "type": "number",
              "minimum": 0,
              "maximum": 9999999.99
            },
            "lastUpdateDate": {
              "description": "SKU item prices last updated date",
              "type": "string"
            }
          }
        },
        "errorInfo": {
          "description": "Error processing information to be populated by ROCON. For internal usage only",
          "type": "array",
          "items": {
            "$ref": "#/definitions/errorDetail"
          }
        }
      },
      "required": [ "itemLineNumber", "itemSku", "itemQuantity", "priceInfo" ]
    },
    "errorDetail": {
      "type": "object",
      "errorType": {
        "description": "Error type as specified in the enumeration. For internal use only",
        "type": "string",
        "enum": [ "INVALID_ORDER", "PRICE_ERROR", "INVENTORY_ERROR", "SKU_ERROR", "MISSING_ELEMENT",
          "WRONG_FORMAT",  "UNRECOGNIZED_ELEMENT",
          "FILE_CREATE_ERROR", "FILE_DROP_ERROR",
          "OUT_OF_SEQ", "INVALID_STATUS","OTHER",
          "ACTION_FAILED"
        ]
      },
      "errorMessage": {
        "description": "Detailed description of the error. For internal use only",
        "type": "string"
      },
      "errorData": {
        "type": "object",
        "key": {
          "type": "string"
        },
        "value": {
          "type": "string"
        }
      },
      "required": [ "errorType", "errorMessage" ]
    }
  },
  "type": "object",
  "properties": {
    "orderHeader": {
      "type": "object",
      "$ref": "#/definitions/orderHeaderDetail"
    },
    "orderItems": {
      "type": "array",
      "minItems": 1,
      "items": {
        "$ref": "#/definitions/orderItemDetail"
      }
    },
    "errorInfo": {
      "description": "Error processing information. For internal usage only",
      "type": "array",
      "items": {
        "$ref": "#/definitions/errorDetail"
      }
    },
    "errorFileName": {
      "description": "Generated error file name. Populated in case of an error only. For internal usage only",
      "type": "string"
    },
    "errorFileContent": {
      "description": "generated error file content. Populated in case of an error only. For internal usage only ",
      "type": "string"
    }
  },
  "required": [
    "orderHeader",
    "orderItems"
  ],
  "additionalProperties": true
}