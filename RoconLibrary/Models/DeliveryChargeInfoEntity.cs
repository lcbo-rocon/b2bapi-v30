using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using RoconLibrary.Models;
using RoconLibrary.Domains;

namespace RoconLibrary.Models
{
    [DataContract]
    public class DeliveryChargeInfoEntity
    {
        public string deliverySourceSysId {get; set;}
        public string deliverySourceSystem {get; set;}
        public string deliveryReferenceSysId {get; set;}
        public string deliveryReferenceSystem {get; set;}
        public string deliveryReferenceId {get; set;}
        public string deliveryReferenceIdType {get; set;}
        public string deliveryReferenceIdSubType {get; set;}
        public int deliveryChargeSourceid {get; set; }
        public string deliveryChargeType {get; set;}
        public double deliverBaseAmount {get; set;}
        public double deliveryHstAmount {get; set;}
        public DateTime createdDate {get; set;}
    }
}