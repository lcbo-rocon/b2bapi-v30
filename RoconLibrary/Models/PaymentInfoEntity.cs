using System;
using System.Runtime.Serialization;


namespace RoconLibrary.Models
{
    [DataContract]
    public class PaymentInfoEntity
    {
        public string LV_PAYMENTTENDERTYPE {get; set;}
        public string LV_PAYMENTCHANNEL {get; set;}
        public string LV_SOURCESYSTEMID {get; set;}
        public string LV_SOURCESYSTEMNAME {get; set;}
        public DateTime LV_PAYMENTDATE {get; set;}
        public double LV_PAYMENTAMOUNT {get; set;}
        public string V_PAYREFERENCENUMBER {get; set;}
    }
}