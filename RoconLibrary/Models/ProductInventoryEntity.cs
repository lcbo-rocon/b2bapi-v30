﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RoconLibrary.Models
{
    public partial class ProductInventoryEntity : ProductEntity
    {
        public InventoryInfoEntity inventory;
    }
    public partial class InventoryInfoEntity
    { 
        public int sku { get; set; }
        public int OnHandQuantity { get; set; }
        public int AvailableForSale { get; set; }
        public int OnOrderQuantity { get; set; }
        public int inTransitQuantity { get; set; }
    }
}
