﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using RoconLibrary.Models;
using RoconLibrary.Domains;

namespace RoconLibrary.Models
{
    [DataContract]
    public class OrderEntity 
        {
            public virtual OrderHeaderEntity orderHeader { get; set; }
            public virtual OrderItemsEntity[] orderItems { get; set; }
        }

    public partial class OrderHeaderEntity
    {
        public OrderSourceInfoEntity orderSourceInfo { get; set; }
        public string orderEvent { get; set; }
        public OrderStatusCodeDetailEntity orderStatus { get; set; }
        public OrderStatusDetailEntity[] publishedOrderStatus { get; set; }
        public string orderType { get; set; }
          
        public string customerNumber { get; set; }
        public string customerType { get; set; }

        public int lcoOrderNumber { get; set; }
        public SystemIdentifyEntity lcoOrderSystem { get; set; }

        public int sorOrderNumber { get; set; }
        public SystemIdentifyEntity sorSystem { get; set; }

        public ReferenceInfoEntity[] referenceInfo { get; set; }

        public string orderRequestDate { get; set; }
        public string orderCreationDate { get; set; }
        public string orderRequiredDate { get; set; }
        public string orderShipmentDate { get; set; }
        public string deliveryType { get; set; }

        public string trafficPlanningIndicator { get; set; }

        public string uncommittedItemQtyIndicator { get; set; }

        public string airmMilesNo { get; set; }

        public string orderPriority { get; set; }

        public string hostOrderNumber { get; set; }

        public string referenceNumber { get; set; }

        public string carrierId { get; set; }

        public string serviceLevel { get; set; }

        public RouteInfoEntity routeInfo { get; set; }
        public OrderShipToInfoEntity shipToInfo { get; set; }

        public OrderCommentsInfoEntity orderCommentsInfo { get; set; }

        public InvoiceInfoEntity invoiceInfo { get; set; }

        public InvoiceInfoEntity invoiceInfoFinal { get; set; }

        public DeliveryChargeEntity[] deliveryCharges { get; set; }

        public TenderInfoEntity[] tenderInfo { get; set; }

        public PaymentEventInfoEntity paymentEventInfo { get; set; }

        public int numberOfLineItems { get; set; }
        public int numberOfFullCases { get; set; }
        public int numberOfPartialCases { get; set; }

       // public OrderItemsEntity[] orderItemDetail { get; set; }

        // public string paymentMethod { get; set; }
    }

    public class OrderItemsEntity
    {
        public int lineNumber { get; set; }
        public string lineStatus { get; set; }
        public int sku { get; set; }
        public int quantity { get; set; }
        public decimal itemLineTotal { get; set; }
        public int shippedQuantity { get; set; }
        // public string item_category { get; set; }
        public OrderItemPriceInfoEntity priceinfo { get; set;}
    }
    public partial class OrderItemPriceInfoEntity
    {
        public decimal sellingPrice { get; set; }
        public decimal unitPrice { get; set; }
        public decimal itemBottleDeposit { get; set; }
        public decimal itemDiscountAmount { get; set; }
        public decimal licMarkUp { get; set; }
        public decimal hstTax { get; set; }
        public decimal retailPrice { get; set; }
    }

    public partial class OrderShipToInfoEntity : AddressEntity
    {
        public bool overwriteShipToInformation { get; set; }
    }
    public  partial class OrderCommentsInfoEntity
    {
        public string BolComment { get; set; }
        public string PickerComment { get; set; }
        public string ShipLabelComment { get; set; }
        public PackingInvoiceCommentEntity PackingInvoiceComment { get; set; }
    }
    public partial class PackingInvoiceCommentEntity
    {
        public string TextLine1 { get; set; }
        public string TextLine2 { get; set; }
        public string TextLine3 { get; set; }
    }
    

    public partial class OrderStatusDetailEntity
    {
        public string statusPathName { get; set; }
        public int statusSequenceNo { get; set; }
        public string statusIdentifier { get; set; }
        public string statusCreateTime { get; set; }
    }
    public partial class OrderSourceInfoEntity
    {
       public string systemIdentifier { get; set; }
       public string systemName { get; set; }
       public OrderStatusDetailEntity[] systemOrderStatusDetail { get; set; }
    }
    public partial class OrderStatusCodeDetailEntity
    {
        public string orderStatusCode { get; set; }
        public string orderStatusName { get; set; }
        public string statusCreateTime { get; set; }
    }
    public partial class SystemIdentifyEntity
    {
        public string SystemIdentifier { get; set; }
        public string SystemName { get; set; }
    }
    public partial class ReferenceInfoEntity
    {
        public SystemIdentifyEntity referenceSystem { get; set; }
        public string referenceIdentifier { get; set; }
        public string referenceIdentifierType { get; set; }
        public string referenceIdentifierSubtype { get; set; }
        public string referenceComment { get; set; }
    }

    
    public partial class InvoiceInfoEntity
    {
        public string InvoiceNumber { get; set; }
        public string InvoiceType { get; set; }
        public decimal InvoiceTotalAmount { get; set; }
        public decimal InvoiceDiscountAmount { get; set; }
        public decimal InvoiceBottleDepositAmount { get; set; }
        public decimal InvoiceHstAmount { get; set; }
        public decimal? InvoiceLicMuAmount { get; set; }
        public decimal? InvoiceLevyAmount { get; set; }
        public decimal InvoiceRetailAmount { get; set; }
        public string CreatedDate { get; set; }
    }

    public partial class DeliveryChargeEntity
    {
        public SystemIdentifyEntity deliveryChargeSourceInfo { get; set; }
        public DeliveryChargeReferenceInfoEntity deliveryChargeReferenceInfo { get; set; }
        public string deliveryChargeType { get; set; }
        public string deliveryChargeSourceId { get; set; }
        public decimal deliveryBaseAmount { get; set; }
        public decimal deliveryHstAmount { get; set; }
        public string CreatedDate { get; set; }
    }

    public partial class TenderInfoEntity
    {
        public SystemIdentifyEntity tenderIssuingSystem { get; set; }
        public string tenderId { get; set; }
        public string tenderNotes { get; set; }
        public string tenderAmount { get; set; }
        public string tenderClass { get; set; }
        public string tenderType { get; set; }
        public string tenderDate { get; set; }
        public string referenceDocumentType { get; set; }
        public string referenceDocumentSubType { get; set; }
        public string referenceDocumentId { get; set; }
    }

    public partial class PaymentEventInfoEntity
    {
        public PaymentTransactionEntity[] paymentTransactions { get; set; }
        public PaymentInvoiceReferenceEntity paymentInvoiceReference { get; set; }
        public SettlementStatusEntity[] settlementStatus { get; set; }
    }
    public partial class DeliveryChargeReferenceInfoEntity
    {
        public SystemIdentifyEntity referenceSystem { get; set; }
        public string referenceIdentifier { get; set; }
        public string referenceIdentifierType { get; set; }
        public string referenceIdentifierSubtype { get; set; }
        public string referenceComment { get; set; }
    }
    public partial class PaymentTransactionEntity
    {
        public SystemIdentify initiatingSystem { get; set; }
        public string paymentTenderType { get; set; }
        public string paymentProcessingChannel { get; set; }
        public string paymentDate { get; set; }
        public string paymentTransactionId { get; set; }
        public string paymentConfirmationId { get; set; }
        public string paymentAmount { get; set; }
        public PaymentReferenceEntity paymentReference { get; set; }
    }
    public partial class PaymentInvoiceReferenceEntity
        {
            public SystemIdentify invoicingSystem { get; set; }
            public string paymentAmount { get; set; }
            public string invoiceSubmissionDate { get; set; }
            public string invoicePaymentDate { get; set; }
            public string invoicePaymentChannel { get; set; }
            public string invoicePaymentReponseMsg { get; set; }
            public ExternalInvoiceView externalInvoiceView { get; set; }

        }
    public partial class PaymentReferenceEntity
    {
        public string referenceNumber { get; set; }
    }
    public partial class SettlementStatusEntity
        {
            public string settlementStatus { get; set; }
            public string settlementTime { get; set; }
            public string settlementNotificationText { get; set; }
        }
    
}
