using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using RoconLibrary.Models;
using RoconLibrary.Domains;

namespace RoconLibrary.Models
{
    [DataContract]
    public partial class TenderEventInfoEntity
    {
        public string LV_SOUCESYSTEMID {get; set;}
        public string LV_SOURCESYSTEMNAME {get; set;}
        public string LV_TENDERID {get; set;}
        public string LV_TENDERCOMMENT {get; set;}
        public string LV_TENDERAMOUNT {get; set;}
        public string LV_TENDERCLASS {get; set;}
        public string LV_TENDERTYPE {get; set;}
        public DateTime LV_TENDERDATE {get; set;}
        public string LV_REFERENCEDOCUMENTTYPE {get; set;}
        public string LV_REFERENCEDOCUMENTSUBTYPE {get; set;}
        public string LV_REFERENCEDOCUMENTID {get; set;}
    }
}