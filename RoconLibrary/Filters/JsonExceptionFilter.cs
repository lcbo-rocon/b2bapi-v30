﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using RoconLibrary.Domains;
using RoconLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RoconLibrary.Filters
{
    public class JsonExceptionFilter : IExceptionFilter 
    {
        private readonly IHostingEnvironment _env;

        public JsonExceptionFilter(IHostingEnvironment env)
        {
            _env = env;
        }
        public void OnException(ExceptionContext context)
        {
            var error = new ErrorDetail();
            
            //To capture any system exception
            if (_env.IsDevelopment())
            {
                error.errorType = ErrorType.OTHER.ToString();
                error.errorMessage = "Exception: " + context.Exception.Message + " StactTrace: " + context.Exception.StackTrace.ToString();
                //error.errorData.key = context.Exception.StackTrace;
            }
            else
            {
                error.errorType = ErrorType.OTHER.ToString();
                error.errorMessage = context.Exception.Message;
            }


            context.Result = new ObjectResult(error)
            {
                StatusCode = 500
            };
        }
    }
}
