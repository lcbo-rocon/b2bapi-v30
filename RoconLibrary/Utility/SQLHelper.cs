﻿using System;
using Oracle.ManagedDataAccess.Client;
using RoconLibrary.Services;
using RoconLibrary.Services.Implementation;
using RoconLibrary.Models;

namespace RoconLibrary.Utility
{
    public static class SqlHelper
    {
        public static void logToErrorService(string errInfo, string errModule, string errCode, OracleConnection connection)
        {
            ILcoErrorLogService lcoErrorLogService = new LcoErrorLogService(connection);
            var lcoErrorLog = new LcoErrorLog();
            lcoErrorLog.lcoService = "RoconAPI";
            lcoErrorLog.lcoModule = errModule;
            lcoErrorLog.errorInfo = errInfo;
            lcoErrorLog.createdBy = "System";
            lcoErrorLog.errorCode = errCode;
            lcoErrorLogService.insertToTable(lcoErrorLog);
        }
        public static void logToErrorService(LcoErrorLog lcoErrorLog, OracleConnection connection)
        {
            ILcoErrorLogService lcoErrorLogService = new LcoErrorLogService(connection);
            lcoErrorLogService.insertToTable(lcoErrorLog);
        }

        public static string ExecuteProcedureReturnString(string connString, string procName,
            params OracleParameter[] paramters)
        {
            string result = "";
            using (var sqlConnection = new OracleConnection(connString))
            {
                using (var command = sqlConnection.CreateCommand())
                {
                    command.CommandType = System.Data.CommandType.StoredProcedure;
                    command.CommandText = procName;
                    if (paramters != null)
                    {
                        command.Parameters.AddRange(paramters);
                    }
                    sqlConnection.Open();
                    var ret = command.ExecuteScalar();
                    if (ret != null)
                        result = Convert.ToString(ret);
                }
            }
            return result;
        }

        public static TData ExecuteProcedureReturnData<TData>(string connString, string procName,
            Func<OracleDataReader, TData> translator,
            params OracleParameter[] parameters)
        {
            using (var sqlConnection = new OracleConnection(connString))
            {
                using (var sqlCommand = sqlConnection.CreateCommand())
                {
                    sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                    sqlCommand.CommandText = procName;
                    if (parameters != null)
                    {
                        sqlCommand.Parameters.AddRange(parameters);
                    }
                    sqlConnection.Open();
                    using (var reader = sqlCommand.ExecuteReader())
                    {
                        TData elements;
                        try
                        {
                            elements = translator(reader);
                        }
                        finally
                        {
                            while (reader.NextResult())
                            { }
                        }
                        return elements;
                    }
                }
            }
        }


        ///Methods to get values of 
        ///individual columns from sql data reader
        #region Get Values from Sql Data Reader
        public static string GetNullableString(OracleDataReader reader, string colName)
        {
            return reader.IsDBNull(reader.GetOrdinal(colName)) ? null : Convert.ToString(reader[colName]);
        }

        public static int GetNullableInt32(OracleDataReader reader, string colName)
        {
            return reader.IsDBNull(reader.GetOrdinal(colName)) ? 0 : Convert.ToInt32(reader[colName]);
        }

        public static decimal GetNullableDecimal(OracleDataReader reader, string colName)
        {
            return reader.IsDBNull(reader.GetOrdinal(colName)) ? 0 : Convert.ToDecimal(reader[colName]);
        }

        public static bool GetBoolean(OracleDataReader reader, string colName)
        {
            return reader.IsDBNull(reader.GetOrdinal(colName)) ? default(bool) : Convert.ToBoolean(reader[colName]);
        }

        //this method is to check wheater column exists or not in data reader
        public static bool IsColumnExists(this System.Data.IDataRecord dr, string colName)
        {
            try
            {
                return (dr.GetOrdinal(colName) >= 0);
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion
    }
}
